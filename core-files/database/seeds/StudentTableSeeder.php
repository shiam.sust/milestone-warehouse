<?php

use Illuminate\Database\Seeder;
use App\Model\Student;

class StudentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Student::create([
        	'year' => '2018',
        	'quantity' => 100,
        	'status' => 1
        ]);

        Student::create([
        	'year' => '2018',
        	'quantity' => 110,
        	'status' => 1
        ]);

        Student::create([
        	'year' => '2018',
        	'quantity' => 120,
        	'status' => 1
        ]);
    }
}
