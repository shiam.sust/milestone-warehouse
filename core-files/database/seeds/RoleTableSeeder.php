<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            'Initial Product Create',
            'Initial Product Update',
            'Initial Product Set Create',
            'Initial Product Set Update',
            'Class Create',
            'Class Update',
            'Class Delete',
            'Wirehouse Create',
            'Wirehouse Update',
            'Wirehouse Delete',
            'Wirehouse Manager Create',
            'Wirehouse Manager Update',
            'Wirehouse Manager Delete',
            'Purchase Order Create',
            'Purchase Order Update',
            'Purchase Order Delete',
            'Purchase Challan Create',
            'Purchase Challan Update',
            'Purchase Challan Delete',
            'Product Requisition Approve',
            'Product Return Update',
            'Product Return Approve',
            'Payment Create For Purchase Challan',
        ];
        foreach ($roles as $role){
            $slug = str_slug($role,'-');
            \App\Role::firstOrCreate([
                'slug'  => $slug,
                'name'  => $role
            ]);
        }
    }
}
