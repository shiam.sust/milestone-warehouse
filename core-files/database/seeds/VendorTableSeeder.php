<?php

use Illuminate\Database\Seeder;
use App\Model\WireHouse\Vendor;
use App\Model\WireHouse\VendorContact;

class VendorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        

         $one = Vendor::create([
            'name' => 'firstwh', 
            'mobile' => '147',
            'email' => '@gmail.com',
            'address' => 'gajipur',
            'invoiceaddress' => 'bangladesh',
            'status' => 1          
        ]);

        $two = Vendor::create([
            'name' => 'secondwh', 
            'mobile' => '258',
            'email' => '@gmail.com',
            'address' => 'kakrail',
            'invoiceaddress' => 'bangladesh',
            'status' => 1           
        ]);

        $three = Vendor::create([
            'name' => 'thirdwh', 
            'mobile' => '369',
            'email' => '@gmail.com',
            'address' => 'badda',
            'invoiceaddress' => 'bangladesh',
            'status' => 1          
        ]);

        $four = Vendor::create([
        	'name' => 'fourwh', 
            'mobile' => '369',
            'email' => '@gmail.com',
            'address' => 'badda',
            'invoiceaddress' => 'bangladesh',
            'status' => 1
        ]);

        VendorContact::create([
            'name' => 'xyz',
            'vd_id' => $one->id,
            'mobile' => '123',
            'designation' => 'kichu ekta',   
        ]);

        VendorContact::create([
            'name' => 'abc',
            'vd_id' => $two->id,
            'mobile' => '456',
            'designation' => 'kichu ekta',
        ]);

        VendorContact::create([
            'name' => 'asd',
            'vd_id' => $three->id,
            'mobile' => '789',
            'designation' => 'kichu ekta',
        ]);

        VendorContact::create([
            'name' => 'asdcvb',
            'vd_id' => $four->id,
            'mobile' => '582',
            'designation' => 'kichu ekta', 
        ]);
    }
}
