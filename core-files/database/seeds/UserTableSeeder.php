<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Model\Product\ProductType;
use App\Model\Product\Product;
use App\Model\Product\ProductSet;
use App\Model\Product\ProductSetDetails;
use App\Model\Product\Unit;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*User::create([
        	'name' => 'Exabyte',
        	'email' => 'admin@gmail.com',
        	'password' => bcrypt('123456'),
        	'status'	=> true,
        ]);*/
        $setone = ProductSet::create([
            'name' => 'set one',
            'status' => 1
        ]);

        $settwo = ProductSet::create([
            'name' => 'set two',
            'status' => 1
        ]);

        $setthree = ProductSet::create([
            'name' => 'set three',
            'status' => 1
        ]);



        $ptbook = ProductType::create([
            'name' => 'Book',
            'status' => 1
        ]);

        $ptpencil = ProductType::create([
            'name' => 'pencil',
            'status' => 1
        ]);

        $ptpen = ProductType::create([
            'name' => 'pencil',
            'status' => 1
        ]);

        $unitone = Unit::create([
            'name' => 'PC/s',
            'status' => 1
        ]);

        $unittwo = Unit::create([
            'name' => 'Dozon',
            'status' => 1
        ]);

        $unitthree = Unit::create([
            'name' => 'Kg',
            'status' => 1
        ]);

        $productone = Product::create([
            'name' => 'Rhymes for the play group',
            'type_id' => $ptbook->id,
            'base_unit' => $unitone->id,
            'author' => 'World Book house',
            'selling_price' => '100',
            'cost_price' => '90',
            'status' => 1
        ]);


        $producttwo = Product::create([
            'name' => 'Kids ABC Eng',
            'type_id' => $ptbook->id,
            'base_unit' => $unittwo->id,
            'author' => 'Akib Publication',
            'selling_price' => '120',
            'cost_price' => '110',
            'status' => 1
        ]);

        $productthree = Product::create([
            'name' => 'Grammer Plus-8',
            'type_id' => $ptbook->id,
            'base_unit' => $unitthree->id,
            'author' => 'Akib Publication',
            'selling_price' => '120',
            'cost_price' => '110',
            'status' => 1
        ]);

        ProductSetDetails::create([
            'product_set_id' => $setone->id,
            'product_id' => $productone->id
        ]);

        ProductSetDetails::create([
            'product_set_id' => $setone->id,
            'product_id' => $producttwo->id
        ]);

        ProductSetDetails::create([
            'product_set_id' => $setone->id,
            'product_id' => $productthree->id
        ]);

        


    }
}
