<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(AdminSeeder::class);
        $this->call(RoleTableSeeder::class);
        //$this->call(ClassTableSeeder::class);
        //$this->call(VendorTableSeeder::class);
    }
}
