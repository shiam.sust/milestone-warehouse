<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Model\BasicSettings::create([
            'name'  => 'Milestone School & College',
            'address'   => 'Dhaka,Bangladesh'
        ]);
    }
}
