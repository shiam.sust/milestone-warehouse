<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUnitQuantityToProductSetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_set_details', function (Blueprint $table) {
            $table->integer('unit_id')->unsigned()->nullable();
            $table->integer('quantity')->nullable();
            $table->foreign('unit_id')->references('id')->on('units');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_set_details', function (Blueprint $table) {
            $table->dropColumn(['unit_id','quantity']);
        });
    }
}
