<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductReturnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_returns', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('from_warehouse')->unsigned()->nullable();
            $table->integer('to_warehouse')->unsigned()->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->string('ret_no_auto');
            $table->string('ret_no_manual')->nullable();
            $table->date('return_date')->nullable();
            $table->tinyInteger('status')->default(1);

            $table->foreign('from_warehouse')->references('id')->on('wire_houses');
            $table->foreign('to_warehouse')->references('id')->on('wire_houses');
            $table->foreign('created_by')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_returns');
    }
}
