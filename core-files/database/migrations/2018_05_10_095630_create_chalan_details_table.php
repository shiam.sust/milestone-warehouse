<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChalanDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chalan_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('chalan_id')->unsigned();
            $table->integer('purchase_order_details_id')->unsigned();
            $table->integer('product_id')->unsigned()->nullable();
            $table->integer('product_set_id')->unsigned()->nullable();
            $table->integer('received_qty')->nullable();
            $table->float('product_amount')->nullable();
            $table->timestamps();
            $table->foreign('chalan_id')->references('id')->on('chalans');
            $table->foreign('purchase_order_details_id')->references('id')->on('purchase_order_details');
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('product_set_id')->references('id')->on('product_sets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chalan_details');
    }
}
