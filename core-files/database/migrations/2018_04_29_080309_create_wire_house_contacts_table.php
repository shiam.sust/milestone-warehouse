<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWireHouseContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wire_house_contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('wh_id')->unsigned()->nullable();
            $table->string('name');
            $table->string('mobile')->nullable();
            $table->string('email')->nullable();
            $table->string('designation')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
            $table->foreign('wh_id')->references('id')->on('wire_houses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wire_house_contacts');
    }
}
