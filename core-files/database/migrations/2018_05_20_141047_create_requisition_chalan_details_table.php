<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequisitionChalanDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requisition_chalan_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('req_chalan_id')->unsigned();
            $table->integer('item_id')->unsigned();
            $table->tinyInteger('item_type');
            $table->integer('unit_id')->unsigned()->nullable();
            $table->integer('quantity');
            $table->timestamps();

            $table->foreign('req_chalan_id')->references('id')->on('requisition_chalans');
            $table->foreign('unit_id')->references('id')->on('units');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requisition_chalan_details');
    }
}
