<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payment_method_id')->unsigned();
            $table->integer('chalan_id')->unsigned();
            $table->float('amount')->default(0);
            $table->float('discount')->default(0);
            $table->float('due')->default(0);
            $table->boolean('is_fixed')->default(0);
            $table->string('attachment')->nullable();
            $table->timestamps();

            $table->foreign('chalan_id')->references('id')->on('chalans');
            $table->foreign('payment_method_id')->references('id')->on('payment_methods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
