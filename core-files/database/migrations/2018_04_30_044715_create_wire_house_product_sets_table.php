<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWireHouseProductSetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wire_house_product_sets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('wh_id')->unsigned()->nullable();
            $table->integer('product_set_id')->unsigned()->nullable();
            $table->integer('initial_quantity')->nullable()->default(0);
            $table->integer('quantity')->nullable()->default(0);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
            $table->foreign('wh_id')->references('id')->on('wire_houses');
            $table->foreign('product_set_id')->references('id')->on('product_sets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wire_house_product_sets');
    }
}
