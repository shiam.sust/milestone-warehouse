<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClassIdToProductSetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_sets', function (Blueprint $table) {
            $table->integer('class_id')->unsigned()->nullable();
            $table->foreign('class_id')->references('id')->on('academic_classes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_sets', function (Blueprint $table) {
            $table->dropColumn('class_id');
        });
    }
}
