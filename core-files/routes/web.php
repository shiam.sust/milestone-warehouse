<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('reset-pass',function (){
    \App\User::where('email','sacchu@yahoo.com')->update([
        'password'  => bcrypt('123456')
    ]);
});

Auth::routes();
Route::group(['middleware'=> ['auth','super.admin']],function (){

    Route::get('/', 'HomeController@index')->name('default');

    Route::get('/home', 'HomeController@index')->name('home');
});

Route::group(['middleware' => 'guest'],function (){});

Route::group(['middleware' => 'auth'],function (){
    Route::get('user-logout',function (){
        Auth::logout();
        return redirect()->route('login');
    })->name('user-logout');
});

Route::group(['middleware' => ['auth','super.admin']],function (){
    
    /**
     * Category
     */

    Route::get('/categories', [
        'uses'  => 'Product\CategoryController@index',
        'as'    => 'categories'
    ]);

    Route::get('/category/{id?}', [
        'uses'  => 'Product\CategoryController@getCategory',
        'as'    => 'get-category'
    ]);

    Route::get('delete/category/{id}', [
        'uses'  => 'Product\CategoryController@delete',
        'as'    => 'delete-category'
    ]);


    Route::post('/category', [
        'uses'  => 'Product\CategoryController@create',
        'as'    => 'category'
    ]);

    Route::post('/update-category/{id?}', [
        'uses'  => 'Product\CategoryController@update',
        'as'    => 'update-category'
    ]);


    /*
     * Manufacturer
     */
    Route::get('/manufacturers', [
        'uses'  => 'Product\ManufacturerController@index',
        'as'    => 'manufacturers'
    ]);

    Route::get('/manufacturer/{id?}', [
        'uses'  => 'Product\ManufacturerController@manufacturer',
        'as'    => 'manufacturer'
    ]);

    Route::post('/manufacturer', [
        'uses'  => 'Product\ManufacturerController@create',
        'as'    => 'manufacturer'
    ]);

    Route::post('update/manufacturer/{id?}', [
        'uses'  => 'Product\ManufacturerController@update',
        'as'    => 'update-manufacturer'
    ]);

    /*
     * Unit Of Measurement wirehousemanager
     */

    Route::get('/units', [
        'uses'  => 'Product\UnitController@index',
        'as'    => 'units'
    ]);

    Route::get('/unit/{id?}', [
        'uses'  => 'Product\UnitController@getUnit',
        'as'    => 'unit'
    ]);

    Route::get('delete/unit/{id}', [
        'uses'  => 'Product\UnitController@delete',
        'as'    => 'delete-unit'
    ]);


    Route::post('/unit', [
        'uses'  => 'Product\UnitController@create',
        'as'    => 'unit'
    ]);

    Route::post('/update-unit/{id?}', [
        'uses'  => 'Product\UnitController@update',
        'as'    => 'update-unit'
    ]);


    /**
    * All Basic Settings Routes
    */
    Route:: get('basicsettings', [
        'uses' => 'BasicSettingsController@index'
    ])->name('basicsettings');

    Route:: post('update_basic/{id?}', [
        'uses' => 'BasicSettingsController@update'
    ])->name('update_basic');



    /**
     * WireHouses
     */

    Route:: get('wirehousees', [
        'uses'  => 'WirehouseController@index',
        'as'    => 'wirehouses'
    ]);

    Route:: get('delete_wirehouse/{id}', [
        'uses' => 'WirehouseController@delete'
    ])->name('delete_wirehouse')->middleware('can:wirehouse-delete');

    Route:: get('edit_wirehouse/{id}', [
        'uses' => 'WirehouseController@showUpdate'
    ])->name('edit_wirehouse')->middleware('can:wirehouse-update');

    Route:: post('update_wirehouse/{id}', [

        'uses' => 'WirehouseController@update'

    ])->name('update_wirehouse')->middleware('can:wirehouse-update');


    /**
     * Wirehouse Manager
     */
    Route:: get('wirehouse-manager', [
        'uses'  => 'WirehouseManagerController@index',
        'as'    => 'wirehouse-manager'
    ]);

    Route:: get('add_wirehouse_manager', [
        'uses' => 'WirehouseManagerController@showWireHouseManagerForm'
    ])->name('add_wirehouse_manager')->middleware('can:wirehouse-manager-create');

    Route:: get('delete_wirehouse_manager/{id}', [
        'uses' => 'WirehouseManagerController@delete'
    ])->name('delete_wirehouse_manager')->middleware('can:wirehouse-manager-delete');

    Route:: get('edit_wh_manager/{id}', [
        'uses' => 'WirehouseManagerController@showUpdate'
    ])->name('edit_wh_manager')->middleware('can:wirehouse-manager-update');

    Route:: get('add_wirehouse', function(){
        return view('admin.add_wirehouse');
    })->name('add_wirehouse')->middleware('can:wirehouse-create');

    Route:: post('create-wirehouse', [

        'uses' => 'WirehouseController@store'

    ])->name('post.wirehouse')->middleware('can:wirehouse-create');//wirehouse_manager

    Route:: post('wirehouse_manager', [

        'uses' => 'WirehouseManagerController@store'

    ])->name('post.wirehouse_manager')->middleware('can:wirehouse-manager-create');

    Route:: post('update/wirehouse_manager/{id}', [

        'uses' => 'WirehouseManagerController@update'

    ])->name('update.wirehouse_manager')->middleware('can:wirehouse-manager-update');




    /**
     * All Class related route
     */
    Route:: get('class', [
        'uses' => 'AcademicClassController@index'
    ])->name('class');

    Route:: post('class', [
        'uses' => 'AcademicClassController@store'
    ])->name('post.class')->middleware('can:class-create');

    Route:: get('add_class', function(){
        return view('admin.add_class');
    })->name('add_class')->middleware('can:class-create');

    Route:: get('delete_class/{id}',[
        'uses' => 'AcademicClassController@delete'
    ])->name('delete_class')->middleware('can:class-delete');

    Route:: get('edit_class/{id}', [
        'uses' => 'AcademicClassController@showUpdate'
    ])->name('edit_class')->middleware('can:class-update');

    Route:: post('update_class/{id}', [

        'uses' => 'AcademicClassController@update'

    ])->name('update_class')->middleware('can:class-update');




    /**
     * Product Type related route
    */
    Route:: get('product_type', [

        'uses' => 'Product\ProductTypeController@index'

    ])->name('product_type');

    Route:: post('product_type', [

        'uses' => 'Product\ProductTypeController@store'

    ])->name('post.product_type');

    Route:: get('show_edit_product_type/{id}', [
        'uses' => 'Product\ProductTypeController@showUpdate'
    ])->name('show_edit_product_type');

    Route:: post('update_product_type/{id}', [

        'uses' => 'Product\ProductTypeController@update'

    ])->name('update_product_type');
    
    Route:: get('add_product_type', function(){
        return view('admin.add_product_type');
    })->name('add_product_type');

    Route:: get('delete_product_type/{id}', [
        'uses' => 'Product\ProductTypeController@delete'
    ])->name('delete_product_type');




    /**
     * Product Unit type route
     * 
     */
    Route:: get('product_unit', [

        'uses' => 'Product\UnitController@index'

    ])->name('product_unit');
    
    Route:: get('add_product_unit', function(){
        return view('admin.add_product_unit');
    })->name('add_product_unit');

    Route:: post('unit',[
        'uses' => 'Product\UnitController@store'
    ])->name('post.unit');

    Route:: get('show_update_unit/{id}', [
        'uses' => 'Product\UnitController@showUpdate'
    ])->name('show_update_unit');

    Route:: post('update_product_unit/{id}', [
        'uses' => 'Product\UnitController@update'
    ])->name('update_product_unit');

    Route:: get('delete_unit/{id}', [
        'uses' => 'Product\UnitController@delete'
    ])->name('delete_unit');



    /**
     * Product routes
     * 
     */
    Route:: get('product', [

        'uses' => 'Product\ProductController@index'

    ])->name('product');

    Route:: get('delete_product/{id}', [
        'uses' => 'Product\ProductController@delete'
    ])->name('delete_product');

    Route:: get('add_product', [
        'uses' => 'Product\ProductController@showProductForm'
    ])->name('add_product');

    Route:: post('product', [
        'uses' => 'Product\ProductController@store'
    ])->name('post.product');

    Route:: get('show_edit_product_form/{id}', [
        'uses' => 'Product\ProductController@showUpdate'
    ])->name('show_edit_product_form');

    Route:: post('update_product/{id}', [
        'uses' => 'Product\ProductController@update'
    ])->name('update_product');


    /**
     * Product Set related routes
     */
    Route:: get('product_set', [
        'uses' => 'Product\ProductSetController@index'
    ])->name('product_set');

    Route:: get('add_product_set', [ 
        'uses' => 'Product\ProductSetController@showForm'   
    ])->name('add_product_set');

    Route:: get('edit-product-set/{id}', [ 
        'uses' => 'Product\ProductSetController@edit'   
    ])->name('edit_product_set');

    Route:: post('edit-product-set/{id}', [ 
        'uses' => 'Product\ProductSetController@update'   
    ])->name('update_product_set');

    Route:: post('product_set', [
        'uses' => 'Product\ProductSetController@store'
    ])->name('post.product_set');

    Route:: post('update_product_set/{id}', [
        'uses' => 'Product\ProductSetController@update'
    ])->name('update.product_set');

    Route:: get('delete_product_set/{id}', [
        'uses' => 'Product\ProductSetController@delete'
    ])->name('delete_product_set');


    /**
     * Vendors
     */
    Route:: get('vendors', [
        'uses' => 'VendorController@index',
        'as'    => 'vendors'
    ]);

    Route:: post('vendors', [

        'uses' => 'VendorController@store'

    ])->name('post.vendor');
    
    Route:: get('add_vendor', function(){

        return view('admin.add_vendor');
        
    })->name('add_vendor');

    Route:: get('delete_vendor/{id}', [
        'uses' => 'VendorController@delete'
    ])->name('delete_vendor');

    Route:: get('edit_vendor/{id}', [
        'uses' => 'VendorController@showUpdate'
    ])->name('edit_vendor');

    Route:: post('update_vendor/{id}', [
        'uses' => 'VendorController@update'
    ])->name('update_vendor');



    /**
     * Product requisition related routes
     */  
    Route::get('product_demand', [
        'uses' => 'Product\ProductDemandController@index'
    ])->name('product_demand');

    Route:: get('add_demand', [
        'uses' => 'Product\ProductDemandController@showForm'
    ])->name('add_demand');

    Route:: post('product_demand', [
        'uses' => 'Product\ProductDemandController@store'
    ])->name('post.product_demand');

    Route:: post('edit_demand/{id}', [

    ])->name('edit_demand');

    route::get('product-demand/{id}',[
        'uses'  => 'Product\ProductDemandController@show',
        'as'    => 'show-product-demand'
    ]);

    Route::get('approve-requisition/{id}',[
        'uses' => 'Product\ProductDemandController@approve',
        'as'    => 'approve-requisition'

    ])->middleware('can:product-requisition-approve');

    Route::get('receive-return/{id}',[
        'uses' => 'Product\ProductDemandController@receiveReturn',
        'as'    => 'receive-return'
    ]);




    /**
     * Get Products and Product Sets
     */
    Route::get('get-product-and-product-sets',[
        'uses'  => 'ItemController@getProductsAndSets',
        'as'    => 'get-product-and-product-sets'
    ]);

    
Route:: get('item/{id?}', [
        'uses'  => 'Product\ProductDemandController@getItems',

    ])->name('item');


    /**
     * Purchase Order
     */
    Route:: get('purchase', function(){
        return view('admin.show_purchase');
    })->name('purchase');

    Route::get('purchase-orders',[
        'uses'  => 'PurchaseOrderController@index',
        'as'    => 'purchase-orders'
    ]);

    Route::get('purchase-order/{id}',[
        'uses'  => 'PurchaseOrderController@getPurchaseOrder',
        'as'    => 'get-purchase-order'
    ]);

    Route::get('create-purchase-order',[
        'uses'  => 'PurchaseOrderController@create',
        'as'    => 'create-purchase-order'
    ])->middleware('can:purchase-order-create');

    Route::post('create-purchase-order',[
        'uses'  => 'PurchaseOrderController@store',
        'as'    => 'store-purchase-order'
    ])->middleware('can:purchase-order-create');

    Route::get('edit-purchase-order/{id}',[
        'uses'  => 'PurchaseOrderController@editPurchaseOrder',
        'as'    => 'edit-purchase-order'
    ])->middleware('can:purchase-order-update');

    Route::post('edit-purchase-order/{id}',[
        'uses'  => 'PurchaseOrderController@updatePurchaseOrder',
        'as'    => 'update-purchase-order'
    ])->middleware('can:purchase-order-update');

    Route::get('return-purchase-order/{id}',[
        'uses'  => 'PurchaseOrderController@getReurnPurchaseOrder',
        'as'    => 'return-purchase-order'
    ])->middleware('can:purchase-return-create');

    Route::post('return-purchase-order/{id}',[
        'uses'  => 'PurchaseOrderController@storeReurnPurchaseOrder',
        'as'    => 'store-purchase-return'
    ])->middleware('can:purchase-return-create');



    /**
     * Chalan
     */
    Route::get('chalans',[
        'uses'  => 'ChalanController@index',
        'as'    => 'chalans'
    ]);

    Route::get('chalan/{id}',[
        'uses'  => 'ChalanController@getChalan',
        'as'    => 'get-chalan'
    ]);

    Route::get('approve/chalan/{id}',[
        'uses'  => 'ChalanController@approveChalan',
        'as'    => 'approve-chalan'
    ])->middleware('can:purchase-challan-approve');


    Route:: get('item/{id?}', [
        'uses'  => 'Product\ProductDemandController@getItems',

    ])->name('item');



    Route::get('create-chalan/{id?}',[
        'uses'  => 'ChalanController@create',
        'as'    => 'create-chalan'
    ])->middleware('can:purchase-challan-create');

    Route::post('create-chalan',[
        'uses'  => 'ChalanController@store',
        'as'    => 'store-chalan'
    ])->middleware('can:purchase-challan-create');

    Route::get('purchase-order/items/{id}',[
        'uses'  => 'PurchaseOrderController@getPurchaseOrderItems',
        'as'     => 'purchase-order-items'
    ]);

    /**
     * Payment
     */

    Route::get('payments',[
        'uses'  => 'PaymentController@index',
        'as'    => 'payments'
    ]);

    Route::get('payment',[
        'uses'  => 'PaymentController@create',
        'as'    => 'create-payment'
    ])->middleware('can:payment-create-for-purchase-challan');

    Route::post('payment',[
        'uses'  => 'PaymentController@store',
        'as'    => 'store-payment'
    ]);

    Route::get('payment/chalan/items/{id}',[
        'uses'  => 'PaymentController@getChalanItem',
        'as'    => 'get-payment-chalan-items'
    ]);

    /**
     * Profile
     */

    Route::get('profile',[
        'uses'   => 'UserController@index',
        'as'     => 'my-profile'
    ]);

    Route::post('change-password',[
        'uses'   => 'UserController@changePassword',
        'as'     => 'change-password'
    ]);

    /**
     * Stock Balance
     */

    Route::get('balance-product',[
        'uses'  => 'Product\ProductController@stock',
        'as'    => 'balance-product'
    ]);

    Route::get('create-initial-product-quantity',[
        'uses'  => 'Product\ProductController@createInitialQuantity',
        'as'    => 'create-initial-product-quantity'
    ])->middleware('can:initial-product-create');

    Route::post('create-initial-product-quantity',[
        'uses'  => 'Product\ProductController@storeInitialQuantity',
        'as'    => 'store-initial-product-quantity'
    ])->middleware('can:initial-product-create');
    Route::post('update-initial-product-quantity/{id}',[
        'uses'  => 'Product\ProductController@updateInitialQuantity',
        'as'    => 'update-initial-product-quantity'
    ])->middleware('can:initial-product-update');

    Route::get('balance-product-set',[
        'uses'  => 'Product\ProductSetController@stock',
        'as'    => 'balance-product-set'
    ]);

    Route::get('create-initial-product-set-quantity',[
        'uses'  => 'Product\ProductSetController@createInitialQuantity',
        'as'    => 'create-initial-product-set-quantity'
    ])->middleware('can:initial-product-set-create');

    Route::post('create-initial-product-set-quantity',[
        'uses'  => 'Product\ProductSetController@storeInitialQuantity',
        'as'    => 'store-initial-product-set-quantity'
    ])->middleware('can:initial-product-set-create');

    Route::post('update-initial-product-set-quantity/{id}',[
        'uses'  => 'Product\ProductSetController@updateInitialQuantity',
        'as'    => 'update-initial-product-set-quantity'
    ])->middleware('can:initial-product-set-update');

    /**
     *Product Demand Chalan
     */

    Route::get('product-demand-chalans',[
        'uses'  => 'ProductDemandChalanController@index',
        'as'    => 'product-demand-chalans'
    ]);

    Route::get('product-demand-chalan/{id}',[
        'uses'  => 'ProductDemandChalanController@show',
        'as'    => 'product-demand-chalan'
    ]);

    Route::get('approve/product-demand-chalan/{id}',[
        'uses'  => 'ProductDemandChalanController@approve',
        'as'    => 'approve-product-demand-chalan'
    ])->middleware('can:approve-product-demand-chalan');

    /**
     * Product Return
     */


    Route::get('get-products-return',[
        'uses'  => 'ProductReturnController@index',
        'as'    => 'products-return'
    ]);

    Route::get('get-product-return/{id}',[
        'uses'  => 'ProductReturnController@show',
        'as'    => 'product-return'
    ]);

    Route::get('approve/product-return/{id}',[
        'uses'  => 'ProductReturnController@approveReturn',
        'as'    => 'approve-product-return'
    ])->middleware('can:product-return-approve');
    Route:: get('edit/product-return/{id}', [
        'uses' => 'ProductReturnController@edit',
        'as' => 'edit-product-return'
    ])->middleware('can:product-return-update');

    Route:: post('edit/product-return/{id}', [
        'uses' => 'ProductReturnController@update',
        'as' => 'update-product-return'
    ])->middleware('can:product-return-update');

    /**
     * Return Chalan
     */

    Route::get('get-products-return-chalan',[
        'uses'  => 'ReturnChalanController@index',
        'as'    => 'products-return-chalans'
    ]);

    Route::get('get-product-return-chalan/{id}',[
        'uses'  => 'ReturnChalanController@show',
        'as'    => 'product-return-chalan'
    ]);

    Route::get('approve-product-return-chalan/{id}',[
        'uses'  => 'ReturnChalanController@approve',
        'as'    => 'approve-product-return-chalan'
    ]);

    Route::get('update-roles/{id}',[
        'uses'  => 'WirehouseManagerController@editRole',
        'as'    => 'edit-role'
    ])->middleware('can:role-update');

    Route::post('update-roles/{id}',[
        'uses'  => 'WirehouseManagerController@updateRole',
        'as'    => 'update-role'
    ])->middleware('can:role-update');


});

ROute::group(['middleware' => 'auth'],function(){
    Route::post('change-profile-pic',[
        'uses'  => 'UserController@changeProfilePic',
        'as'    => 'change-profile-pic'
    ]);
});
