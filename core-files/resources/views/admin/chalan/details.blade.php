@extends('layouts.admin')

@section('content')

    <div class="content-wraper">
        <div class="white-box mb-20">
            <h3 class="box-title">Chalan Details
                <a href="{{ route('chalans') }}" class="btn btn-info btn-sm pull-right flat"><i class="fa-fw ti-view-list"></i>Chalan List</a>
                @if($chalan->status == 1 || $chalan->status == 2)
                    <a href="{{ route('create-payment',['id' => $chalan->id]) }}" class="btn btn-sm btn-success flat pull-right"><i class="fa-fw ti-plus"></i>Create Payment</a>
                @endif
            </h3>
            @if($chalan)
                <p>
                    <span class="text-success"><strong>CH No Auto:</strong> {{ $chalan->chalan_no }}</span>
                    <span class="text-info">| <strong>CH No Manual:</strong> {{ $chalan->chalan_no_manual }}</span>
                    <span class="text-warning">| <strong>PO No Auto</strong>: {{ $chalan->purchaseOrder->po_no_auto  }}</span>
                    <span class="text-primary">| <strong>Date:</strong> {{ $chalan->chalan_date }}</span>
                    <span class="text-success">| <strong>Status:</strong>
                        @if(!$chalan->status)
                            Not Approved
                        @elseif($chalan->status ==1)
                            Approved
                        @elseif($chalan->status ==2)
                            Partially Paid
                        @elseif($chalan->status ==3)
                            Paid
                        @endif
                    </span>
                </p>
            @endif
            @if(!empty($chalan))
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Product</th>
                        <th>Quantity</th>
                        <th>Unit</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($chalan->chalanDetails as $chalan_detail)
                        <tr>
                            <td>{{ $chalan_detail->item->name  }}</td>
                            <td>{{ $chalan_detail->received_qty }}</td>
                            <td>{{ $chalan_detail->item->base_unit ? $chalan_detail->item->unit->name : 'SET' }}</td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            @else
                <p class="text-danger"><strong>Sorry! No chalan found.</strong></p>
            @endif
        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {

        });

    </script>
@endsection
