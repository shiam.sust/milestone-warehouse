@extends('layouts.admin')

@section('content')

    <div class="content-wraper">
        <div class="white-box mb-20">
            <h3 class="box-title text-success">PRODUCT PURCHASE CHALAN LISTS<a href="{{ route('create-chalan') }}" class="btn btn-sm btn-info flat pull-right"><i class="fa fa-plus-circle"></i> CREATE NEW CHALAN</a></h3>
            <p class="text-muted m-b-30">All Challan List </p>
            <hr/>
            <table class="table table-bordered table-striped" id="chalanListTable">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>CH No Auto/Manual</th>
                    <th>Wirehouse</th>
                    <th>PO No Auto/Manual</th>
                    <th>Chalan Date</th>
                    <th>Status</th>
                    <th><i class="fa-fw ti-eye"></i></th>
                </tr>
                </thead>

                <tbody>
                @foreach($chalans as $k => $chalan)
                    <tr>
                        <td>{{ $k+1 }}</td>
                        <td><a href="{{ route('get-chalan',['id' => $chalan->id]) }}">{{ $chalan->chalan_no }} / {{ $chalan->chalan_no_manual }}</a></td>
                        <td>{{ $chalan->wireHouse->name }}</td>
                        <td>{{ $chalan->purchaseOrder->po_no_auto }} / {{ $chalan->purchaseOrder->po_no_namual }}</td>
                        <td>{{Carbon\Carbon::parse( $chalan->chalan_date)->format('d F Y') }}</td>
                        <td>
                            @if(!$chalan->status)
                                <span class="badge badge-danger">Pending</span>
                            @elseif($chalan->status == 1)
                                <span class="badge badge-warning">Approved</span>
                            @elseif($chalan->status == 2)
                                <span class="badge badge-primary">Partially Paid</span>
                            @elseif($chalan->status == 3)
                                <span class="badge badge-success">Paid</span>
                            @endif
                        </td>
                        <td>
                            @if(!$chalan->status && Auth::user()->can('purchase-challan-approve'))
                                <a href="{{ route('approve-chalan',['id' => $chalan->id]) }}" class="btn btn-warning btn-xs flat"><i class="fa fa-fw fa-check-circle"></i>Approve</a>
                            @endif

                            @if(($chalan->status == 1 || $chalan->status == 2) && Auth::user()->can('payment-create-for-purchase-challan'))
                                <a href="{{ route('create-payment',['id' => $chalan->id]) }}" class="btn btn-xs btn-info flat pull-right"><i class="fa fa-plus-circle"></i> PAYMENT</a>
                            @endif

                            <a href="{{ route('get-chalan',['id' => $chalan->id]) }}" class="btn btn-primary btn-xs  flat"><i class="ti-eye fa-fw"></i>DETAILS</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>

            </table>
        </div>

    </div>

@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#chalanListTable").dataTable();
        });

    </script>
@endsection
