@extends('layouts.admin')

@section('content')

    <div class="white-box mb-20">
        <h3 class="box-title m-b-20">Update Purchase Order<a href="{{ route('purchase-orders') }}" class="btn btn-sm btn-info pull-right"><i class="fa-fw ti-view-list"></i>Purchase Orders</a></h3>
        <hr>
        <form method="post" action="{{ route('update-purchase-order',['id' => $purchase_order->id]) }}">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Warehouse <span class="text-danger m-l-5">*</span></label>
                        <select class="form-control" data-placeholder="Choose a Category" tabindex="1" name="wh_id" required>
                            <option value="">Select Warehouse</option>
                            @foreach($wirehouses as $warehouse)
                                <option value="{{ $warehouse->id }}" {{ $purchase_order->wh_id == $warehouse->id ? 'selected="selected"' : '' }}>{{ $warehouse->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Vendor<span class="text-danger m-l-5">*</span></label>
                        <select class="form-control" data-placeholder="Choose a Category" tabindex="1" name="vendor_id" required>
                            <option value="">--Select Vendor--</option>
                            @foreach($vendors as $vendor)
                                <option value="{{ $vendor->id }}" {{ $purchase_order->vendor_id == $vendor->id ? 'selected="selected"' : '' }}>{{ $vendor->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Purchase No Auto.<span class="text-danger m-l-5">*</span></label>
                        <input type="text" class="form-control" name="po_no_auto"  value="{{ $purchase_order->po_no_auto }}" readonly required>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>Purchase No Manual.</label>
                        <input type="text" class="form-control" name="po_no_manual" value="{{ $purchase_order->po_no_manual }}">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label>Purchase Order Date<span class="text-danger m-l-5">*</span></label>
                        <input type="text" class="form-control datepicker" name="po_date" value="{{ $purchase_order->po_date }}" required>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Delivery Date</label>
                        <input type="text" class="form-control datepicker" placeholder="YYYY-MM-DD" name="delivery_date" value="{{ $purchase_order->po_date }}">
                    </div>
                </div>

            </div>

            <edit-purchase :purchase_data="{{ $data }}"></edit-purchase>

            <div class="form-group clearfix text-right">
                <button class="btn btn-success">UPDATE</button>
            </div>

        </form>

    </div>


@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.datepicker').datepicker({
                "format": 'yyyy-mm-dd',
                "todayHighlight": true,
                "autoclose": true
            });
        });
    </script>
@endsection
