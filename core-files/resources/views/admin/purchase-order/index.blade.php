@extends('layouts.admin')

@section('content')

    <div class="content-wraper">
        <div class="white-box mb-20">
            <h3 class="box-title text-success">PURCHASE ORDERS
                @if(Auth::user()->can('purchase-order-create'))
                    <a href="{{ route('create-purchase-order') }}" class="btn pull-right btn-sm btn-info"><i class="fa fa-plus-circle"></i> CREATE NEW PURCHASE ORDER</a>
                @endif
            </h3>
             <p class="text-muted m-b-30"> List of All Purchase Order</p>
             <hr/>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                @foreach($purchase_orders as $k => $purchase_order)
                    <div class="panel">
                    <div class="panel-heading card-3" role="tab" id="heading-{{ $purchase_order->id }}">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-{{ $purchase_order->id }}" aria-expanded="false" aria-controls="collapse-{{ $purchase_order->id }}">
                                <strong class="text-info">PO # </strong><span>{{ $purchase_order->po_no_auto }} {{ $purchase_order->po_no_manual ? '/ '.$purchase_order->po_no_manual : '' }}</span> | <strong class="text-warning">Warehouse # </strong><span>{{ $purchase_order->wireHouse->name }}</span> |
                                <strong class="text-primary">VENDOR #</strong> <span>{{ $purchase_order->vendor->name }}</span> | <strong class="text-blue">DATE # </strong> <span>{{ $purchase_order->po_date }}</span> |
                                @if($purchase_order->status == 0)
                                    <span class="badge badge-info no-radius">Created</span>
                                @elseif($purchase_order->status == 1)
                                    <span class="badge badge-warning no-radius">Partially Received</span>
                                @elseif($purchase_order->status == 2)
                                    <span class="badge badge-success no-radius">Complete</span>
                                @endif
                            </a>
                        </h4>
                    </div>
                    <div id="collapse-{{ $purchase_order->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-{{ $purchase_order->id }}">
                        <div class="panel-body">
                            @if(!empty($purchase_order))
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th>Ordered Quantity</th>
                                        <th>Received Qty</th>
                                        <th>Returned Qty</th>
                                        <th>Rem Qty</th>
                                        <th>Unit Price</th>
                                        <th>Sub Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($purchase_order->purchaseOrderDetails as $od)
                                        <tr>
                                            <td>{{ $od->item->name  }}</td>
                                            <td>{{ $od->quantity }}</td>
                                            <td>{{ $od->chalanDetails ? $od->chalanDetails->sum('received_qty') : 0 }}</td>
                                            <td>{{ $od->returned_quantity }}</td>
                                            <td>{{ $od->chalanDetails ? $od->quantity - ($od->chalanDetails->sum('received_qty') + $od->returned_quantity) : ($od->quantity -  $od->returned_quantity) }}</td>
                                            <td>{{ $od->unit_price }}</td>
                                            <td>{{ $od->sub_total }}</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="6">Grand Total</td>
                                        <td>{{ $purchase_order->total_amount }}</td>
                                    </tr>

                                    </tbody>
                                </table>
                                <div class="btn-group pull-right">
                                    <button type="button" class="btn btn-info flat btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        OPTIONS <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{ route('get-purchase-order',['id' => $purchase_order->id]) }}"  data-toggle="tooltip" data-original-title="view"><i class="ti-eye"></i> DETAILS</a></li>
                                        @if(!$purchase_order->status && Auth::user()->can('purchase-order-update'))
                                            <li><a href="{{ route('edit-purchase-order',['id' => $purchase_order->id]) }}"  data-toggle="tooltip" data-original-title="edit"><i class="fa fa-pencil-square-o"></i> UPDATE</a></li>
                                        @endif

                                        @if($purchase_order->status < 2 && Auth::user()->can('purchase-challan-create'))
                                            <li><a href="{{ route('create-chalan',['id' => $purchase_order->id]) }}"  data-toggle="tooltip" data-original-title="return"><i class="fa fa-plus-circle"></i> CREATE CHALAN</a></li>
                                        @endif

                                        @if($purchase_order->status != 2 && Auth::user()->can('purchase-return-create'))
                                            <li><a href="{{ route('return-purchase-order',['id' => $purchase_order->id]) }}"  data-toggle="tooltip" data-original-title="return"><i class="ti-reload"></i> RETURN</a></li>
                                        @endif

                                    </ul>
                                </div>
                            @else
                                <p class="text-danger"><strong>Sorry! No purchase order found.</strong></p>
                            @endif



                        </div>
                    </div>
                </div>
                @endforeach
            </div>

            {{ $purchase_orders->links() }}
        </div>

    </div>

@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            //$("#purchaseOrderList").dataTable();
        });

    </script>
@endsection
