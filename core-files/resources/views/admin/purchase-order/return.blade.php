@extends('layouts.admin')

@section('content')

    <div class="content-wraper">
        <div class="white-box mb-20">
            <h3 class="box-title">Purchase Order Details <a href="{{ route('purchase-orders') }}" class="btn btn-sm btn-info pull-right"><i class="fa-fw ti-view-list"></i>Purchase Orders</a></h3>
            @if($purchase_order)
                <p>
                    <span class="text-success"><strong>PO No:</strong> {{ $purchase_order->po_no_auto }}</span>
                    <span class="text-info">| <strong>Wirehouse:</strong> {{ $purchase_order->wireHouse->name }}</span>
                    <span class="text-warning">| <strong>Vendor name</strong>: {{ $purchase_order->vendor->name }}</span>
                    <span class="text-primary">| <strong>Date:</strong> {{ $purchase_order->po_date }}</span>
                </p>
            @endif
        </div>

        <form action="{{ route('store-purchase-return',['id' => $purchase_order->id]) }}" method="post">
            {{ csrf_field() }}

            <div class="white-box">
                <h4>PURCHASE ORDER ITEM DETAILS</h4>

                <return-purchase-order-item :items="{{ $items }}"></return-purchase-order-item>

                <div class="form-group text-right">
                    <button class="btn btn-success btn-sm flat" type="submit">SAVE</button>
                </div>

            </div>


        </form>

    </div>

@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.datepicker').datepicker({
                "format": 'yyyy-mm-dd',
                "todayHighlight": true,
                "autoclose": true,
                "startDate": new Date()
            });
        });

    </script>
@endsection
