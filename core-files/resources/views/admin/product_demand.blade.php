@extends('layouts.admin')

@section('content')
<div class="white-box">
    <div class="col-mod-12">
        <div class="col-mod-6 col-lg-6">
                <h3 class="box-title m-b-0">Requisition List</h3>
                <p class="text-muted m-b-30">list of all product requisitions</p>
        </div>        
        <div class="col-mod-6 col-lg-6 ">
            <a href="{{ route('add_demand') }}" class="waves-effect pull-right"><button class="btn btn-xs btn-info pull-right"><i class="fa fa-plus"></i> Add New Requisitions</button></a>
        </div>    
    </div>  
    <div class="clear"></div><hr/>
<div class="table-responsive col-mod-12">

                                

                                <table id="myTable" class="table table-striped dataTable no-footer" role="grid" aria-describedby="myTable_info">
                                    <thead>
                                        <tr role="row">
                                            <th >Requisition From </th>
                                            <th >Requisition To</th>
                                            <th >Manager Name</th>
                                            <th >Requisition Type</th>
                                            <th >Quantity</th>
                                            <th >Unit</th>
                                            <th>Status</th>  
                                            <th>Action</th> 
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    @foreach($demands as $data)
                                        
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{ $data->fromWireHouse ? $data->fromWireHouse->name : "" }}</td>
                                        <td>{{ $data->toWireHouse ? $data->toWireHouse->name : "" }}</td> 
                                        <td>{{ $data->user ? $data->user->name : "" }}</td> 
                                        <td>{{ ($data->demand_type == 1) ? "Product" : "Product Set" }}</td>
                                        <td>{{ $data->quantity }}</td>

                                        <td>{{ $data->unit ? $data->unit->name : "" }}</td>
                                        
                                        <td>@if ($data->status == 1)
                                                Initialized
                                            @elseif ($data->status == 2)
                                                Approved
                                            @elseif ($data->status == 3)
                                                Received
                                            @elseif ($data->status == 4)
                                                Returned
                                            @else
                                                unknown
                                            @endif
                                        </td>
                                        <td>                                         
                                            <a href="edit_demand/{{ $data->id }}" type="button" class="btn btn-warning"  data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i> Edit</a>

                                            <a href="#" type="button" class="btn btn-info" data-toggle="tooltip" title="Demand Received"><i class="fa fa-check"></i> Received</a>  
                                        </td>
                                    </tr>

                                    @endforeach

                                   



                                    </tbody>
                                </table>

                                
                        </div>

@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    $('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });

    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip(); 
    });
    
</script>
@endsection