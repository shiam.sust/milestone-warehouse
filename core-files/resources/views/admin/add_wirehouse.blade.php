@extends('layouts.admin')

@section('content')
    <div class="white-box">
        <h3 class="box-title text-success m-b-15">Create Warehouse <a href="{{ route('wirehouses') }}" class="waves-effect pull-right"><button class="btn btn-sm btn-info "><i class="fa fa-arrow-circle-left"></i> ALL WAREHOUSES LIST</button></a></h3>
        <p class="text-muted m-b-30"> Create New Warehouse</p>
        <hr>
        <form action="{{ route('post.wirehouse') }}" method="post">
            {{ csrf_field() }}
            <div class="form-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Warehouse Name <span class="text-danger m-l-5">*</span></label>
                            <input type="text" id="firstName" class="form-control" placeholder="Warehouse name" name="name" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Phone <span class="text-danger m-l-5">*</span></label>
                            <input type="text" id="firstName" class="form-control" placeholder="+88 01675645158" name="phone" required>
                        </div>
                    </div>
                </div>
                <!--/row-->

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Email <span class="text-danger m-l-5">*</span></label>
                            <input type="text" id="firstName" class="form-control" placeholder="email@example.com" name="email" required>
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Mobile No. <span class="text-danger m-l-5">*</span></label>
                            <input type="text" id="firstName" class="form-control" placeholder="+88 01675645158" name="mobile" required>
                        </div>
                    </div>
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Address <span class="text-danger m-l-5">*</span></label>
                            <textarea class="form-control" name="address" placeholder="Enter Wirehouse address" required></textarea>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label">Status</label>
                            <div class="radio-list">
                                <label class="radio-inline p-0">
                                    <div class="radio radio-info">
                                        <input type="radio" id="radio1" value=1 name="status" checked>
                                        <label for="radio1">active</label>
                                    </div>
                                </label>
                                <label class="radio-inline">
                                    <div class="radio radio-info">
                                        <input type="radio" id="radio2" value=0 name="status">
                                        <label for="radio2">inactive </label>
                                    </div>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Version</label>
                            <select name="version" id="version" class="form-control">
                                <option value="">--Select Version --</option>
                                <option value="1">Bangla Medium Junior Section (BMJS)</option>
                                <option value="2">Bangla Medium Senior Section (BMSS)</option>
                                <option value="3">English Medium Junior Section (EMJS)</option>
                                <option value="4">English Medium Senior Section (EMSS)</option>
                            </select>
                        </div>
                    </div>

                </div>

            </div>
            <div class="form-group text-right">
                <button type="submit" class="btn btn-success pull-right"> <i class="fa fa-check"></i> SAVE WAREHOUSE INFORMATION</button>
            </div>
        </form>
    </div>
    </div>
@endsection