@extends('layouts.admin')

@section('stylesheets')

    <link rel="stylesheet" type="text/css" href="{{ asset('css/parsley.css') }}">
@endsection

@section('content')
    <div class="content-wraper">
        <div class="white-box">
            <h3 class="box-title m-b-0">Basic Settings</h3>
            <p class="text-muted m-b-30">Update Settings</p>
            <hr>
            <form action="{{ route('update_basic',['id' => $data->id]) }}" method="post" enctype="multipart/form-data">

                {{ csrf_field() }}
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Organization Logo</label>
                                        <input type="file" name="attach" class="form-control" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Organization Name <span class="text-danger m-l-5">*</span></label>
                                        <input type="text" id="firstName" name="name" class="form-control" value="{{ $data->name }}" placeholder="John doe" required=""></div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Phone <span class="text-danger m-l-5">*</span></label>
                                        <input type="text" id="firstName" name="phone" class="form-control" value="{{ $data->phone }}" required=""></div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Email <span class="text-danger m-l-5">*</span></label>
                                        <input type="text" id="firstName" name="email" class="form-control" value="{{ $data->email }}" required=""></div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Mobile No. <span class="text-danger m-l-5">*</span></label>
                                        <input type="text" id="firstName" name="mobile" class="form-control" value="{{ $data->mobile }}" required=""></div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Address</label>
                                        <textarea class="form-control" name="address" required="">{{ $data->address }}</textarea></div>
                                </div>

                            </div>
                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-success pull-right"> <i class="fa fa-check"></i> Update</button>
                            </div>
                        </div>

                        <div class="col-md-4 text-center">
                            <img src="{{ asset($data->image) }}"/>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <link rel="stylesheet" type="text/css" href="{{ asset('js/parsley.min.js') }}">
@endsection
