@extends('layouts.admin')

@section('content')
<div class="content-wraper">
    <div class="white-box">
        <h3 class="box-title text-success m-b-0">Create Warehouse Manager <a href="{{ route('wirehouse-manager') }}" class="btn btn-sm btn-info pull-right"><i class="fa fa-arrow-circle-left"></i> WAREHOUSE MANAGER LIST</a></h3>
        <p class="text-muted m-b-30"> Create New Warehouse Manager</p>
        <hr>
        <form action="{{ route('post.wirehouse_manager') }}" method="post">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-6 ">
                    <div class="form-group">
                        <label>Person Name <span class="text-danger m-l-5">*</span></label>
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}" required>
                        @if ($errors->has('name'))
                            <span class="help-block text-danger">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Wirehouse <span class="text-danger m-l-5">*</span></label>
                        <select class="form-control" data-placeholder="Choose a Category" tabindex="1" name="wh_id" required>
                            <option>---select warehouse---</option>>
                            @foreach($wirehouses as $data)
                                <option value="{{ $data->id }}" {{ $data->id == old('wh_id') ? 'selected="selected"' : '' }}>{{ $data->name }}</option>

                            @endforeach
                        </select>
                        @if ($errors->has('wh_id'))
                            <span class="help-block text-danger">
                                <strong>{{ $errors->first('wh_id') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Mobile No.</label>
                        <input type="text" class="form-control" name="mobile" value="{{ old('mobile') }}">
                    </div>
                @if ($errors->has('mobile'))
                    <span class="help-block text-danger">
                            <strong>{{ $errors->first('mobile') }}</strong>
                        </span>
                @endif
                </div>
                <!--/span-->
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" name="email" value="{{ old('email') }}" > </div>
                    @if ($errors->has('email'))
                        <span class="help-block text-danger">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <!--/span-->
            </div>
            <!--/row-->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Address</label>
                        <input type="textarea" class="form-control" name="address" value="{{ old('address') }}"> </div>
                    @if ($errors->has('address'))
                        <span class="help-block text-danger">
                            <strong>{{ $errors->first('address') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Designation</label>
                        <input type="text" class="form-control" name="designation" value="{{ old('designation') }}"> </div>
                        @if ($errors->has('designation'))
                            <span class="help-block text-danger">
                                <strong>{{ $errors->first('designation') }}</strong>
                            </span>
                        @endif
                </div>
                <!--/span-->
            </div>
            <div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Username <span class="text-danger m-l-5">*</span></label>
                        <input type="text" class="form-control"
                               name="username" value="{{ old('username') }}" required>
                        @if ($errors->has('username'))
                            <span class="help-block text-danger">
                                <strong>{{ $errors->first('username') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Password <span class="text-danger m-l-5">*</span></label>
                        <input type="password" class="form-control"
                               name="password" required>
                    </div>
                </div>
            </div>
            <div class="form-group text-right">
                <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> SAVE WAREHOUSE MANAGER INFO</button>
            </div>
        </form>
    </div>
</div>
</div>
@endsection