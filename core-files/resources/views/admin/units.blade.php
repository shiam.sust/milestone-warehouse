@extends('layouts.admin')

@section('content')

    <section class="section section-designation-list">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-sm-12">
                    <div class="white-box">
                        <h3 class="box-title m-b-0">
                            UNIT OF MEASUREMENT LIST
                            <button type="button" class="btn btn-info pull-right" data-toggle="modal" data-target="#create-unit" data-whatever="@fat">CREATE UNIT</button>
                        </h3>
                        <p class="text-muted m-b-30">List of all unit you cas use on your inventory</p>
                        <div class="table-responsive">
                            <table id="example" class="table display">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>UNIT</th>
                                    <th class="text-right">ACTION</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($units as $unit)
                                    <tr>
                                        <td>{{ $unit->id }}</td>
                                        <td>{{ $unit->name }}</td>
                                        <td class="text-right">
                                            <button type="button" data-id="{{ $unit->id }}" data-toggle="modal" data-target="#update-unit" class="btn btn-warning btn-edit waves-effect waves-light"> <i class="fa fa-times m-r-5"></i> <span>EDIT</span></button>
                                            <a href="#" class="btn btn-danger waves-effect waves-light"><i class="fa fa-times m-r-5"></i> <span>DELETE</span></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade in" id="create-unit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{ route('unit') }}" method="post">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="exampleModalLabel1">CREATE UNIT</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name" class="control-label">UNIT OF MEASUREMENT *</label>
                            <input type="text" class="form-control" id="name" name="name" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">CREATE</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade in" id="update-unit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="#" method="post" id="update-unit-form">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="exampleModalLabel1">UPDATE UNIT</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name" class="control-label">UNIT OF MEASUREMENT *</label>
                            <input type="text" class="form-control" id="name" name="name" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">CLOSE</button>
                        <button type="submit" class="btn btn-primary">UPDATE</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {

        });
    </script>
@endsection