@extends('layouts.admin')

@section('content')

    <div class="content-wraper">
        <div class="white-box mb-20">
            <h3 class="box-title">
                CREATE INITIAL PRODUCT QUANTITY
                <a href="{{ route('balance-product-set') }}" class="btn btn-info btn-sm pull-right"><i class="fa fa-fw fa-list"></i>Stocked Product Set</a>
            </h3>
        </div>

        <div class="white-box">
            <form action="{{ route('store-initial-product-set-quantity') }}" method="post">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Select Warehouse <span class="text-danger">*</span></label>
                            <select name="wh_id" id="wh_id" class="form-control" required>
                                @foreach($warehouses as $warehouse)
                                    <option value="{{ $warehouse->id }}">{{ $warehouse->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <create-initial-item :items="{products: {{ $product_sets }},units: {{ $units }}}"></create-initial-item>

            </form>
        </div>

    </div>

@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {

        });

    </script>
@endsection
