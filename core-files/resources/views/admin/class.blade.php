@extends('layouts.admin')

@section('content')
    <div class="white-box">
        <div class="col-mod-12">
            <div class="col-mod-6 col-lg-6">
                <h3 class="box-title text-success m-b-0">Class</h3>
                <p class="text-muted m-b-30">List of all Class</p>
            </div>
            <div class="col-mod-6 col-lg-6 ">
                @if(Auth::user()->can('class-create'))
                    <a href="{{ route('add_class') }}" class="waves-effect pull-right"><button class="btn btn-xs btn-info pull-right"><i class="fa fa-plus-circle"></i> ADD NEW CLASS</button></a>
                @endif
            </div>
        </div>
        <div class="clear"></div><hr/>
        <div class="table-responsive col-mod-12">



            <table id="myTable" class="table table-bordered table-striped dataTable no-footer" role="grid" aria-describedby="myTable_info">
                <thead>


                <tr role="row">
                    <th >SL </th>
                    <th >Class Name </th>
                    <th >Version</th>
                    <th >Status</th>


                    <th>Action</th>

                </tr>
                </thead>
                <tbody>


                @foreach($academic_classes as $k => $data)
                    @if($data->status == 0)
                        @continue
                    @endif

                    <tr role="row" class="odd">
                        <td class="sorting_1">{{ $k+1 }}</td>
                        <td>{{ $data->name }}</td>
                        <td>
                            @if( $data->version == 1 )
                                Bangla Medium Junior Section (BMJS)
                            @elseif( $data->version == 2 )
                                Bangla Medium Senior Section (BMSS)
                            @elseif( $data->version == 3 )
                                English Medium Junior Section (EMJS)
                            @elseif( $data->version == 4 )
                                English Medium Senior Section (EMSS)
                            @else

                            @endif
                        </td>
                        <td>{{ $data->status ? "Active" : "Inactive" }}</td>

                        <td>
                            @if(Auth::user()->can('class-update'))
                                <a href="edit_class/{{ $data->id }}" type="button" class="btn btn-warning btn-xs"  data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i> UDPATE</a>
                            @endif

                            @if(Auth::user()->can('class-delete'))
                            <a href="delete_class/{{ $data->id }}" type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Delete"><i class="fa fa-times-circle"></i> DELETE
                            </a>
                            @endif
                        </td>
                    </tr>

                @endforeach



                </tbody>
            </table>


        </div>

        @endsection

        @section('script')
            <script type="text/javascript">
                $(document).ready(function() {
                    $('#myTable').DataTable();
                    $(document).ready(function() {
                        var table = $('#example').DataTable({
                            "columnDefs": [{
                                "visible": false,
                                "targets": 2
                            }],
                            "order": [
                                [2, 'asc']
                            ],
                            "displayLength": 25,
                            "drawCallback": function(settings) {
                                var api = this.api();
                                var rows = api.rows({
                                    page: 'current'
                                }).nodes();
                                var last = null;
                                api.column(2, {
                                    page: 'current'
                                }).data().each(function(group, i) {
                                    if (last !== group) {
                                        $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                                        last = group;
                                    }
                                });
                            }
                        });
                        // Order by the grouping
                        $('#example tbody').on('click', 'tr.group', function() {
                            var currentOrder = table.order()[0];
                            if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                                table.order([2, 'desc']).draw();
                            } else {
                                table.order([2, 'asc']).draw();
                            }
                        });
                    });
                });
                $('#example23').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                });

                $(document).ready(function(){
                    $('[data-toggle="tooltip"]').tooltip();
                });

            </script>
@endsection