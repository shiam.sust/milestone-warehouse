@extends('layouts.admin')

@section('content')
    <div class="white-box">
        <h3 class="box-title m-b-0">Product Set <a href="{{ route('product_set') }}" class="waves-effect pull-right btn btn-sm btn-info" ><i class="fa fa-arrow-circle-left"></i> All Product Set</a></h3>
        <p class="text-muted m-b-30">Create New Product Set </p>
        <hr>
        <form action="{{ route('update.product_set',['id' => $product_set->id]) }}" method="post">

            {{ csrf_field() }}

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Product Set Name<span class="text-danger m-1-5">*</span></label>
                        <input type="text" id="firstName" class="form-control" placeholder="John doe" name="name" value="{{ $product_set->name }}" required>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Class<span class="text-danger m-1-5">*</span></label>
                        <select class="form-control" data-placeholder="Choose a Category" tabindex="1" name="academic_class" required="">
                            <option value="">--Select Class--</option>
                            @foreach($class as $data)

                                <option value="{{ $data->id }}" {{ $product_set->class_id == $data->id ? 'selected="selected"' : '' }}>
                                    {{ $data->name }}
                                </option>

                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Status<span class="text-danger m-1-5">*</span></label>
                        <div class="radio-list">
                            <label class="radio-inline p-0">
                                <div class="radio radio-info">
                                    <input type="radio" name="status" id="radio1" value="1" {{ $product_set->status ? 'checked' : ''}}>
                                    <label for="radio1">active</label>
                                </div>
                            </label>
                            <label class="radio-inline">
                                <div class="radio radio-info">
                                    <input type="radio" name="status" id="radio2" value="0" {{ !$product_set->status ? 'checked' : ''}}>
                                    <label for="radio2">inactive </label>
                                </div>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <edit-product-set :items="{{ $products }}"></edit-product-set>

            <div class="form-group text-right">
                <button type="submit" class="btn btn-success pull-right"> <i class="fa fa-check"></i> UPDATE PRODUCT SET INFO</button>
            </div>

        </form>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {


        });
    </script>
@endsection         