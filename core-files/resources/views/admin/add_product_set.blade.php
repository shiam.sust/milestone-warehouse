@extends('layouts.admin')

@section('content')
<div class="white-box">
    <h3 class="box-title text-success m-b-0">Product Set <a href="{{ route('product_set') }}" class="waves-effect pull-right btn btn-sm btn-info" ><i class="fa fa-arrow-circle-left"></i> ALL PRODUCT SET LIST</a></h3>
    <p class="text-muted m-b-30">Create New Product Set </p>
    <hr>
    <form action="{{ route('post.product_set') }}" method="post">

        {{ csrf_field() }}

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Product Set Name<span class="text-danger m-1-5">*</span></label>
                    <input type="text" id="firstName" class="form-control" placeholder="John doe" name="name" required="">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Class<span class="text-danger m-1-5">*</span></label>
                    <select class="form-control" data-placeholder="Choose a Category" tabindex="1" name="academic_class" required="">
                        <option value="">--Select Class--</option>
                        @foreach($class as $data)

                            <option value="{{ $data->id }}">
                                {{ $data->name }} [{{ $data->classVersion() }}]
                            </option>

                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label">Status<span class="text-danger m-1-5">*</span></label>
                    <div class="radio-list">
                        <label class="radio-inline p-0">
                            <div class="radio radio-info">
                                <input type="radio" name="status" id="radio1" checked value=1 required="">
                                <label for="radio1">active</label>
                            </div>
                        </label>
                        <label class="radio-inline">
                            <div class="radio radio-info">
                                <input type="radio" name="status" id="radio2" value=0>
                                <label for="radio2">inactive </label>
                            </div>
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <create-product-set :items="{products: {{ $products }},units: {{ $units }}}"></create-product-set>

        <div class="form-group text-right">
            <button type="submit" class="btn btn-success pull-right"> <i class="fa fa-check"></i> SAVE PRODUCT SET INFORMATION</button>
        </div>

    </form>
</div>
@endsection
@section('script')
<script type="text/javascript">
        $(document).ready(function() {
            
          
            // Switchery
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            $('.js-switch').each(function() {
                new Switchery($(this)[0], $(this).data());
            });
                   
           /// For multiselect
            $('#pre-selected-options').multiSelect();
            $('#optgroup').multiSelect({
                selectableOptgroup: true
            });
            $('#public-methods').multiSelect();
            $('#select-all').click(function() {
                $('#public-methods').multiSelect('select_all');
                return false;
            });
            $('#deselect-all').click(function() {
                $('#public-methods').multiSelect('deselect_all');
                return false;
            });
            $('#refresh').on('click', function() {
                $('#public-methods').multiSelect('refresh');
                return false;
            });
            $('#add-option').on('click', function() {
                $('#public-methods').multiSelect('addOption', {
                    value: 42,
                    text: 'test 42',
                    index: 0
                });
                return false;
            });
        });
        </script>
@endsection         