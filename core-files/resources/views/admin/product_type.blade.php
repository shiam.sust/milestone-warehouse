@extends('layouts.admin')

@section('content')
<div class="white-box">
    <div class="col-mod-12">
        <div class="col-mod-6 col-lg-6">
                <h3 class="box-title text-success m-b-0">Product Type</h3>
                <p class="text-muted m-b-30"> List of all product type</p>
                <hr/>
        </div>        
        <div class="col-mod-6 col-lg-6 ">
            <a href="{{ route('add_product_type') }}" class="waves-effect pull-right"><button class="btn btn-xs btn-info pull-right"><i class="fa fa-plus-circle"></i> Add New Product Type</button></a>
        </div>    
    </div>  
    <div class="clear"></div><hr/>
<div class="table-responsive col-mod-12">

                                

                                <table id="myTable" class="table table-bordered table-striped dataTable no-footer" role="grid" aria-describedby="myTable_info">
                                    <thead>
                                        <tr role="row">
                                            <th >SL </th>
                                            <th >Product Type Name </th>
                                            <th >Status</th>
                                            
                                            
                                           <th>Action</th> 
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        
                                        
                                     
                                        
                                    @foreach($product_types as $k => $row)
                                    
                                    @if($row->status == 0)
                                        @continue
                                    @endif 
                                        
                                    <tr role="row" class="odd">
                                            <td>{{ $k+1 }}</td>
                                            <td>{{ $row->name }}</td>
                                            <td>{{ $row->status ? "Active" : "Inactive" }}</td>
                                            
                                           
                                            <td>                                         
                                                <a href="show_edit_product_type/{{ $row->id }}" type="button" class="btn btn-warning btn-xs"  data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i> UPDATE</a>

                                                <a href="delete_product_type/{{ $row->id }}" type="button"   class="btn btn-danger btn-xs" data-toggle="tooltip" title="Delete"><i class="fa fa-times-circle"></i> DELETE
                                                </a>  
                                            </td>
                                        </tr>

                                    @endforeach

                                    </tbody>
                                </table>

                                 
                        </div>

@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });


        // Product Type CRUD
        $('.btn-edit').click(function () {
            var id = $(this).data('id');
            var url = "{{ route('get-category') }}/"+id;
            var formUrl = "{{ route('update-category') }}/"+id;

            $.ajax({url: url, success: function(result){
                $("#edit-category-form").attr('action',formUrl);
                $("#edit-category-form input[name='name']").val(result.name);
            }});
        });
    });
    $('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });

    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip(); 
    });


    
</script>
@endsection