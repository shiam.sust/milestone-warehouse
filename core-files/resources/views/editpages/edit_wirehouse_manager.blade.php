@extends('layouts.admin')

@section('content')
    <div class="white-box">
        <h3 class="box-title text-success m-b-0">Update Warehouse Manager <a href="{{ route('wirehouse-manager') }}" class="waves-effect pull-right"><button class="btn btn-sm btn-info "><i class="fa fa-arrow-circle-left"></i> WAREHOUSE MANAGER LIST</button></a></h3>
        <p class="text-muted m-b-30"> Update Warehouse Manager info</p>
        <hr>
        <form action="{{ route('update.wirehouse_manager',['id' => $manager->id]) }}" method="post">
            {{ csrf_field() }}
            <div class="form-body">
                <div class="row">
                    <div class="col-md-6 ">
                        <div class="form-group">
                            <label>Person Name <span class="text-danger m-l-5">*</span></label>
                            <input type="text" class="form-control" name="name" value="{{ $manager->name }}" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Select Warehouse <span class="text-danger m-l-5">*</span></label>
                            <select class="form-control" data-placeholder="Choose a Category" tabindex="1" name="wh_id" required>
                                @foreach($wirehouses as $data)
                                    <option value="{{ $data->id }}" {{ $manager->wireHouse->id == $data->id ? 'selected="selected"' : '' }}>{{ $data->name }}</option>
                                @endforeach

                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Mobile No.</label>
                            <input type="text" class="form-control" name="mobile" value="{{ $manager->mobile }}">
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" class="form-control" name="email" value="{{ $manager->email }}">
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Address</label>
                            <input type="textarea" class="form-control" name="address" value="{{ $manager->address }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Designation</label>
                            <input type="text" class="form-control" name="designation" value="{{ $manager->designation }}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Username <span class="text-danger m-l-5">*</span></label>
                            <input type="text" class="form-control"
                                   name="username" value="{{ old('username') }}" required>
                            @if ($errors->has('username'))
                                <span class="help-block text-danger">
                                <strong>{{ $errors->first('username') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Password</label>
                            <input id="password" name="password" type="password" class="form-control" >

                            @if ($errors->has('password'))
                                <span class="help-block text-danger">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

            </div>
            <div class="form-group text-right">
                <button type="submit" class="btn btn-success pull-right"> <i class="fa fa-check"></i> UPDATE MANAGER INFORMATION</button>
            </div>
        </form>
    </div>
    </div>
@endsection