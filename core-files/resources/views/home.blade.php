@extends('layouts.admin')

@section('title')
    <div>
        <div class="content-wraper">
            <div class="white-box">
                <div class="row">
                    <div class="col-lg-6">
                     <h4 class="box-title">
                        <img src="{{ asset('public/'.$settings->image) }}" alt="" class="dashboard-logo" width="35px">
                        {{ $settings ? $settings->name : 'MILESTONE SCHOOL AND COLLEGE' }}
                     </h4>
                     <hr/>
                      {{ $settings->email}} <br/>
                     {{ $settings->phone}} <br/>
                     {{ $settings->address}} <br/>
                    </div>
                    <div class="col-lg-6">
                        <p class="text-info box-title">Warehouse information</p><hr/>
                         {{ Auth::user()->warehouse->name }} <br/>
                        Email: {{ Auth::user()->warehouse->email }} <br/>
                        Phone: {{ Auth::user()->warehouse->phone }} <br/>
                        Address: {{ Auth::user()->warehouse->address }} <br/>
                    </div>
                </div>
               

            </div>

        </div>
        <div class="content-wraper">
            <div class="row">
                <div class="col-lg-3 col-sm-6 col-xs-12">
                    <div class="white-box analytics-info">
                        <h5><a href="{{ route('basicsettings') }}"><i class="dashboard-icon fa-fw ti-settings text-info"></i>BASIC SETTINGS</a></h5>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-xs-12">
                    <div class="white-box analytics-info">
                        <h5><a href="{{ route('product_type') }}"><i class="dashboard-icon fa-fw ti-layout-accordion-list text-info"></i>PRODUCT TYPE</a></h5>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-xs-12">
                    <div class="white-box analytics-info">
                        <h5><a href="{{ route('product_unit') }}"><i class="dashboard-icon fa-fw ti-layers text-info"></i>PRODUCT UNIT</a></h5>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-xs-12">
                    <div class="white-box analytics-info">
                        <h5><a href="{{ route('product') }}"><i class="dashboard-icon fa-fw ti-clipboard text-info"></i>PRODUCT</a></h5>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-xs-12">
                    <div class="white-box analytics-info">
                        <h5><a href="{{ route('product_set') }}"><i class="dashboard-icon fa-fw ti-server text-info"></i>PRODUCT SET</a></h5>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-xs-12">
                    <div class="white-box analytics-info">
                        <h5><a href="{{ route('class') }}"><i class="dashboard-icon fa-fw ti-star text-info"></i>CLASS</a></h5>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-xs-12">
                    <div class="white-box analytics-info">
                        <h5><a href="{{ route('wirehouses') }}"><i class="dashboard-icon fa-fw ti-home text-info"></i>WAREHOUSE</a></h5>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-xs-12">
                    <div class="white-box analytics-info">
                        <h5><a href="{{ route('wirehouse-manager') }}"><i class="dashboard-icon fa-fw ti-user text-info"></i>WAREHOUSE MANAGER</a></h5>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-xs-12">
                    <div class="white-box analytics-info">
                        <h5><a href="{{ route('vendors') }}"><i class="dashboard-icon fa-fw ti-anchor text-info"></i>VENDOR</a></h5>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-xs-12">
                    <div class="white-box analytics-info">
                        <h5><a href="{{ route('purchase-orders') }}"><i class="dashboard-icon fa-fw ti-stats-up text-info"></i>PURCHASE ORDER</a></h5>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-xs-12">
                    <div class="white-box analytics-info">
                        <h5><a href="{{ route('product_demand') }}"><i class="dashboard-icon fa-fw ti-stats-down text-info"></i>PRODUCT REQUISITION</a></h5>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-xs-12">
                    <div class="white-box analytics-info">
                        <h5><a href="{{ route('chalans') }}"><i class="dashboard-icon fa-fw ti-files text-info"></i>CHALAN</a></h5>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-xs-12">
                    <div class="white-box analytics-info">
                        <h5><a href="{{ route('payments') }}"><i class="dashboard-icon fa-fw ti-text text-info"></i>PAYMENT</a></h5>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
