<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductDemand extends Model
{
    protected $fillable = [
        'status',
        'requisition_date',
        'req_no_auto',
        'req_no_manual',
        'from_warehouse',
        'to_warehouse',
        'created_by',
    ];

    public function product(){
        return $this->belongsTo('App\Model\Product\Product','product_id','id');
    }

    public function productSet(){
        return $this->belongsTo('App\Model\Product\ProductSet','product_id','id');
    }

    public function fromWireHouse(){
        return $this->belongsTo('App\Model\WireHouse\WireHouse','from_warehouse','id');
    }

    public function toWireHouse(){
        return $this->belongsTo('App\Model\WireHouse\WireHouse','to_warehouse','id');
    }


    public function user(){
        return $this->belongsTo('App\User','created_by','id');
    }

    public function productDemandDetails(){
        return $this->hasMany('App\Model\Product\ProductDemandDetail','product_demand_id','id');
    }

}
