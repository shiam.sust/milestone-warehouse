<?php

namespace App\Providers;

use App\Repository\AssetDetailRepository;
use App\Repository\AssetRepository;
use App\Repository\CategoryRepository;
use App\Repository\ClientRepository;
use App\Repository\Eloquent\EloquentAsset;
use App\Repository\Eloquent\ELoquentAssetDetail;
use App\Repository\Eloquent\EloquentCategory;
use App\Repository\Eloquent\EloquentClient;
use App\Repository\Eloquent\EloquentEmployee;
use App\Repository\Eloquent\EloquentInvoice;
use App\Repository\Eloquent\EloquentInvoiceAssetDetail;
use App\Repository\Eloquent\EloquentManufacturer;
use App\Repository\Eloquent\EloquentOrganization;
use App\Repository\Eloquent\EloquentPermission;
use App\Repository\Eloquent\EloquentProduct;
use App\Repository\Eloquent\EloquentRole;
use App\Repository\Eloquent\EloquentTransaction;
use App\Repository\Eloquent\EloquentUnit;
use App\Repository\Eloquent\EloquentUser;
use App\Repository\Eloquent\EloquentVendor;
use App\Repository\EmployeeRepository;
use App\Repository\InvoiceAssetDetailRepository;
use App\Repository\InvoiceRepository;
use App\Repository\ManufacturerRepository;
use App\Repository\OrganizationRepository;
use App\Repository\PermissionRepository;
use App\Repository\ProductRepository;
use App\Repository\RoleRepository;
use App\Repository\TransactionRepository;
use App\Repository\UnitRepository;
use App\Repository\UserRepository;
use App\Repository\VendorRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ProductTypeRepository::class, EloquentCategory::class);
        $this->app->bind(UnitRepository::class, EloquentUnit::class);
        $this->app->bind(ManufacturerRepository::class, EloquentManufacturer::class);
        $this->app->bind(ProductRepository::class, EloquentProduct::class);
    }
}
