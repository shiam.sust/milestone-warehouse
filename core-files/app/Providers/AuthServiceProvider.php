<?php

namespace App\Providers;

use App\Role;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        $this->registerPolicies();
        foreach ($this->getPermissions() as $permission){
            Gate::define($permission->slug, function ($user) use($permission){
                return $user->is_admin ? true : $user->hasRole($permission->id);
            });
        }
    }
    private function getPermissions(){
        return Role::all();
    }
}
