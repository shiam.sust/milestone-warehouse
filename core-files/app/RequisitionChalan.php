<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequisitionChalan extends Model
{
    protected $fillable = [
        'demand_id',
        'ch_no_auto',
        'ch_no_manual',
        'ch_date',
        'status',
        'wh_id'
    ];

    public function wireHouse(){
        return $this->belongsTo('App\Model\WireHouse\WireHouse','wh_id','id');
    }

    public function demand(){
        return $this->belongsTo('App\ProductDemand','demand_id','id');
    }

    public function chalanDetails(){
        return $this->hasMany('App\RequisitionChalanDetail','req_chalan_id','id');
    }
}
