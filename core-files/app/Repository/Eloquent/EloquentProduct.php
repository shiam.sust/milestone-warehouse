<?php
/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 24-Apr-18
 * Time: 7:20 AM
 */

namespace App\Repository\Eloquent;


use App\Model\Product\Product;
use App\Repository\ProductRepository;

class EloquentProduct extends EloquentBase implements ProductRepository
{
    public function __construct(Product $product)
    {
        $this->model = $product;
    }

}