<?php
/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 24-Apr-18
 * Time: 7:20 AM
 */

namespace App\Repository\Eloquent;


use App\Model\Product\Manufacturer;
use App\Repository\ManufacturerRepository;

class EloquentManufacturer extends EloquentBase implements ManufacturerRepository
{
    public function __construct(Manufacturer $manufacturer)
    {
        $this->model = $manufacturer;
    }

}