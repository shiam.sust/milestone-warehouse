<?php
/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 24-Apr-18
 * Time: 7:17 AM
 */

namespace App\Repository\Eloquent;


use App\Model\Product\ProductType;
use App\Repository\CategoryRepository;

class EloquentCategory extends EloquentBase implements CategoryRepository
{
    public function __construct(ProductType $product_type)
    {
        $this->model = $product_type;
    }

}