<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class Student extends Model
{

	protected $fillable = [
        'year',
        'quantity',
        'status',
        'class_id',
        'wh_id'
    ];

    public function wirehouse(){
        return $this->belongsTo('App\Model\WireHouse\WireHouse','wh_id','id');
    }

    public function studentClass(){
    	return $this->belongsTo('App\Model\Classes\AcademicClass','class_id','id');
    }
}
