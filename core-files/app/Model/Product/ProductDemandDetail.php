<?php

namespace App\Model\Product;

use Illuminate\Database\Eloquent\Model;

class ProductDemandDetail extends Model
{
    protected $fillable = ['product_demand_id','item_id','unit_id','quantity','item_type','returned_qty'];
    protected $appends = ['item'];

    public function product(){
        return $this->belongsTo('App\Model\Product\Product','item_id','id');
    }
    public function unit(){
        return $this->belongsTo('App\Model\Product\Unit','unit_id','id');
    }

    public function productSet(){
        return $this->belongsTo('App\Model\Product\ProductSet','item_id','id');
    }

    public function chalanDetails(){
        return $this->hasMany('App\RequisitionChalanDetail','p_req_d_id','id');
    }

    public function getItemAttribute(){
        return $this->item_type == 1 ? $this->product : $this->productSet;
    }
}
