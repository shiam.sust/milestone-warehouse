<?php

namespace App\Model\Product;

use Illuminate\Database\Eloquent\Model;

class ProductSetDetails extends Model
{
    protected $fillable = [ 'product_set_id', 'product_id','unit_id','quantity' ];

    public function productSet(){
        return $this->belongsTo('App\Model\Product\ProductSet','product_set_id','id');
    }

    public function product(){
        return $this->belongsTo('App\Model\Product\Product','product_id','id');
    }

    public function unit(){
        return $this->belongsTo('App\Model\Product\Unit','unit_id','id');
    }

 }
