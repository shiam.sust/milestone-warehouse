<?php

namespace App\Model\Product;

use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
    protected $fillable = ['name','status'];

    public function products(){
    	return $this->hasMany('App\Model\Product\Product','type_id','id');
    }

}
