<?php

namespace App\Model\Product;

use Illuminate\Database\Eloquent\Model;

class ProductSet extends Model
{
    protected $fillable = [ 'name', 'status' ,'class_id','base_unit'];

    protected $appends = ['item_type'];

    public function productSetDetails(){
    	return $this->hasMany('App\Model\Product\ProductSetDetails','product_set_id','id');
    }

    public function setProducts(){
    	return $this->belongsToMany('App\Model\Product\Product','product_set_details','product_set_id','product_id')
            ->withPivot(
                'quantity',
                'unit_id'
            )->withTimestamps();
    }

    public function unit(){
        return $this->belongsTo('App\Mode\Product\Unit','base_unit','id');
    }

    public function purchaseDetails(){
        return $this->hasMany('App\Model\PurchaseOrderDetail','product_set_id','id');
    }
    public function academicClass(){
        return $this->belongsTo('App\Model\Classes\AcademicClass','class_id','id');
    }

    public function getItemTypeAttribute(){
        return 2;
    }
}
