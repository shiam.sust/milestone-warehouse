<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = ['chalan_id','payment_method_id','amount','due','is_fixed','attachment','discount','status','payment_date'];

    public function chalan(){
        return $this->belongsTo('App\Model\Chalan','chalan_id','id');
    }

    public function paymentMethod(){
        return $this->belongsTo('App\PaymentMethod','payment_method_id','id');
    }
}
