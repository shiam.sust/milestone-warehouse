<?php

namespace App\Model\ProductReturn;

use Illuminate\Database\Eloquent\Model;

class ProductReturn extends Model
{
    protected $fillable = [
        'ret_no_auto',
        'ret_no_manual',
        'from_warehouse',
        'to_warehouse',
        'return_date',
        'created_by',
        'status',
    ];

    public function fromWireHouse(){
        return $this->belongsTo('App\Model\WireHouse\WireHouse','from_warehouse','id');
    }

    public function toWireHouse(){
        return $this->belongsTo('App\Model\WireHouse\WireHouse','to_warehouse','id');
    }


    public function user(){
        return $this->belongsTo('App\User','created_by','id');
    }

    public function productReturnDetails(){
        return $this->hasMany('App\Model\ProductReturn\ProductReturnDetail','product_ret_id','id');
    }
}
