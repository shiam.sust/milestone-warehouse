<?php

namespace App\Model\ProductReturn;

use Illuminate\Database\Eloquent\Model;

class ProductReturnChalanDetail extends Model
{
    protected $fillable = [
        'ret_chalan_id',
        'item_id',
        'item_type',
        'unit_id',
        'quantity',
        'p_ret_d_id'
    ];

    protected $appends = ['item'];

    public function product(){
        return $this->belongsTo('App\Model\Product\Product','item_id','id');
    }
    public function unit(){
        return $this->belongsTo('App\Model\Product\Unit','unit_id','id');
    }

    public function productSet(){
        return $this->belongsTo('App\Model\Product\ProductSet','item_id','id');
    }
    public function getItemAttribute(){
        return $this->item_type == 1 ? $this->product : $this->productSet;
    }
}
