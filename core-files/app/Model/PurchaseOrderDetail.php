<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrderDetail extends Model
{
    protected $fillable = [
        'po_id',
        'quantity',
        'unit_price',
        'base_unit',
        'item_type',
        'product_id',
        'product_set_id',
        'returned_quantity',
        'item_type'
    ];

    protected $appends = ['sub_total','item'];

    public function purchaseOrder(){
        return $this->belongsTo('App\Model\PurchaseOrder','po_id','id');
    }


    public function product(){
        return $this->belongsTo('App\Model\Product\Product','product_id','id');
    }
    public function unit(){
        return $this->belongsTo('App\Model\Product\Unit','base_unit','id');
    }

    public function productSet(){
        return $this->belongsTo('App\Model\Product\ProductSet','product_set_id','id');
    }

    public function chalanDetails(){
        return $this->hasMany('App\Model\ChalanDetail','purchase_order_details_id','id');
    }

    public function getSubTotalAttribute(){
        return $this->unit_price * $this->quantity;
    }

    public function getItemAttribute(){
        return $this->item_type == 1 ? $this->product : $this->productSet;
    }
}
