<?php

namespace App\Model\WireHouse;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $fillable = ['name', 'mobile', 'email', 'address', 'invoiceaddress','status'];

    public function vendorContact(){
    	return $this->hasMany('App\Model\WireHouse\VendorContact', 'vd_id', 'id');
    } 
}
