<?php

namespace App\Model\WireHouse;

use Illuminate\Database\Eloquent\Model;

class VendorContact extends Model
{
    protected $fillable = ['name', 'vd_id', 'mobile', 'designation'];

    public function vendor(){
    	return $this->belongsTo('App\Model\WireHouse\Vendor', 'vd_id', 'id');
    } 
}
