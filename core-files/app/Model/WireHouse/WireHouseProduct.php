<?php

namespace App\Model\WireHouse;

use App\ProductDemand;
use Illuminate\Database\Eloquent\Model;

class WireHouseProduct extends Model
{
    protected $fillable = ['product_id','quantity','wh_id','status','initial_quantity','stock_balance'];


    public function product(){
        return $this->belongsTo('App\Model\Product\Product','product_id','id');
    }

    public function warehouse(){
        return $this->belongsTo('App\Model\WireHouse\WireHouse','wh_id','id');
    }




}
