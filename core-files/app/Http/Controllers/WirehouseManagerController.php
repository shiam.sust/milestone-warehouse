<?php

namespace App\Http\Controllers;

use App\Model\WireHouse\WireHouseContact;
use App\Model\WireHouse\WireHouse;
use App\Role;
use Illuminate\Http\Request;
use App\User;
use DB;

class WirehouseManagerController extends Controller
{
    public function index(){
    	$managers = WireHouseContact::all();

    	return view('admin.wirehouse-manager.index')->with([
    		'managers' => $managers
    	]);
    }

    public function showWireHouseManagerForm(){
    	$wirehouses = WireHouse::where('status','>',0)->get();

    	return view('admin.add_wirehouse_manager')->with([
    		'wirehouses' => $wirehouses
    	]);
    }

    public function store(Request $request){

        //return $request->all();

        $this->validate($request, array(
            'name' => 'required|max:255',
            'username' => 'required',
            'password' => 'required',
            'wh_id'     => 'required'
        ));

        $manager = new WireHouseContact;

        $manager->name = $request->name;
        $manager->wh_id = $request->wh_id;
        $manager->mobile = $request->mobile;
        $manager->email = $request->email;
        $manager->designation = $request->designation;
        $manager->username = $request->username;
        $manager->address = $request->address;
        
        $manager->save();

        $user = new User;

        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->status = 1;
        $user->wh_id = $request->wh_id;
        $user->username = $request->username;
        $user->manager_id = $manager->id;

        $user->save();

        

        return redirect()->route('wirehouse-manager')->withMessage([
            'status'    => 'alert-success',
            'text'      => 'Successfully created warehouse manager.'
        ]);
    
    }

    public function delete($id){
        
        DB::table('wire_house_contacts')
                    ->where('id',$id)
                    ->update(['status' => -1]);

        return redirect()->route('wirehousemanager')->withMessage([
            'status'    => 'alert-success',
            'text'      => 'Successfully deleted warehouse manager.'
        ]);
        
    }

    public function showUpdate($id){
        $wirehouses = WireHouse::all();
        $manager = WireHouseContact::find($id);
        return view('editpages.edit_wirehouse_manager')->with([
            'manager'   => $manager,
            'wirehouses' => $wirehouses
        ]);
    }

    public function update(Request $request, $id){

        $this->validate($request, array(
            'name' => 'required|max:255',
            'wh_id' => 'required',
        ));


        $manager = WireHouseContact::findOrFail($id);
        $manager->update($request->only([
            'name',
            'wh_id',
            'designation',
            'address',
            'email',
            'mobile',
            'username'
        ]));

        $user = User::where('manager_id',$manager->id)->first();

        $user->update([
            'name'      => $request->input('name'),
            'email'     => $request->input('email'),
            'wh_id'     => $request->input('wh_id'),
            'username'  => $request->input('username'),
            'manager_id'=> $manager->id
        ]);

        if($request->password){
            $user->fill([
                'password'  => bcrypt($request->input('password')),
            ])->save();
        }

        return redirect()->route('wirehouse-manager')->withMessage([
            'status'    => 'alert-success',
            'text'      => 'Successfully updated warehouse manager.'
        ]);
    }

    public function editRole($id){
        $manager = WireHouseContact::findOrFail($id);
        $user = User::where('manager_id',$manager->id)->first();
        if(!$user->mainBranch()){
            return redirect()->back()->withMessage([
                'status'    => 'alert-danger',
                'text'      => 'This manager does not need to attach role.'
            ]);
        }
        $roles = Role::orderBy('name','asc')->get();
        return view('admin.wirehouse-manager.edit-roles')->with([
            'user'  => $user,
            'roles' => $roles,
            'manager'   => $manager
        ]);
    }

    public function updateRole(Request $request,$id){
        $manager = WireHouseContact::findOrFail($id);
        $user = User::where('manager_id',$manager->id)->first();

        if(!$user->mainBranch()){
            return redirect()->back()->withMessage([
                'status'    => 'alert-danger',
                'text'      => 'This manager does not need to attach role.'
            ]);
        }

        $user->roles()->sync($request->input('roles'));
        return redirect()->back()->withMessage([
            'status'    => 'alert-success',
            'text'      => 'Successfully updated warehouse manager roles.'
        ]);
    }
}
