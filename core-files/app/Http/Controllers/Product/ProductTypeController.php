<?php

namespace App\Http\Controllers\Product;

use App\Model\Product\ProductType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;

class ProductTypeController extends Controller
{


    public function index(){
        $product_types = ProductType::orderBy('id','desc')->get();
                

        //return $product_types;
        return view('admin.product_type')->with([
            'product_types'   => $product_types
        ]);
    }

    public function store(Request $request){
    	//$request->all();
    	$this->validate($request, array(
            'name' => 'required|max:255',
            'status' => 'required'
        ));

        $ptype = new ProductType;

        $ptype->name = $request->name;
        $ptype->status = $request->status;

        $ptype->save();

    	return redirect()->route('product_type')->withMessage([
    	    'status'    => 'alert-success',
            'text'      => 'Successfully created product type.'
        ]);
    }

    public function delete($id){
        
        DB::table('product_types')
                    ->where('id',$id)
                    ->update(['status' => 0]);

        return redirect()->route('product_type');
    }

    public function showUpdate($id){
        //$product_types = ProductType::all();
        /*  $types = DB::table('product_types')
                        ->where('id', $id)
                        ->get();*/  
        $types = ProductType::find($id);   

            /*return $types;*/


        //return $product_types;
        return view('editpages.edit_product_type')->with([
            'types'   => $types
        ]);
    }

    public function update(Request $request, $id){
        //echo $id;
        //return $request->all();

        $this->validate($request, array(
            'name' => 'required|max:255',
            'status' => 'required'
        ));

        $updateDetails=array(
            'name'   => $request->get('name'),
            'status' => $request->get('status')
        );

        DB::table('product_types')
                ->where('id', $id)
                ->update($updateDetails);

        return redirect()->route('product_type')->with([
            'message' => [
                'status'    => 'alert-success',
                'text'      => 'Successfully updated product type.'
            ]
        ]);
    }
}
