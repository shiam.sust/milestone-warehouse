<?php

namespace App\Http\Controllers\Product;

use App\Model\Product\Product;
use App\Model\Product\ProductSet;
use App\Model\Product\ProductType;
use App\Model\Product\Unit;
use App\Model\Classes\AcademicClass;
use App\Model\WireHouse\WireHouse;
use App\Model\WireHouse\WireHouseProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;

class ProductController extends Controller
{

    public function index(){
        $products = Product::orderBy('id','desc')->get();
        
        return view('admin.product')->with([
            'products' => $products
        ]);
    }

    public function stock(){
        $warehouse = WireHouse::where('is_main',true)->first();
        $products = WireHouseProduct::where('wh_id',$warehouse->id)->orderBy('id','desc')->get();
        return view('admin.stock.products')->with([
            'products'  => $products
        ]);
    }

    public function createInitialQuantity(){
        $units = Unit::all();
        $warehouse = WireHouse::where('is_main',true)->first();
        $wh_id = $warehouse->id;
        $products = Product::whereDoesntHave('warehouses',function($q) use($wh_id){
            $q->where('wh_id',$wh_id);
        })->get();
        return view('admin.stock.create')->with([
            'products'  => $products,
            'units'     => $units,
        ]);
    }

    public function updateInitialQuantity(Request $request,$id){
        $product = WireHouseProduct::findOrFail($id);
        $product->update([
            'stock_balance' => $product->stock_balance - $product->initial_quantity + $request->input('quantity'),
            'initial_quantity' => $request->input('quantity')
        ]);
        return redirect()->back()->withMessage([
            'status'    => 'alert-success',
            'text'      => 'Successfully updated initial quantity'
        ]);
    }

    public function storeInitialQuantity(Request $request){
        $wh_id = $request->input('wh_id');

        foreach ($request->input('product') as $p){
            $product = WireHouseProduct::firstOrCreate(
                ['wh_id' => $wh_id,'product_id' => $p['id']]
            );

            $product->update([
                'stock_balance' => $product->stock_balance - $product->initial_quantity + $p['quantity'],
                'initial_quantity' => $p['quantity']
            ]);
        }

        return redirect()->route('balance-product')->withMessage([
            'status'    => 'alert-success',
            'text'      => 'Successfully created initial quantity'
        ]);
    }

    public function showProductForm(){
        $ptype = ProductType::all();
        $unit = Unit::all();
        
        return view('admin.add_product')->with([
            'ptype' => $ptype,
            'unit' => $unit
        ]);
    }

     public function store(Request $request){

        //return $request->all();

        //server validation of our form data
        $this->validate($request, array(
            'name' => 'required|max:255',
            'unit' => 'required|max:255',
            'status' => 'required'
        ));

        //creating model object and setting our data to database column
        $product = new Product;

        $product->name = $request->name;
        $product->type_id = $request->type;
        $product->base_unit = $request->unit;
        $product->author = $request->author;
        $product->selling_price = $request->selling_price;
        $product->cost_price = $request->cost_price;
        $product->status = $request->status;

        //push data to database
        $product->save();

        //redirecting to productlist 
        return redirect()->route('product')->with([
            'message' => [
                'status'    => 'alert-success',
                'text'      => 'Successfully created product.'
            ]
        ]);
    }

    public function delete($id){
        
        DB::table('products')
                    ->where('id',$id)
                    ->update(['status' => 0]);

        return redirect()->route('product')->withMessage([
            'status'    => 'alert-success',
            'text'      => 'Successfully deleted product.'
        ]);
        
    }

    public function showUpdate($id){
        //$product_types = ProductType::all();
        /*  $types = DB::table('product_types')
                        ->where('id', $id)
                        ->get();*/  
        $ptype   = ProductType::all();
        $unit    = Unit::all();
        $product = Product::find($id);   

            /*return $types;*/


        //return $product_types;
        return view('editpages.edit_product')->with([
            'ptype'   => $ptype,
            'unit'    => $unit,
            'product' => $product
      ]);
    }

    public function update(Request $request, $id){
        //echo $id;
        //return $request->all();

        $this->validate($request, array(
            'name'          => 'required|max:255',
            'unit'          => 'required',
            'status'        => 'required'
        ));


        $updateDetails=array(
            'name'   => $request->get('name'),
            'base_unit'   => $request->get('unit'),
            'type_id'   => $request->get('type'),
            'author'   => $request->get('author'),
            'selling_price'   => $request->get('selling_price'),
            'cost_price'   => $request->get('cost_price'),
            'status' => $request->get('status')
        );

        DB::table('products')
                ->where('id', $id)
                ->update($updateDetails);

        return redirect()->route('product')->withMessage([
            'status'    => 'alert-success',
            'text'      => 'Successfully updated product.'
        ]);
    }

    


    
}
