<?php

namespace App\Http\Controllers\Product;

use App\Model\Product\Unit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class UnitController extends Controller
{
    
    public function index(){
        $units = Unit::orderBy('id','desc')->get();

        return view('admin.product_unit')->with([
            'units' => $units
        ]);
    }

    public function store(Request $request){
    	//return $request->all();

    	$this->validate($request, array(
            'name' => 'required|max:255',
            'status' => 'required'
        ));

        $unit = new Unit;

        $unit->name = $request->name;
        $unit->status = $request->status;

        $unit->save();

    	return redirect()->route('product_unit')->with([
            'message' => [
                'status'    => 'alert-success',
                'text'      => 'Successfully created unit.'
            ]
        ]);
    }

    public function delete($id){
        
        DB::table('units')
                    ->where('id',$id)
                    ->update(['status' => 0]);

        return redirect()->route('product_unit');
        
    }

    public function showUpdate($id){
        //$product_types = ProductType::all();
        /*  $types = DB::table('product_types')
                        ->where('id', $id)
                        ->get();*/  
        $unit = Unit::find($id);   

            /*return $types;*/


        //return $product_types;
        return view('editpages.edit_product_unit')->with([
            'unit'   => $unit
        ]);
    }

    public function update(Request $request, $id){
        //echo $id;
        //return $request->all();

        $this->validate($request, array(
            'name' => 'required|max:255',
            'status' => 'required'
        ));

        $updateDetails=array(
            'name'   => $request->get('name'),
            'status' => $request->get('status')
        );

        DB::table('units')
                ->where('id', $id)
                ->update($updateDetails);

        return redirect()->route('product_unit')->with([
            'message' => [
                'status'    => 'alert-success',
                'text'      => 'Successfully updated unit.'
            ]
        ]);
    }

    
}
