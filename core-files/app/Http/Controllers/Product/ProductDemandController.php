<?php

namespace App\Http\Controllers\Product;

use App\Model\WireHouse\WireHouseProduct;
use App\Model\WireHouse\WireHouseProductSet;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\WireHouse\WireHouse;
use App\Model\Product\Unit;
use App\Model\Product\Product;
use App\Model\Product\ProductSet;
use App\ProductDemand;
use Auth;

class ProductDemandController extends Controller
{
    public function index(){
        $demands = ProductDemand::orderBy('id','desc')->get();
        //return $demands;
        return view('admin.product-demand.index')->with([
            'demands' => $demands
        ]);

    }

    public function show($id){
        $demand = ProductDemand::findOrFail($id);
        return view('admin.product-demand.show')->with([
            'demand'    => $demand
        ]);
    }

    public function showForm(){
    	$wh = WireHouse::all();
    	$unit = Unit::all();
    	$product = Product::all();

        return view('admin.add_product_demand')->with([
        	'wh' => $wh,
        	'unit' => $unit,
        	'product' => $product
        ]);

    }

    public function store(Request $request){

        //return $request->all();

        //server validation of our form data
        $this->validate($request, array(
            'demand_type' => 'required',
            'towirehouse' => 'required',
            'product' => 'required',
            'quantity' => 'required',
            'unit' => 'required',
        ));

        //creating model object and setting our data to database column
        $demand = new ProductDemand;

        $demand->demand_type 	= $request->demand_type;
        
        $demand->to_warehouse 	= $request->towirehouse;
        $demand->product_id = $request->product;
        $demand->quantity = $request->quantity;
        $demand->unit_id = $request->unit;
        //$demand->created_by = Auth::user()->id;
        
        $demand->return_quantity = 0;

        //push data to database
        $demand->save();

        //redirecting to productlist 
        return redirect()->route('product_demand')->with([
            'message' => [
                'status'    => 'alert-success',
                'text'      => 'Successfully created requisition.'
            ]
        ]);
    }

    public function showUpdate($id){
        //$product_types = ProductType::all();
        /*  $types = DB::table('product_types')
                        ->where('id', $id)
                        ->get();*/  
        $ptype   = ProductType::all();
        $unit    = Unit::all();
        $product = Product::find($id);   

            /*return $types;*/


        //return $product_types;
        return view('editpages.edit_product')->with([
            'ptype'   => $ptype,
            'unit'    => $unit,
            'product' => $product
      ]);
    }

    public function getItems($id){
    	if($id == 1){
    		$item = Product::all();
    	}else{
    		$item = ProductSet::all();
    	}

    	return response()->json(['items'=>$item], 200);
    }

    public function approve($id){
        //return $id;
        $wh_id = Auth::user()->id;
        $product_demand = ProductDemand::where('id',$id)->first();
        //return $product_demand;
        $product_demand->update([
            'status'    => 1
        ]);

        return redirect()->back()->withMessage([
            'status'    => 'alert-success',
            'text'  => 'Successfully updated this information'
        ]);
    }

    public function receiveReturn($id){
        $product_demand = ProductDemand::findOrFail($id);
        $wh_id = Auth::user()->id;
        $product_demand->update([
            'status'    => 5
        ]);
        if($product_demand->demand_type == 1){
           //$wh_id;

            $product = WireHouseProduct::where('product_id',$product_demand->product_id)->where('wh_id',$product_demand->from_warehouse)->first();

            $product->update([
                'quantity'  => $product->quantity - $product_demand->return_quantity
            ]);

            $own_product = WireHouseProduct::where('product_id',$product_demand->product_id)->where('wh_id',$wh_id)->first();

           if(!$own_product){
                $own_product = WireHouseProduct::create([
                    'product_id'    => $product_demand->product_id,
                    'quantity'      => 0,
                    'wh_id'         => $wh_id
                ]);
           }
            
            $own_product->update([
                'quantity'  => $own_product->quantity + $product_demand->return_quantity
            ]);

        }

        else if($product_demand->demand_type == 2){
            return $product_demand;

            $product = WireHouseProductSet::where('product_set_id',$product_demand->product_id)->where('wh_id',$product_demand->from_warehouse)->first();

            $product->update([
                'quantity'  => $product->quantity - $product_demand->return_quantity,
            ]);

            $own_product = WireHouseProductSet::where('product_id',$product_demand->product_id)->where('wh_id',$wh_id)->first();
            if(!$own_product){
                $own_product = WireHouseProductSet::create([
                    'product_id'    => $product_demand->product_id,
                    'quantity'      => 0,
                    'wh_id'         => $wh_id
                ]);
           }
            $own_product->update([
                'quantity'  => $own_product->quantity + $product_demand->return_quantity
            ]);
        }

        return redirect()->back()->withMessage([
            'status'    => 'alert-success',
            'text'      => 'Successfully received this product'
        ]);
    }

}
