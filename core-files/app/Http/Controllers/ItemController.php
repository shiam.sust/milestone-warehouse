<?php

namespace App\Http\Controllers;

use App\Model\Product\Product;
use App\Model\Product\ProductSet;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    public function getProductsAndSets(){
        $products = Product::all();
        $product_sets = ProductSet::all();

        $product_data = [];

        foreach ($products as $product){
            $product_data[] = [
                'id'    => $product->id,
                'name'  => $product->name,
                'unit'  => $product->unit->name,
                'type'  => 1,
                'price'  => $product->cost_price,
                'quantity'  => 1,
                'subTotal'  => $product->cost_price
            ];
        }

        $product_set_data = [];

        foreach ($product_sets as $product_set){
            $product_set_data[] = [
                'id'    => $product_set->id,
                'name'  => $product_set->name,
                'unit'  => 'SET',
                'type'  => 2,
                'price'  => $product_set->cost_price,
                'quantity'  => 1,
                'subTotal'  => $product_set->cost_price
            ];
        }

        return response()->json([
            'product'  => $product_data,
            'sets'      => $product_set_data
        ],200);

    }
}
