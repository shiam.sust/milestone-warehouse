<?php

namespace App\Http\Controllers\ManagerAdmin;

use App\Model\Chalan;
use App\Model\ChalanDetail;
use App\Model\Product\Product;
use App\Model\Product\ProductSet;
use App\Model\PurchaseOrder;
use App\Model\PurchaseOrderDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class ChalanController extends Controller
{
    public function index(){
        $wh_id = Auth::user()->wh_id;
        $chalans = Chalan::where('wh_id',$wh_id)->orderBy('id','desc')->get();
        return view('wirehouse-manager.chalan.index')->with([
            'chalans'  => $chalans
        ]);
    }

    public function create(){
        $wh_id = Auth::user()->wh_id;
        $ch_no = 'CH-'.mt_rand(100000,999999);
        $purchase_orders = PurchaseOrder::where('wh_id',$wh_id)->where('status','<',2)->orderBy('id','desc')->get();
        return view('wirehouse-manager.chalan.create')->with([
            'purchase_orders'   => $purchase_orders,
            'ch_auto' => $ch_no
        ]);
    }

    public function store(Request $request){

        $purchase_order = PurchaseOrder::findOrFail($request->input('purchase_order'));
        $wh_id = Auth::user()->wh_id;

        $chalan = Chalan::create([
            'purchase_order_id' => $purchase_order->id,
            'status'    => false,
            'chalan_date'   => $request->input('chalan_date'),
            'chalan_no'     => $request->input('chalan_no'),
            'chalan_no_manual' => $request->input('chalan_no_manual'),
            'wh_id' => $wh_id
        ]);

        $created = false;

        foreach ($request->input('purchase_od') as $k => $v){
            $od = PurchaseOrderDetail::findOrFail($k);

            if(isset($v['quantity']) && $v['quantity'] > 0){
                ChalanDetail::create([
                    'chalan_id' => $chalan->id,
                    'purchase_order_details_id' => $od->id,
                    'product_id' => $od->item_type == 1 ? $od->item->id : null,
                    'product_set_id' => $od->item_type == 2 ? $od->item->id : null,
                    'received_qty'  => $v['quantity'],
                    'product_amount' => $od->unit_price * $v['quantity'],
                    'item_type' => $od->item_type
                ]);

                $created = true;
            }

        }
        if(!$created)
            return redirect()->back();

        return redirect()->route('wirehouse.chalans')->with([
            'message' => [
                'status'    => 'alert-success',
                'text'      => 'Successfully created chalan.'
            ]
        ]);

    }

    public function getChalan($id){
        $chalan = Chalan::findOrFail($id);

        if($chalan->wh_id != Auth::user()->wh_id && !Auth::user()->is_admin){
            return redirect()->back();
        }

        return view('wirehouse-manager.chalan.details')->with([
            'chalan'    => $chalan
        ]);
    }
}
