<?php

namespace App\Http\Controllers\ManagerAdmin;

use App\Model\Product\ProductDemandDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\WireHouse\WireHouse;
use App\Model\Product\Unit;
use App\Model\Product\Product;
use App\Model\Product\ProductSet;
use App\Model\wireHouse\WireHouseProductSet;
use App\Model\wireHouse\WireHouseProduct;
use App\ProductDemand;
use Auth;

class ProductDemandController extends Controller
{
    public function index(){
        $wh_id = Auth::user()->wh_id;
        $demands = ProductDemand::where('from_warehouse',$wh_id)->orderBy('id','desc')->get();
        return view('wirehouse-manager.product-demand.index')->with([
            'demands'  => $demands
        ]);
    }

    public function showCreate(){
    	$wh = WireHouse::all();
    	$unit = Unit::all();
    	$product = Product::all();
    	$product_sets = ProductSet::all();
    	$date = Carbon::today();
    	$pd = count(ProductDemand::all()) + 1;
    	$req_no = 'PReq-'.$date->year.'-' . $date->month.'-'. $date->day. '-' .str_pad($pd,4,'0',STR_PAD_LEFT);

        return view('wirehouse-manager.product-demand.add_demand')->with([
        	'wh' => $wh,
        	'unit' => $unit,
        	'product' => $product,
            'req_no'    => $req_no,
            'product_sets'  => $product_sets
        ]);

    }

    public function show($id){
        $demand = ProductDemand::findOrFail($id);
        return view('wirehouse-manager.product-demand.show')->with([
            'demand'    => $demand
        ]);
    }

    public function store(Request $request){


        $demand = ProductDemand::create([
            'requisition_date'  => $request->input('requisition_date'),
            'req_no_auto'       => $request->input('req_no_auto'),
            'req_no_manual'     => $request->input('req_no_manual'),
            'from_warehouse'    => Auth::user()->wh_id,
            'to_warehouse'      => $request->input('towirehouse'),
            'created_by'        => AUth::user()->id
        ]);

        foreach ($request->input('product') as $product){
            ProductDemandDetail::create([
                'product_demand_id' => $demand->id,
                'item_id'           => $product['id'],
                'item_type'         => $product['type'],
                'unit_id'           => $product['unit'],
                'quantity'          => $product['quantity']
            ]);
        }

        return redirect()->route('wirehouse.product-demands')->with([
            'message' => [
                'status'    => 'alert-success',
                'text'      => 'Successfully created this requisition.'
            ]
        ]);
    }

    public function getItems($id){
    	if($id == 1){
    		$item = Product::all();
    	}else{
    		$item = ProductSet::all();
    	}

    	return response()->json(['items'=>$item], 200);
    }

    public function edit($id){
        $wh_id = Auth::user()->wh_id;
        $product_demand = ProductDemand::where('id',$id)->where('from_warehouse',$wh_id)->first();
        if(!$product_demand){
            return redirect()->back();
        }

        $demand_products =  $product_demand->productDemandDetails->where('item_type',1);
        $product_ids = count($demand_products) ? $demand_products->pluck('item_id') : [];
        $demand_product_sets =  $product_demand->productDemandDetails->where('item_type',2);
        $product_set_ids = count($demand_products) ? $demand_product_sets->pluck('item_id') : [];

        $warehouses = WireHouse::orderBy('name','asc')->get();
        $products = Product::whereNotIn('id',$product_ids)->get();
        $product_sets = ProductSet::whereNotIn('id',$product_set_ids)->get();
        $units = Unit::orderBy('name','asc')->get();

        $data = [];
        foreach ($product_demand->productDemandDetails as $dd){
            $item = $dd->item;
            $item->unitId = $dd->unit_id;
            $item->quantity = $dd->quantity;
            $data[] = $item;
        }




        return view('wirehouse-manager.product-demand.edit-demand')->with([
            'product_demand'    => $product_demand,
            'wh'        => $warehouses,
            'units'              => $units,
            'products'          => $products,
            'product_sets'       => $product_sets,
            'demand_products'  => json_encode($data)
        ]);
    }

    public function update(Request $request,$id){

        $demand = ProductDemand::findOrFail($id);
        $demand->update([
            'requisition_date'  => $request->input('requisition_date'),
            'req_no_manual'     => $request->input('req_no_manual'),
            'from_warehouse'    => Auth::user()->wh_id,
            'to_warehouse'      => $request->input('towirehouse'),
            'created_by'        => Auth::user()->id
        ]);

        ProductDemandDetail::where('product_demand_id',$demand->id)->delete();
        foreach ($request->input('product') as $product){
            ProductDemandDetail::create([
                'product_demand_id' => $demand->id,
                'item_id'           => $product['id'],
                'item_type'         => $product['type'],
                'unit_id'           => $product['unit'],
                'quantity'          => $product['quantity']
            ]);
        }

        return redirect()->route('wirehouse.product-demands')->withMessage([
            'status'    => 'alert-success',
            'text'      => 'Successfully updated this requisition.'
        ]);
    }

    public function receive($id){
        $wh_id = Auth::user()->wh_id;
        $product_demand = ProductDemand::where('id',$id)->where('from_warehouse',$wh_id)->first();
        if($product_demand->demand_type == 1){

            $from_wh_p = WireHouseProduct::where('product_id',$product_demand->product_id)->where('wh_id',$product_demand->to_warehouse)->first();

            if($from_wh_p){
                $from_wh_p->update([
                    'quantity'  => $from_wh_p->quantity - $product_demand->quantity
                ]);
            }else{
                $from_wh_p = WireHouseProduct::create([
                    'product_id'    => $product_demand->product_id,
                    'quantity'      => 0,
                    'wh_id'         => $product_demand->to_warehouse
                ]);
                $from_wh_p->update([
                    'quantity'  => $from_wh_p->quantity - $product_demand->quantity
                ]);
            }

            $product = WireHouseProduct::where('product_id',$product_demand->product_id)->where('wh_id',$wh_id)->first();
            if($product){
                $qty = $product->quantity + $product_demand->quantity;
                $product->update([
                    'quantity'  => $qty
                ]);
            }else{
                $product = WireHouseProduct::create([
                    'product_id'    => $product_demand->product_id,
                    'quantity'      => $product_demand->quantity,
                    'wh_id'         => $wh_id
                ]);
            }
        }

        else {

            $from_wh_p = WireHouseProductSet::where('product_set_id',$product_demand->product_id)->where('wh_id',$product_demand->to_warehouse)->first();
            if($from_wh_p){
                $from_wh_p->update([
                    'quantity'  => $from_wh_p->quantity - $product_demand->quantity
                ]);
            }else{
                $from_wh_p = WireHouseProductSet::create([
                    'product_set_id'    => $product_demand->product_id,
                    'quantity'      => 0,
                    'wh_id'         => $product_demand->to_warehouse
                ]);
                $from_wh_p->update([
                    'quantity'  => $from_wh_p->quantity - $product_demand->quantity
                ]);
            }

            $product = WireHouseProductSet::where('product_set_id',$product_demand->product_id)->where('wh_id',$wh_id)->first();
            if($product){
                $qty = $product->quantity + $product_demand->quantity;
                $product->update([
                    'quantity'  => $qty,
                ]);
            }else{
                $product = WireHouseProductSet::create([
                    'product_set_id'    => $product_demand->product_id,
                    'quantity'      => $product_demand->quantity,
                    'wh_id'         => $wh_id
                ]);
            }
        }

        $product_demand->update([
            'status'    => 3
        ]);

        return redirect()->back()->withMessage([
            'status'    => 'alert-success',
            'text'      => 'Successfully received this product'
        ]);

    }

}
