<?php

namespace App\Http\Controllers\ManagerAdmin;

use App\Model\WireHouse\WireHouseProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class ProductController extends Controller
{
    public function index(){

        $wh_id = Auth::user()->wh_id;
        $products = WireHouseProduct::where('wh_id',$wh_id)->get();

        return view('wirehouse-manager.product.index')->with([
            'products'  => $products
        ]);
    }
}
