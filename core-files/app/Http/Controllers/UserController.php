<?php

namespace App\Http\Controllers;

use App\Model\WireHouse\WireHouseContact;
use App\User;
use Illuminate\Http\Request;
use Auth;

class UserController extends Controller
{
    public function index(){
        return view('admin.user-profile');
    }

    public function changePassword(Request $request){
        $this->validate($request,[
            'password' => 'required|string|min:6|confirmed'
        ]);

        $user = User::findOrFail(Auth::user()->id);

        $user->update([
            'password'  => bcrypt($request->input('password'))
        ]);

        return redirect()->back()->withMessage([
            'status'    => 'alert-success',
            'text'      => 'Password has been updated'
        ]);
    }

    public function changeProfilePic(Request $request){
        $wm_id = Auth::user()->warehouseContact->id;
        $wh_contact = WireHouseContact::findOrFail($wm_id);
        if($request->file('profile_pic')){
            $name = time().".".$request->file('profile_pic')->getClientOriginalExtension();
            $request->file('profile_pic')->move(public_path('images/managers'),$name);
            $name = 'images/managers/'.$name;
            $wh_contact->update(['profile_pic' => $name]);
        }

        return redirect()->back();
    }
}
