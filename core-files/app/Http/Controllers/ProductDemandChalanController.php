<?php

namespace App\Http\Controllers;

use App\Model\WireHouse\WireHouseProduct;
use App\Model\WireHouse\WireHouseProductSet;
use App\ProductDemand;
use App\RequisitionChalan;
use Illuminate\Http\Request;
use Auth;

class ProductDemandChalanController extends Controller
{
    public function index(){
        $chalans = RequisitionChalan::orderBy('id','desc')->get();
        return view('admin.requisition-chalan.index')->with([
            'chalans'   => $chalans
        ]);
    }

    public function show($id){
        $chalan = RequisitionChalan::findOrFail($id);
        return view('admin.requisition-chalan.details')->with([
            'chalan'   => $chalan
        ]);
    }

    public function approve($id){
        $wh_id = Auth::user()->wh_id;
        $chalan = RequisitionChalan::findOrFail($id);
        if($chalan->status){
            return redirect()->back()->withMessage([
                'status'    => 'alert alert-danger',
                'text'      => 'This chan is already approved'
            ]);
        }

        foreach ($chalan->chalanDetails as $chalanDetail){
            if($chalanDetail->item_type == 1){
                $wh_product = WireHouseProduct::where('product_id',$chalanDetail->item_id)->where('wh_id',$chalan->wh_id)->first();
                $cwh_product = WireHouseProduct::where('product_id',$chalanDetail->item_id)->where('wh_id',$wh_id)->first();

                if($wh_product){
                    $q = $chalanDetail->quantity + $wh_product->quantity;
                    $sb = $wh_product->stock_balance + $chalanDetail->quantity;
                    $wh_product->update([
                        'quantity'      => $q,
                        'stock_balance' => $sb
                    ]);
                }else{
                    WireHouseProduct::create([
                        'wh_id' => $chalan->wh_id,
                        'product_id'    => $chalanDetail->item_id,
                        'quantity'      => $chalanDetail->quantity,
                        'stock_balance' => $chalanDetail->quantity,
                        'status'        => 1
                    ]);
                }

                if($cwh_product){
                    $q =  $cwh_product->quantity - $chalanDetail->quantity;
                    $sb = $cwh_product->stock_balance - $chalanDetail->quantity;
                    $cwh_product->update([
                        'quantity'      => $q,
                        'stock_balance' => $sb
                    ]);
                }else{
                    WireHouseProduct::create([
                        'wh_id' => $wh_id,
                        'product_id'    => $chalanDetail->item_id,
                        'quantity'      => $chalanDetail->quantity * (-1),
                        'stock_balance' => $chalanDetail->quantity* (-1),
                        'status'        => 1
                    ]);
                }

            }
            else{
                $wh_product_set = WireHouseProductSet::where('product_set_id',$chalanDetail->item_id)->where('wh_id',$chalan->wh_id)->first();
                $cwh_product_set = WireHouseProductSet::where('product_set_id',$chalanDetail->item_id)->where('wh_id',$wh_id)->first();
                if($wh_product_set){
                    $wh_product_set->update([
                        'quantity'      => $chalanDetail->quantity + $wh_product_set->quantity,
                        'stock_balance' => $wh_product_set->stock_balance + $chalanDetail->quantity
                    ]);
                }else{
                    WireHouseProductSet::create([
                        'wh_id'             => $chalan->wh_id,
                        'product_set_id'    => $chalanDetail->item_id,
                        'quantity'          => $chalanDetail->quantity,
                        'stock_balance'     => $chalanDetail->received_qty,
                        'status'            => 1
                    ]);
                }

                if($cwh_product_set){
                    $cwh_product_set->update([
                        'quantity'      => $cwh_product_set->quantity - $chalanDetail->quantity,
                        'stock_balance' => $cwh_product_set->stock_balance - $chalanDetail->quantity
                    ]);
                }else{
                    WireHouseProductSet::create([
                        'wh_id'             => $wh_id,
                        'product_set_id'    => $chalanDetail->item_id,
                        'quantity'          => $chalanDetail->quantity *  (-1),
                        'stock_balance'     => $chalanDetail->quantity * (-1),
                        'status'            => 1
                    ]);
                }

            }
        }

        $chalan->update([
            'status'    => 1
        ]);

        $this->checkProductRequisitionStatus($chalan->demand);


        return redirect()->route('product-demand-chalans')->withMessage([
            'status'    => 'alert-success',
            'text'      => 'Successfully approved chalan.'
        ]);
    }

    private function checkProductRequisitionStatus(ProductDemand $productDemand){
        foreach ($productDemand->productDemandDetails as $dd ){
            $total_received = $dd->chalanDetails ? $dd->chalanDetails->sum('quantity') : 0;
            $rem_qty = $dd->quantity - $total_received;
            if($rem_qty > 0){
                $productDemand->update([
                    'status'    => 1
                ]);

                return;
            }
        }
        $productDemand->update([
            'status'    => 2
        ]);
        return;
    }
}
