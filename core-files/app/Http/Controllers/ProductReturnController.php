<?php

namespace App\Http\Controllers;

use App\Model\Product\Product;
use App\Model\Product\ProductSet;
use App\Model\Product\Unit;
use App\Model\ProductReturn\ProductReturn;
use App\Model\ProductReturn\ProductReturnDetail;
use App\Model\WireHouse\WireHouse;
use Illuminate\Http\Request;
use Auth;

class ProductReturnController extends Controller
{
    public function index(){
        $wh_id = Auth::user()->wh_id;
        $return_products = ProductReturn::orderBy('id','desc')->get();
        return view('admin.product-return.index')->with([
            'return_products'  => $return_products
        ]);
    }

    public function show ($id){
        $product_return = ProductReturn::findOrFail($id);
        return view('admin.product-return.show')->with([
            'product_return'    => $product_return
        ]);
    }

    public function approveReturn($id){
        $product_return = ProductReturn::findOrFail($id);
        $product_return->update([
            'status'    => 1
        ]);

        return redirect()->back()->withMessage([
            'status'    => 'alert-success',
            'text'      => 'Approved this product return request.'
        ]);
    }
    public function edit($id){

        $product_return = ProductReturn::where('id',$id)->first();
        if(!$product_return){
            return redirect()->back();
        }


        $product_ids = ProductReturnDetail::where('product_ret_id',$product_return->id)->where('item_type',1)->get()->pluck('item_id');

        $product_set_ids = ProductReturnDetail::where('product_ret_id',$product_return->id)->where('item_type',2)->get()->pluck('item_id');

        $warehouses = WireHouse::orderBy('name','asc')->get();
        $products = Product::whereNotIn('id',$product_ids)->get();
        $product_sets = ProductSet::whereNotIn('id',$product_set_ids)->get();
        $units = Unit::orderBy('name','asc')->get();

        $data = [];
        foreach ($product_return->productReturnDetails as $rd){
            $item = $rd->item;
            $item->unitId = $rd->unit_id;
            $item->quantity = $rd->quantity;
            $data[] = $item;
        }




        return view('admin.product-return.edit-return')->with([
            'product_return'    => $product_return,
            'wh'        => $warehouses,
            'warehouses'        => $warehouses,
            'units'              => $units,
            'products'          => $products,
            'product_sets'       => $product_sets,
            'demand_products'  => json_encode($data)
        ]);
    }

    public function update(Request $request,$id){
        $demand = ProductReturn::findOrFail($id);
        $demand->update([
            'return_date'  => $request->input('return_date'),
            'ret_no_auto'       => $request->input('ret_no_auto'),
            'ret_no_manual'     => $request->input('ret_no_manual'),
            'to_warehouse'      => $request->input('towirehouse'),
            'created_by'        => Auth::user()->id
        ]);

        ProductReturnDetail::where('product_ret_id',$demand->id)->delete();
        foreach ($request->input('product') as $product){
            ProductReturnDetail::create([
                'product_ret_id'    => $demand->id,
                'item_id'           => $product['id'],
                'item_type'         => $product['type'],
                'unit_id'           => $product['unit'],
                'quantity'          => $product['quantity']
            ]);
        }

        return redirect()->route('products-return')->with([
            'message' => [
                'status'    => 'alert-success',
                'text'      => 'Successfully created this return.'
            ]
        ]);
    }
}
