<?php

namespace App\Http\Middleware;

use Closure;

class ManagerAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()->is_admin || $request->user()->mainBranch()){
            return redirect()->back();
        }
        return $next($request);
    }
}
