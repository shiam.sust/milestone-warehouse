<?php

use Illuminate\Database\Seeder;
use App\ProductDemand;

class ProductDemandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductDemand::create([
        	'demand_type' => 1,
        	'quantity' => 5,
        	'return_quantity' => 2,
        	'status' => 1
        ]);

        ProductDemand::create([
        	'demand_type' => 1,
        	'quantity' => 5,
        	'return_quantity' => 2,
        	'status' => 1
        ]);

        ProductDemand::create([
        	'demand_type' => 1,
        	'quantity' => 5,
        	'return_quantity' => 2,
        	'status' => 1
        ]);

        ProductDemand::create([
        	'demand_type' => 1,
        	'quantity' => 5,
        	'return_quantity' => 2,
        	'status' => 1
        ]);

        ProductDemand::create([
        	'demand_type' => 1,
        	'quantity' => 5,
        	'return_quantity' => 2,
        	'status' => 1
        ]);
    }
}
