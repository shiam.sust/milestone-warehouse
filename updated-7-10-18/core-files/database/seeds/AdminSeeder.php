<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $wirehouse = \App\Model\WireHouse\WireHouse::create([
            'name'  => 'Central Warehouse',
            'phone' => '123456',
            'email' => 'admin@gmail.com',
            'mobile'    => '123456',
            'address'  => 'Uttora,DHaka,Bangladesh',
            'status'    => 1,
            'is_main'  => true
        ]);

        $manager = \App\Model\WireHouse\WireHouseContact::create([
            'wh_id' => $wirehouse->id,
            'name'  => 'Nizam',
            'mobile'=> '01791944248',
            'address'    => 'Mohakhali DOHS, DHaka, Bangladesh',
            'status'    => 1,
            'designation'   => 'Manager',
            'email' => 'nizamsuet@gmail.com'
        ]);

        \App\User::create([
            'name'  => $manager->name,
            'email' => $manager->email,
            'password'  => bcrypt('123456'),
            'wh_id' => $wirehouse->id,
            'manager_id'    => $manager->id,
            'is_admin'  => 1,
            'status'    => 1
        ]);

        \App\PaymentMethod::create([
            'name'  => 'Cash',
            'status'    => 1,
        ]);

        \App\PaymentMethod::create([
            'name'  => 'Cheque',
            'status'    => 1
        ]);


    }
}
