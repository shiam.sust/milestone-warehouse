<?php

use Illuminate\Database\Seeder;
use App\Model\Classes\AcademicClass;
use App\Model\WireHouse\WireHouse;
use App\Model\WireHouse\WireHouseContact;

class ClassTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     	AcademicClass::create([
     		'name' => 'Ten',
     		'status' => 1
     	]);   

     	AcademicClass::create([
     		'name' => 'Nine',
     		'status' => 1
     	]); 

     	AcademicClass::create([
     		'name' => 'Eight',
     		'status' => 1
     	]); 

     	AcademicClass::create([
     		'name' => 'Seven',
     		'status' => 1
     	]); 

        $one = WireHouse::create([
            'name' => 'firstwh', 
            'phone' => '146',          
            'mobile' => '147',
            'email' => '@gmail.com',
            'address' => 'gajipur'
        ]);

        $two = WireHouse::create([
            'name' => 'secondwh', 
            'phone' => '257',           
            'mobile' => '258',
            'email' => '@gmail.com',
            'address' => 'kakrail'
        ]);

        $three = WireHouse::create([
            'name' => 'thirdwh', 
            'phone' => '268',          
            'mobile' => '369',
            'email' => '@gmail.com',
            'address' => 'badda'
        ]);

        WireHouseContact::create([
            'name' => 'xyz',
            'wh_id' => $one->id,
            'designation' => 'manager',
            'mobile' => '123',
            'email' => '@gmail.com',
            'address' => 'mohakhali'
        ]);

        WireHouseContact::create([
            'name' => 'abc',
            'wh_id' => $two->id,
            'designation' => 'officer',
            'mobile' => '456',
            'email' => '@gmail.com',
            'address' => 'dohs'
        ]);

        WireHouseContact::create([
            'name' => 'asd',
            'wh_id' => $three->id,
            'designation' => 'leader',
            'mobile' => '789',
            'email' => '@gmail.com',
            'address' => 'mirpur'
        ]);

        



    }
}
