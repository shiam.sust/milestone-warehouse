<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductDemandDetailIdToRequisitionChalanDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('requisition_chalan_details', function (Blueprint $table) {
            $table->integer('p_req_d_id')->unsigned();
            $table->foreign('p_req_d_id')->references('id')->on('requisition_chalan_details');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('requisition_chalan_details', function (Blueprint $table) {
            $table->dropColumn('p_req_d_id');
        });
    }
}
