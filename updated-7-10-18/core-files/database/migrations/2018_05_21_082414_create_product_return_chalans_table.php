<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductReturnChalansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_return_chalans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_return_id')->unsigned();
            $table->integer('wh_id')->unsigned();
            $table->string('ch_no_auto');
            $table->string('ch_no_manual')->nullable();
            $table->date('ch_date')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
            $table->foreign('product_return_id')->references('id')->on('product_returns');
            $table->foreign('wh_id')->references('id')->on('wire_houses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_return_chalans');
    }
}
