<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCostAndSellPriceToProductSetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_sets', function (Blueprint $table) {
            $table->double('cost_price')->nullable();
            $table->double('selling_price')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_sets', function (Blueprint $table) {
            $table->dropColumn(['cost_price','selling_price']);
        });
    }
}
