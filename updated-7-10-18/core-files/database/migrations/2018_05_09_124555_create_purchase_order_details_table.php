<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_order_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('po_id')->unsigned();
            $table->integer('quantity');
            $table->float('unit_price');
            $table->integer('base_unit')->unsigned()->nullable();
            $table->tinyInteger('item_type')->unsigned();
            $table->integer('product_id')->unsigned()->nullable();
            $table->integer('product_set_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('po_id')->references('id')->on('purchase_orders');
            $table->foreign('base_unit')->references('id')->on('units');
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('product_set_id')->references('id')->on('product_sets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_order_details');
    }
}
