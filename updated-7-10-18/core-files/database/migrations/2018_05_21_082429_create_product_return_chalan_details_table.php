<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductReturnChalanDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_return_chalan_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ret_chalan_id')->unsigned();
            $table->integer('item_id')->unsigned();
            $table->tinyInteger('item_type');
            $table->integer('unit_id')->unsigned()->nullable();
            $table->integer('p_ret_d_id')->unsigned();
            $table->integer('quantity');
            $table->timestamps();

            $table->foreign('ret_chalan_id')->references('id')->on('product_return_chalans');
            $table->foreign('p_ret_d_id')->references('id')->on('product_return_details');
            $table->foreign('unit_id')->references('id')->on('units');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_return_chalan_details');
    }
}
