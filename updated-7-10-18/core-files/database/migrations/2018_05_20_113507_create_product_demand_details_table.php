<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductDemandDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_demand_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_demand_id')->unsigned();
            $table->integer('item_id')->unsigned();
            $table->tinyInteger('item_type');
            $table->integer('unit_id')->unsigned();
            $table->integer('quantity');
            $table->timestamps();

            $table->foreign('product_demand_id')->references('id')->on('product_demands');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_demand_details');
    }
}
