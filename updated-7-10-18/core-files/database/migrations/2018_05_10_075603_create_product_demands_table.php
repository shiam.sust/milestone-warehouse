<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductDemandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_demands', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('demand_type')->default(1);
            $table->integer('from_warehouse')->unsigned()->nullable();
            $table->integer('to_warehouse')->unsigned()->nullable();
            $table->integer('product_id')->unsigned()->nullable();
            $table->integer('quantity')->unsigned()->nullable();
            $table->integer('unit_id')->unsigned()->nullable();
            $table->tinyInteger('status')->default(1);
            $table->integer('return_quantity')->unsigned();
            
            $table->foreign('from_warehouse')->references('id')->on('wire_houses');
            $table->foreign('to_warehouse')->references('id')->on('wire_houses');
            $table->foreign('unit_id')->references('id')->on('units');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_demands');
    }
}
