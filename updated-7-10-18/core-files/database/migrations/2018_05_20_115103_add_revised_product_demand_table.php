<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRevisedProductDemandTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_demands', function (Blueprint $table) {
            $table->date('requisition_date')->nullable();
            $table->string('req_no_auto');
            $table->string('req_no_manual')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_demand', function (Blueprint $table) {
            $table->dropColumn('requisition_date','requisition_date','requisition_date');
        });
    }
}
