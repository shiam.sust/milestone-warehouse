<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWhIdToChalansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('chalans', function (Blueprint $table) {
            $table->integer('wh_id')->unsigned()->nullable();
            $table->foreign('wh_id')->references('id')->on('wire_houses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chalans', function (Blueprint $table) {
            $table->dropColumn('wh_id');
        });
    }
}
