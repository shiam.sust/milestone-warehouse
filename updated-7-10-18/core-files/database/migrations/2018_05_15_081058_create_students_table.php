<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('wh_id')->unsigned()->nullable();
            $table->string('year');
            $table->integer('class_id')->unsigned()->nullable();
            $table->integer('quantity');
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
            $table->foreign('wh_id')->references('id')->on('wire_houses');
            $table->foreign('class_id')->references('id')->on('academic_classes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
