<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('wh_id')->unsigned();
            $table->integer('vendor_id')->unsigned();
            $table->string('po_no_auto')->unique();
            $table->string('po_no_manual')->unique()->nullable();
            $table->date('po_date')->nullable();
            $table->timestamps();

            $table->foreign('wh_id')->references('id')->on('wire_houses');
            $table->foreign('vendor_id')->references('id')->on('vendors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_orders');
    }
}
