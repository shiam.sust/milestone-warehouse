@extends('layouts.admin')

@section('content')
<div class="white-box">
    <div class="col-mod-12">
        <div class="col-mod-6 col-lg-6">
                <h3 class="box-title text-success m-b-0">Product Unit</h3>
                <p class="text-muted m-b-30">Update Product Unit</p>
        </div>        
        <div class="col-mod-6 col-lg-6 ">
            <a href="{{ route('product_unit') }}" class="waves-effect pull-right"><button class="btn btn-xs btn-info "><i class="fa fa-arrow-circle-left"></i> ALL PRODUCT UNIT LIST</button></a>
        </div>    
    </div>  
    <div class="clear"></div><hr/>
    <div class="panel-body">
                <form action="{{ route('update_product_unit',['id' => $unit->id]) }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-body">
                      
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Product Unit Name</label>
                                    <input type="text" id="firstName" class="form-control" placeholder="Enter product unit name" name="name" value="{{ $unit->name }}"> </div>
                            </div> 
                       
                                <div class="form-group">
                                    <label class="control-label">Status</label>
                                    <div class="radio-list">
                                        <label class="radio-inline p-0 active">
                                            <div class="radio radio-info">
                                                <input type="radio" name="status" id="radio1" value=1 checked="">
                                                <label for="radio1">active</label>
                                            </div>
                                        </label>
                                        <label class="radio-inline">
                                            <div class="radio radio-info">
                                                <input type="radio" name="status" id="radio2" value=0 >
                                                <label for="radio2">inactive </label>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                             
                        </div>
                         
                    <div class="form-actions">
                        <button type="submit" class="btn btn-success pull-right"> <i class="fa fa-check"></i> UPDATE PRODUCT UNIT INFORMATION</button>
                    </div>
                </form>
    </div>
</div>    
</div>
@endsection