@extends('layouts.admin')

@section('content')
<div class="white-box">
    <h3 class="box-title m-b-0">Update Warehouse <a href="{{ route('wirehouses') }}" class="waves-effect pull-right"><button class="btn btn-sm btn-info "><i class="fa fa-arrow-circle-left"></i> ALL WAREHOUSES LIST</button></a></h3>
    <p class="text-muted m-b-30"> Update Warehouse information</p>
    <hr>
    <form action="{{ route('update_wirehouse',['id' => $wirehouse->id]) }}" method="post">
        {{ csrf_field() }}
        <div class="form-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Warehouse Name <span class="text-danger m-l-5">*</span></label>
                        <input type="text" id="firstName" class="form-control" placeholder="Enter warehouse name."
                               name="name" value="{{ $wirehouse->name }}" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Phone  <span class="text-danger m-l-5">*</span></label>
                        <input type="text" id="firstName" class="form-control" placeholder="+88 01675645158" name="phone"
                               value="{{ $wirehouse->phone }}" required>
                    </div>
                </div>

            </div>
            <!--/row-->

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Email <span class="text-danger m-l-5">*</span></label>
                        <input type="text" id="firstName" class="form-control" placeholder="email@example.com"
                               name="email" value="{{ $wirehouse->email }}" required>
                    </div>
                </div>



                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Mobile No.  <span class="text-danger m-l-5">*</span></label>
                        <input type="text" id="firstName" class="form-control" placeholder="+88 01675645158"
                               name="mobile" value="{{ $wirehouse->mobile }}" required>
                    </div>
                </div>
            </div>
            <!--/row-->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Address  <span class="text-danger m-l-5">*</span></label>
                        <textarea class="form-control" name="address" placeholder="Enter Wirehouse address" required>{{ $wirehouse->address }}</textarea>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label">Status</label>
                        <div class="radio-list">
                            <label class="radio-inline p-0 active">
                                <div class="radio radio-info">
                                    <input type="radio" id="radio1" value=1 name="status" {{ $wirehouse->status == 1 ? 'checked' : '' }}>
                                    <label for="radio1">active</label>
                                </div>
                            </label>
                            <label class="radio-inline">
                                <div class="radio radio-info">
                                    <input type="radio" id="radio2" value=0 name="status" {{ $wirehouse->status == 0 ? 'checked' : '' }}>
                                    <label for="radio2">inactive </label>
                                </div>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Version</label>
                        <select name="version" id="version" class="form-control">
                            <option value="">--Select Version --</option>
                            <option value="1" {{ $wirehouse->version == 1 ? 'selected="selected"' : '' }}>Bangla</option>
                            <option value="2" {{ $wirehouse->version == 2 ? 'selected="selected"' : '' }}>English</option>
                            <option value="3" {{ $wirehouse->version == 3 ? 'selected="selected"' : '' }}>Bangla & English</option>
                        </select>
                    </div>
                </div>

            </div>

        </div>
        <div class="form-group text-right">
            <button type="submit" class="btn btn-success pull-right"> <i class="fa fa-check"></i> UPDATE WAREHOUSE INFORMATION</button>
        </div>
    </form>
</div>    
</div>
@endsection