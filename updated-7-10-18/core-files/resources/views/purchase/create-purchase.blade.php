@extends('layouts.admin')

@section('content')

        <div class="white-box mb-20">
            <h3 class="box-title text-success m-b-20"> Purchase Order<a href="{{ route('purchase-orders') }}" class="btn btn-sm btn-info pull-right"><i class="fa fa-arrow-circle-left"></i> ALL PURCHASE ORDERS LIST</a></h3>
              <p class="text-muted m-b-30">Create New Purchase Order</p>
            <hr>
            <form method="post" action="{{ route('store-purchase-order') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Warehouse <span class="text-danger m-l-5">*</span></label>
                            <select class="form-control" data-placeholder="Choose a Category" tabindex="1" name="wh_id" required>
                                <option value="">Select Warehouse</option>
                                @foreach($wirehouses as $warehouse)
                                    <option value="{{ $warehouse->id }}">{{ $warehouse->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Vendor<span class="text-danger m-l-5">*</span></label>
                            <select class="form-control" data-placeholder="Choose a Category" tabindex="1" name="vendor_id" required>
                                <option value="">--Select Vendor--</option>
                                @foreach($vendors as $vendor)
                                    <option value="{{ $vendor->id }}">{{ $vendor->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Purchase No Auto.<span class="text-danger m-l-5">*</span></label>
                            <input type="text" class="form-control" name="po_no_auto"  value="{{ $po_no_auto }}" readonly required>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Purchase No Manual.</label>
                            <input type="text" class="form-control" name="po_no_manual" >
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Purchase Order Date<span class="text-danger m-l-5">*</span></label>
                            <input type="text" class="form-control datepicker" placeholder="YYYY-MM-DD" name="po_date" required>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Delivery Date</label>
                            <input type="text" class="form-control datepicker" placeholder="YYYY-MM-DD" name="delivery_date">
                        </div>
                    </div>

                </div>

                <create-purchase-order></create-purchase-order>

                <div class="form-group clearfix text-right">
                    <button class="btn btn-success btn-sm"><i class="fa fa-check"></i> SAVE PURCHASE ORDER</button>
                </div>

            </form>

        </div>


@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.datepicker').datepicker({
                "format": 'yyyy-mm-dd',
                "todayHighlight": true,
                "autoclose": true
            });
        });
    </script>
@endsection
