@extends('layouts.admin')

@section('content')

    <div class="content-wraper">
        <div class="white-box mb-20">
            <h3 class="box-title text-success m-b-0">Requisition List</h3>
            <p class="text-muted m-b-30">List Of All Product Requisitions</p>
            <hr/>
            <table class="table table-bordered table-striped" id="productDemands">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>Requisition From</th>
                    <th>Requisition No</th>
                    <th>Date</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($demands as $k => $data)
                    <tr>
                        <td>{{ $k+1 }}</td>
                        <td>{{ $data->fromWireHouse ? $data->fromWireHouse->name : "" }}</td>
                        <td>{{ $data->req_no_auto }} / {{ $data->req_no_manual }}</td>
                        <td>{{ $data->requisition_date }}</td>

                        <td>@if ($data->status == 0)
                                Created
                            @elseif ($data->status == 1)
                                Approved
                            @elseif ($data->status == 2)
                                Completed
                            @endif
                        </td>
                        <td>
                            
                            @if($data->status == 0 && Auth::user()->can('product-requisition-approve'))
                                <a href="{{ route('approve-requisition',['id' => $data->id]) }}" type="button" class="btn btn-warning btn-xs flat" data-toggle="tooltip" title="Approve Requisition"><i class="fa fa-check"></i> Approve</a>
                            @endif
                                <a href="{{ route('show-product-demand',['id' => $data->id]) }}" type="button" class="btn btn-primary btn-xs flat" data-toggle="tooltip" title="Approve Requisition"><i class="fa fa-eye fa-fw"></i> Details</a>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#productDemands").dataTable();
        });

    </script>
@endsection

