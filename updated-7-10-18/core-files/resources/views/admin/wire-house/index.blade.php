@extends('layouts.admin')

@section('content')
    <div class="white-box">
        <h3 class="box-title text-success m-b-0">Warehouses
        @if(Auth::user()->can('wirehouse-create'))
            <a href="{{ route('add_wirehouse') }}" class="waves-effect pull-right"><button class="btn btn-sm btn-info pull-right"><i class="fa fa-plus-circle"></i> CREATE NEW WAREHOUSE</button></a>
        @endif
        </h3>
        <p class="text-muted m-b-30">List of all warehouse</p>
        <hr/>
        <div class="table-responsive col-mod-12">
            <table id="myTable" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="myTable_info">
                <thead>
                <tr role="row">
                    <th >SL </th>
                    <th >Wirehouse </th>
                     <th >Mobile</th>
                    <th >Email</th>
                    <th >Address</th> 
                    <th >Status</th> 
                    <th>Action</th>

                </tr>
                </thead>
                <tbody>


                {{-- // rowspan="{{ count($data->WireHouseContacts) ? count($data->WireHouseContacts) : '1' }}"   --}}



                @foreach($wirehouses as $k => $data)

                    @if($data->status == 0)
                        @continue
                    @endif

                    <tr role="row" class="odd" >
                        <td>{{ $k+1 }}</td>
                        <td>{{ $data->name }}</td>
                        <td>{{ $data->mobile }}</td>
                        <td>{{ $data->email }}</td>
                      
                        <td>{{ $data->address }}</td> 
                          <td><span class="badge badge-info">{{ $data->status ? 'active' : 'inactive' }}</span></td>


                        <td>
                            @if(Auth::user()->can('wirehouse-update'))
                            <a href="edit_wirehouse/{{ $data->id }}" type="button" class="btn btn-warning btn-xs"  data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i> UPDATE</a>
                            @endif
                            @if(Auth::user()->can('wirehouse-delete'))
                                <a href="delete_wirehouse/{{ $data->id }}" type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i> DELETE
                                </a>
                            @endif
                        </td>

                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>

        @endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#myTable').DataTable();
            $(document).ready(function() {
                var table = $('#example').DataTable({
                    "columnDefs": [{
                        "visible": false,
                        "targets": 2
                    }],
                    "order": [
                        [2, 'asc']
                    ],
                    "displayLength": 25,
                    "drawCallback": function(settings) {
                        var api = this.api();
                        var rows = api.rows({
                            page: 'current'
                        }).nodes();
                        var last = null;
                        api.column(2, {
                            page: 'current'
                        }).data().each(function(group, i) {
                            if (last !== group) {
                                $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                                last = group;
                            }
                        });
                    }
                });
                // Order by the grouping
                $('#example tbody').on('click', 'tr.group', function() {
                    var currentOrder = table.order()[0];
                    if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                        table.order([2, 'desc']).draw();
                    } else {
                        table.order([2, 'asc']).draw();
                    }
                });
            });
        });
        $('#example23').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });

    </script>
@endsection