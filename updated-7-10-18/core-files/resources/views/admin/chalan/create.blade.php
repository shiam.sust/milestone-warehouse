@extends('layouts.admin')

@section('content')

    <div class="content-wraper">

        <form action="{{ route('store-chalan') }}" method="post">
            {{ csrf_field() }}
            <div class="white-box m-b-15">
                <h3 class="box-title text-success">CREATE PURCHASE CHALLAN <a href="{{ route('chalans') }}" class="btn pull-right btn-info btn-sm"><i class="fa fa-arrow-circle-left"></i> ALL PURCHASE CHALAN LIST</a></h3>
                <p class="text-muted m-b-30">Create New Challan </p>
                <hr>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="select-purchase-order">PURCHASE ORDER</label>
                            <select name="purchase_order" id="select-purchase-order" class="form-control" {{ $po_id ? 'disabled="true"' : '' }} required>
                                <option value="">---Select PO---</option>
                                @foreach($purchase_orders as $purchase_order)
                                    <option value="{{ $purchase_order->id }}" {{ $purchase_order->id == $po_id ? 'selected="selected"' : '' }}>{{ $purchase_order->po_no_auto }}</option>
                                @endforeach
                            </select>
                            @if($po_id)
                                <input type="hidden" name="purchase_order" value="{{ $po_id }}">
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="po-auto">CHALAN NO AUTO</label>
                            <input type="text" name="chalan_no" class="form-control" value="{{ $ch_auto }}" readonly>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="po-auto">CHALAN NO MANUAL</label>
                            <input type="text" name="chalan_no_manual" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="po-auto">CHALAN DATE</label>
                            <input type="text" name="chalan_date" class="form-control datepicker" placeholder="YYYY-MM-DD" required>
                        </div>
                    </div>
                </div>
            </div>

            <div class="white-box">
                <h4>PURCHASE ORDER ITEM DETAILS</h4>

                <create-central-chalan ref="getPurchaseOrderItem"></create-central-chalan>

                <div class="form-group text-right">
                    <button class="btn btn-success btn-sm flat" type="submit">SAVE</button>
                </div>

            </div>


        </form>

    </div>

@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.datepicker').datepicker({
                "format": 'yyyy-mm-dd',
                "todayHighlight": true,
                "autoclose": true,
                "startDate": new Date()
            });
        });

    </script>
@endsection
