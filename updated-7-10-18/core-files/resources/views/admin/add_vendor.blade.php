@extends('layouts.admin')

@section('content')
    <div class="white-box">
        <h3 class="box-title text-success m-b-15">Create Vendor <a href="{{ route('vendors') }}" class="waves-effect pull-right"><button class="btn btn-sm btn-info "><i class="fa fa-arrow-circle-left"></i> ALL VENDORS LIST</button></a></h3>
          <p class="text-muted m-b-30">Create New Vendor</p>
        <hr>
        <form action=" {{ route('post.vendor') }} " method="post">
            {{ csrf_field() }}

            <div class="form-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Vendor Name <span class="text-danger m-l-5">*</span></label>
                            <input type="text" id="firstName" class="form-control" placeholder="Vendor Name" name="name" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Mobile No. </label>
                            <input type="text" id="firstName" class="form-control" placeholder="Ex: +88 01675645158"
                                   name="mobile">
                        </div>
                    </div>

                </div>
                <!--/row-->

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Email</label>
                            <input type="email" id="firstName" class="form-control" placeholder="email@example.com" name="email">
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Address</label>
                            <input type="text" id="firstName" class="form-control" placeholder="Address"
                                   name="address">
                        </div>
                    </div>
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Invoice Address </label>
                            <textarea class="form-control" placeholder="Enter Invoice address" name="invoiceaddress"></textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Status <span class="text-danger m-l-5">*</span></label>
                            <div class="radio-list">
                                <label class="radio-inline p-0">
                                    <div class="radio radio-info">
                                        <input type="radio" name="status" id="radio1" value="1" checked>
                                        <label for="radio1">active</label>
                                    </div>
                                </label>
                                <label class="radio-inline">
                                    <div class="radio radio-info">
                                        <input type="radio" name="status" id="radio2" value=0 >
                                        <label for="radio2">inactive </label>
                                    </div>
                                </label>
                            </div>
                        </div>
                    </div>

                </div>
                <h3 class="box-title text-success">Vendor Contact person details</h3>
                <hr>

                <div class="row">

                    <div class="col-md-6 ">
                        <div class="form-group">
                            <label>Person Name <span class="text-danger m-l-5">*</span></label>
                            <input type="text" class="form-control" name="pname" required> </div>
                    </div>

                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Contact No. </label>
                            <input type="text" class="form-control" name="pmobile"> </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">


                    <!--/span--> <div class="col-md-6">
                        <div class="form-group">
                            <label>Designation</label>
                            <input type="text" class="form-control" name="pdesignation">
                        </div>
                    </div>
                </div>

            </div>
            <div class="form-group text-right">
                <button type="submit" class="btn btn-success pull-right"> <i class="fa fa-check"></i> SAVE VENDOR INFORMATION</button>
            </div>
        </form>
    </div>
    </div>
@endsection