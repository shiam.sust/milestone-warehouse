@extends('layouts.admin')

@section('content')
<div class="white-box">
    <div class="col-mod-12">
        <div class="col-mod-6 col-lg-6">
                <h3 class="box-title m-b-0">Wire Houses</h3>
                <p class="text-muted m-b-30">List of all wire house</p>
        </div>      
        <div class="col-mod-6 col-lg-6 ">
            <a href="{{ route('add_wirehouse') }}" class="waves-effect pull-right"><button class="btn btn-xs btn-info pull-right"><i class="fa fa-plus"></i> Add New Wirehouse</button></a>
        </div>    
    </div>  
    <div class="clear"></div><hr/>
<div class="table-responsive col-mod-12">

                                

                                <table id="myTable" class="table table-striped dataTable no-footer" role="grid" aria-describedby="myTable_info">
                                    <thead>
                                        <tr role="row">
                                            <th >Wirehouse </th>
                                            <th >Address</th>
                                            
                                            <th >Mobile</th>
                                            
                                           <th>Action</th> 
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        
                                      {{-- // rowspan="{{ count($data->WireHouseContacts) ? count($data->WireHouseContacts) : '1' }}"   --}}
                                     
                                        
                                        
                                    @foreach($wirehouses as $data) 
                                     
                                    @if($data->status == 0)
                                        @continue
                                    @endif
                                        
                                    <tr role="row" class="odd" >

                                        <td class="sorting_1">{{ $data->name }}</td>
                                        <td>{{ $data->address }}</td>
                                        
                                        <td>{{ $data->mobile }}</td>

                                        
                                        <td>                                         
                                            <a href="edit_wirehouse/{{ $data->id }}" type="button" class="btn btn-warning btn-circle"  data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i> </a>

                                            <a href="delete_wirehouse/{{ $data->id }}" type="button" class="btn btn-danger btn-circle" data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i>
                                            </a>  
                                        </td>
                                        
                                    </tr>
                                    @endforeach

                                    </tbody>
                                </table>

                                 
                        </div>

@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    $('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });

    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip(); 
    });
    
</script>
@endsection