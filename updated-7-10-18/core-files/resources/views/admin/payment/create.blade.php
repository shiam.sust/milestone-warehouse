@extends('layouts.admin')

@section('content')

    <div class="content-wraper">
        <div class="white-box">
            <h3 class="box-title text-success">CREATE PAYMENT <a href="{{ route('payments') }}" class="btn btn-sm btn-info flat pull-right"><i class="fa fa-arrow-circle-left"></i> ALL PAYMENTS</a></h3>
            <p class="text-muted m-b-30">Create New Payment Through Challan </p>
            <hr/>
            <form action="{{ route('store-payment') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="chalan-id">Select Challan<span class="text-danger m-l-5">*</span></label>

                            @if($ch_id)
                                <input type="hidden" name="chalan_id" value="{{ $ch_id }}">
                                <select name="chalan_id" id="chalan-id" class="form-control" required disabled>
                                    <option value="">---Select Challan NO---</option>
                                    @foreach($chalans as $chalan)
                                        <option value="{{ $chalan->id }}" {{ $chalan->id == $ch_id ? 'selected="selected"' : '' }}>{{ $chalan->chalan_no }}</option>
                                    @endforeach
                                </select>
                            @else
                                <select name="chalan_id" id="chalan-id" class="form-control" required>
                                    <option value="">---Select Challan NO---</option>
                                    @foreach($chalans as $chalan)
                                        <option value="{{ $chalan->id }}">{{ $chalan->chalan_no }}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="chalan-id">Payment Method<span class="text-danger m-l-5">*</span></label>
                            <select name="payment_method_id" id="chalan-id" class="form-control" required>
                                <option value="">---Select Method---</option>
                                @foreach($payment_methods as $payment_method)
                                    <option value="{{ $payment_method->id }}">{{ $payment_method->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="amount">Amount<span class="text-danger m-l-5">*</span></label>
                            <input id="totalAmount" type="text" class="form-control" name="amount" required>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="amount">Discount</label>
                            <input type="text" class="form-control" name="discount">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="amount">Payment Date <span class="text-danger m-l-5">*</span></label>
                            <input type="text" class="form-control datepicker" name="payment_date" placeholder="YYYY-MM-DD" required>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="amount">Attachment</label>
                            <input type="file" name="attachment" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-group text-right">
                    <button type="submit" class="btn btn-success btn-sm flat"> <i class="fa fa-check"></i> SAVE PAYMENT INFORMATION</button>
                </div>

            </form>
        </div>
        <div class="white-box">
            <h3 class="box-title text-success">CHALLAN DETAILS</h3>
            <hr/>
            <chalan-items ref="getchalanItems"></chalan-items>
        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#chalanListTable").dataTable();

            $('.datepicker').datepicker({
                "format": 'yyyy-mm-dd',
                "todayHighlight": true,
                "autoclose": true,
                daysOfWeekDisabled: [0,6]
            });
        });

    </script>
@endsection
