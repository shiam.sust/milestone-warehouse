@extends('layouts.admin')

@section('content')

    <div class="content-wraper">
        <div class="white-box mb-20">
            <h3 class="box-title text-success">PAYMENT LISTS <a href="{{ route('create-payment') }}" class="btn btn-sm btn-info flat pull-right"><i class="fa fa-plus-circle"></i> CREATE NEW PAYMENT</a></h3>
            <p class="text-muted m-b-30">All payment history </p>
            <hr>
            <table class="table table-bordered table-striped" id="chalanListTable">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>CH No Auto/Manual</th>
                    <th>Vendor</th>
                    <th>Method</th>
                    <th>Discount</th>
                    <th>Amount</th>
                    <th>Due</th>
                    <th>Date</th>
                </tr>
                </thead>

                <tbody>
                @foreach($payments as $k => $payment)
                    <tr>
                        <td>{{ $k+1 }}</td>
                        <td>{{ $payment->chalan->chalan_no }} / {{ $payment->chalan->chalan_no_manual }}</td>
                        <td>{{ $payment->chalan->purchaseOrder->vendor->name }}</td>
                        <td>{{ $payment->paymentMethod->name }}</td>
                        <td>{{ $payment->discount }}</td>
                        <td>{{ $payment->amount }}</td>
                        <td>{{ $payment->due }}</td>
                        <td>{{ $payment->payment_date }}</td>
                    </tr>
                @endforeach
                </tbody>

            </table>
        </div>

    </div>

@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#chalanListTable").dataTable();
        });

    </script>
@endsection
