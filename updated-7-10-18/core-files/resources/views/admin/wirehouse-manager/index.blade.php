@extends('layouts.admin')

@section('content')

    <div class="content-wraper">
        <div class="white-box mb-20">
            <h3 class="box-title text-success ">WAREHOUSE MANAGERS
                @if(Auth::user()->can('wirehouse-manager-create'))
                    <a href="{{ route('add_wirehouse_manager') }}" class="btn btn-sm btn-info pull-right"><i class="fa fa-plus-circle"></i> CREATE NEW WAREHOUSE MANAGER</a>
                @endif
            </h3>
            <p class="text-muted m-b-30"> All Warehouse Managers List</p>
            <hr>
            <table class="table table-bordered" id="wireHouseManager">
                <thead>
                <tr>
                    <th >SL </th>
                    <th >Name </th>
                    <th >Mobile</th>
                    <th >Email</th>
                    <th>Designation</th>
                    <th>Warehouse name</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($managers as $k => $manager)
                    <tr>
                        <td>{{ $k+1}}</td>
                        <td>{{ $manager->name }}</td>
                        <td>{{ $manager->mobile }}</td>
                        <td>{{ $manager->email }}</td>
                        <td>{{ $manager->designation }}</td>
                        <td>{{ $manager->wireHouse->name }}</td>
                        <td>
                            @if(Auth::user()->can('wirehouse-manager-update'))
                                <a href="edit_wh_manager/{{ $manager->id }}" type="button" class="btn btn-warning btn-xs"  data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i> UPDATE</a>
                            @endif
                            @if(Auth::user()->can('role-update'))
                                <a href="{{ route('update-role',['id' => $manager->id]) }}"  class="btn btn-danger btn-xs" data-toggle="tooltip" title="Update Role"><i class="fa fa-times-circle"></i> UPDATE ROLE
                                </a>
                            @endif

                            @if(Auth::user()->can('wirehouse-manager-delete'))
                            <a href="delete_wirehouse_manager/{{ $manager->id }}" type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Delete"><i class="fa fa-times-circle"></i> DELETE
                            </a>
                            @endif
                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>

    </div>

@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#wireHouseManager").dataTable();
        });

    </script>
@endsection
