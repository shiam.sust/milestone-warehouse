@extends('layouts.admin')

@section('content')

    <div class="content-wraper">
        <div class="white-box mb-20">
            <h3 class="box-title text-success ">UPDATE WAREHOUSE MANAGER ROLES</h3>
            <hr>
            <p><strong>Manager Name: </strong>{{ $manager->name }}</p>
            <form action="{{ route('edit-role',['id' => $manager->id]) }}" method="post">
                {{ csrf_field() }}
                <div class="row">
                    @foreach($roles as $role)
                        <div class="col-md-3 col-sm-6">
                            <div class="form-group">
                                <input type="checkbox" name="roles[]" value="{{ $role->id }}" {{ $role->users ? $role->users->contains($user->id) ? 'checked' : '' : '' }}>
                                <label class="check" for="checkboxCustom3">{{ $role->name }}</label>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="form-group text-right">
                    <button type="submit" class="btn btn-primary btn-sm">Update Manager Role</button>
                </div>
            </form>
        </div>

    </div>

@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#wireHouseManager").dataTable();
        });

    </script>
@endsection