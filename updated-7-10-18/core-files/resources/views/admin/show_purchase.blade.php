@extends('layouts.admin')

@section('content')

<div class="content-wraper">
    <div class="white-box mb-20">
        <h3 class="box-title">Search Purchase Order</h3>
    <form>
        <div class="row">
                                                
            <div class="col-md-5">
                <div class="form-group">
                    <label>Person Name</label>
                    <input type="text" class="form-control" name="pname"> </div>
            </div>
            
            <!--/span-->
            <div class="col-md-5">
                <div class="form-group">
                    <label>Contact No.</label>
                    <input type="text" class="form-control" name="pmobile"> </div>
            </div>
            <!--/span-->
            <div class="col-md-2">
                <div class="form-group">
                    <button type="submit" class="btn btn-success" style="margin-top: 25px "> <i class="fa fa-check"></i> Search</button>
            </div>
        </div>
    </div>
    </form>
    
    </div>

<div class="white-box">
    
                                            
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          <span>PO No: <strong class="text-success">123456</strong></span>
          <span>| Customer No:</span>
          <span>| Vendor name:</span>
          <span>| Date:</span>

        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Product</th>
                                            <th>Quantity</th>
                                            <th>Unit Price</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingTwo">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Collapsible Group Item #2
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body">
        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Collapsible Group Item #3
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
      </div>
    </div>
  </div>
</div>
                                 
</div>

<div class="white-box mb-20">
        <h3 class="box-title">Purchase Create</h3>
    <form>
        <div class="row">                                            
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Wirehouse</label>
                    <select class="form-control" data-placeholder="Choose a Category" tabindex="1" name="unit">
                        <option value="">--Select Unit--</option>
                        
                        <option value="">hello</option>
                        <option value="">world</option>
                        
                      
                    </select>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">Vendor</label>
                    <select class="form-control" data-placeholder="Choose a Category" tabindex="1" name="unit">
                        <option value="">--Select Unit--</option>
                        
                        <option value="">hello</option>
                        <option value="">world</option>
                        
                      
                    </select>
                </div>
            </div>       
        </div>

        <div class="row">                                            
            <div class="col-md-6">
                <div class="form-group">
                    <label>Purchase No.</label>
                    <input type="text" class="form-control" name="pname" disabled="" value="#123456"></div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label>Purchase Name</label>
                    <input type="text" class="form-control" name="pname"> </div>
            </div>       
        </div>

        <div class="row">                                            
            <div class="col-md-6">
                <div class="form-group">
                    <label>Purchase Date.</label>
                    <input type="text" class="form-control" name="pname" ></div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label>Delivery Date.</label>
                    <input type="text" class="form-control" name="pname"> </div>
            </div>       
        </div>

    </form>
    
</div>

</div>

<div class="white-box">
    <example-component></example-component>
</div>

@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    $('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    
</script>
@endsection
