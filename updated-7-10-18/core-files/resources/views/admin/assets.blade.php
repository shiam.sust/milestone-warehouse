@extends('layouts.admin')


@section('content')

    <section class="section section-designation-list">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <h3 class="box-title m-b-0">PRODUCTS LIST <button type="button" class="btn btn-info pull-right" data-toggle="modal" data-target="#asset" data-whatever="@fat">CREATE PRODUCT</button></h3>
                        <p class="text-muted m-b-30">All asset with their type and code</p>
                        <div class="table-responsive">
                            <table id="example" class="table display">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>PRODUCT NAME</th>
                                    <th>CATEGORY</th>
                                    <th>MODEL</th>
                                    <th>MANUFACTURER</th>
                                    <th class="text-right">ACTION</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $product)
                                    <tr>
                                        <td>{{ $product->id }}</td>
                                        <td>{{ $product->name }}</td>
                                        <td>{{ $product->category ? $product->category->name : '' }}</td>
                                        <td>{{ $product->manufacturer ? $product->manufacturer->name : '' }}</td>
                                        <td class="text-right">
                                            <button type="button" data-id="{{ $product->id }}" class="btn btn-warning btn-edit waves-effect waves-light" data-toggle="modal" data-target="#edit-asset" data-whatever="@fat"> <i class="fa fa-times m-r-5"></i> <span>EDIT</span></button>
                                            <a href="#" class="btn btn-danger waves-effect waves-light"><i class="fa fa-times m-r-5"></i> <span>DELETE</span></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade in" id="asset" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="{{ route('product') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="exampleModalLabel1">CREATE PRODUCT</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="control-label">PRODUCT NAME *</label>
                                    <input type="text" class="form-control" id="name" name="name" required>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="category_id" class="control-label">PRODUCT CATEGORY</label>
                                    <select name="category_id" id="category_id" class="form-control">
                                        <option value="">SELECT PRODUCT CATEGORY</option>
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="manufacturer_id" class="control-label">MANUFACTURER</label>
                                    <select name="manufacturer_id" id="manufacturer_id" class="form-control">
                                        <option value="">Select Manufacturer</option>
                                        @foreach($manufacturers as $manufacturer)
                                            <option value="{{ $manufacturer->id }}">{{ $manufacturer->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="base_unit" class="control-label">BASE UNIT</label>
                                    <select name="base_unit" id="base_unit" class="form-control">
                                        <option value="">Select Base Unit</option>
                                        @foreach($units as $unit)
                                            <option value="{{ $unit->id }}">{{ $unit->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>



                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">CREATE</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade in" id="edit-asset" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form action="#" id="edit-asset-form" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="exampleModalLabel1">UPDATE PRODUCT</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="name" class="control-label">PRODUCT NAME *</label>
                                    <input type="text" class="form-control" id="name" name="name" required>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="category_id" class="control-label">PRODUCT CATEGORY</label>
                                    <select name="category_id" id="category_id" class="form-control">
                                        <option value="">SELECT PRODUCT CATEGORY</option>
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="manufacturer_id" class="control-label">MANUFACTURER</label>
                                    <select name="manufacturer_id" id="manufacturer_id" class="form-control">
                                        <option value="">Select Manufacturer</option>
                                        @foreach($manufacturers as $manufacturer)
                                            <option value="{{ $manufacturer->id }}">{{ $manufacturer->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="base_unit" class="control-label">BASE UNIT</label>
                                    <select name="base_unit" id="base_unit" class="form-control">
                                        <option value="">Select Base Unit</option>
                                        @foreach($units as $unit)
                                            <option value="{{ $unit->id }}">{{ $unit->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">UPDATE</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.btn-edit').click(function () {
                var id = $(this).data('id');
                var url = "{{ route('get-product') }}/"+id;
                var formUrl = "{{ route('update-product') }}/"+id;

                $.ajax({url: url, success: function(result){
                    $("#edit-asset-form").attr('action',formUrl);
                    $("#edit-asset-form input[name='name']").val(result.name);
                    $("#edit-asset-form select[name='category_id']").val(result.category_id);
                    $("#edit-asset-form select[name='manufacturer_id']").val(result.manufacturer_id);
                    $("#edit-asset-form select[name='base_unit']").val(result.base_unit);
                }});
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

        });
    </script>
@endsection
@endsection