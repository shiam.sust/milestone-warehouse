@extends('layouts.admin')

@section('content')

    <section class="section section-designation-list">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <h3 class="box-title m-b-0">MANUFACTURERS LIST <button type="button" class="btn btn-info pull-right" data-toggle="modal" data-target="#manufacturer" data-whatever="@fat">CREATE MANUFACTURER</button></h3>
                        <p class="text-muted m-b-30">List of all manufacturers.</p>
                        <div class="table-responsive">
                            <table id="example" class="table display">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>MANUFACTURER</th>
                                    <th class="text-right">ACTION</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($manufacturers as $manufacturer)
                                    <tr>
                                        <td>{{ $manufacturer->id }}</td>
                                        <td>{{ $manufacturer->name }}</td>
                                        <td class="text-right">
                                            <button type="button" data-id="{{ $manufacturer->id }}" class="btn btn-warning btn-edit waves-effect waves-light" data-toggle="modal" data-target="#edit-manufacturer" data-whatever="@fat"> <i class="fa fa-times m-r-5"></i> <span>EDIT</span></button>
                                            <a href="#" class="btn btn-danger waves-effect waves-light"><i class="fa fa-times m-r-5"></i> <span>DELETE</span></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade in" id="manufacturer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{ route('manufacturer') }}" method="post">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="exampleModalLabel1">CREATE MANUFACTURER</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name" class="control-label">MANUFACTURER NAME *</label>
                            <input type="text" class="form-control" id="name" name="name" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">CREATE</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade in" id="edit-manufacturer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="#" id="edit-manufacturer-form" method="post">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="exampleModalLabel1">UPDATE MANUFACTURER</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name" class="control-label">MANUFACTURER NAME *</label>
                            <input type="text" class="form-control" id="name" name="name" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">UPDATE</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.btn-edit').click(function () {
                var id = $(this).data('id');
                var url = "{{ route('manufacturer') }}/"+id;
                var formUrl = "{{ route('update-manufacturer') }}/"+id;

                $.ajax({url: url, success: function(result){
                    $("#edit-manufacturer-form").attr('action',formUrl);
                    $("#edit-manufacturer-form input[name='name']").val(result.name);
                }});
            });
        });
    </script>
@endsection