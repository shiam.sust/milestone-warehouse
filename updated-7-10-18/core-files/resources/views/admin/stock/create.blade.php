@extends('layouts.admin')

@section('content')

    <div class="content-wraper">
        <div class="white-box mb-20">
            <h3 class="box-title">
                CREATE INITIAL PRODUCT QUANTITY
                <a href="{{ route('balance-product') }}" class="btn btn-info btn-sm pull-right"><i class="fa fa-fw fa-list"></i>Stocked Products</a>
            </h3>
        </div>

        <div class="white-box">
            <form action="{{ route('store-initial-product-quantity') }}" method="post">
                {{ csrf_field() }}
                <create-initial-item :items="{products: {{ $products }},units: {{ $units }}}"></create-initial-item>

            </form>
        </div>

    </div>

@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {

        });

    </script>
@endsection
