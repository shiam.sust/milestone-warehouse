@extends('layouts.admin')

@section('content')

    <div class="content-wraper">
        <div class="white-box mb-20">
            <h3 class="box-title">
                PRODUCT SET INITIAL QUANTITY
                @if(Auth::user()->can('initial-product-set-create'))
                    <a href="{{ route('create-initial-product-set-quantity') }}" class="btn btn-info btn-sm pull-right"><i class="fa fa-fw fa-plus-circle"></i>Create Initial Set Quantity</a>
                @endif
            </h3>
            <table id="myTable" class="table table-bordered dataTable no-footer">
                <thead>
                <tr>
                    <th>SL </th>
                    <th >Warehouse</th>
                    <th >Product Set Name </th>
                    <th >Product Set Details </th>
                    <th >Initial Quantity</th>
                    <th>Action</th>

                </tr>
                </thead>
                <tbody>
                @foreach($product_sets as $k => $detail)

                    <tr>
                        <td>{{ $k+1 }}</td>
                        <td>{{ $detail->warehouse->name }}</td>
                        <td >{{ $detail->productSet->name }}</td>
                        <td >
                            @foreach($detail->productSet->productSetDetails as $k => $product_set_detail)
                                @if($k==0)
                                    <ul>
                                @endif
                                        <li>{{ $product_set_detail->product->name  }} [ {{ $product_set_detail->quantity }} {{ $product_set_detail->unit ? $product_set_detail->unit->name : '' }} ]</li>
                                @if($k+1 == count($detail->productSet->productSetDetails))
                                    </ul>
                                @endif
                            @endforeach
                        </td>
                        <td >{{ $detail->initial_quantity }}</td>
                        <td>
                            @if(Auth::user()->can('initial-product-set-update'))
                                <button type="button" data-value="{{ $detail->initial_quantity }}" data-url="{{ route('update-initial-product-set-quantity',['id' => $detail->id]) }}" class="btn btn-primary btn-xs btn-edit" data-toggle="modal" data-target="#edit-quantity">
                                    <i class="fa fa-edit"></i> Edit
                                </button>
                            @endif
                        </td>
                    </tr>


                @endforeach

                </tbody>
            </table>
        </div>

    </div>
    @if(Auth::user()->can('initial-product-set-update'))
        <div class="modal fade" id="edit-quantity" tabindex="-1" role="dialog" aria-labelledby="editQuantityLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="#" id="update-quantity" method="post">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Update Initial Quantity</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Quantity</label>
                            <input type="text" name="quantity" class="form-control" id="quantity">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endif

@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#myTable").dataTable();

            $(document).on('click','.btn-edit',function(){
                var url = $(this).data('url');
                var v = $(this).data('value');
                $('#update-quantity #quantity').val(v);
                $('#update-quantity').attr('action',url);
            });
        });

    </script>
@endsection
