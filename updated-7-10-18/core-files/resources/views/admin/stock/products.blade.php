@extends('layouts.admin')

@section('content')

    <div class="content-wraper">
        <div class="white-box mb-20">
            <h3 class="box-title">
                CURRENT PRODUCT STOCK
                @if(Auth::user()->can('initial-product-create'))
                    <a href="{{ route('create-initial-product-quantity') }}" class="btn btn-info btn-sm pull-right"><i class="fa fa-fw fa-plus-circle"></i>Create Initial Quantity</a>
                @endif
            </h3>
        </div>

        <div class="white-box">
            <table class="table table-bordered table-striped" id="product-table">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>Warehouse</th>
                    <th>Name</th>
                    <th>Initial Quantity</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $k => $product)
                    <tr>
                        <td>{{ $k+1 }}</td>
                        <td>{{ $product->warehouse->name }}</td>
                        <td>{{ $product->product->name }}</td>
                        <td>{{ $product->initial_quantity }}</td>
                        <td>
                            @if(Auth::user()->can('initial-product-update'))
                                <button type="button" data-value="{{ $product->initial_quantity }}" data-url="{{ route('update-initial-product-quantity',['id' => $product->id]) }}" class="btn btn-primary btn-xs btn-edit" data-toggle="modal" data-target="#edit-quantity">
                                    <i class="fa fa-edit"></i> Edit
                                </button>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>
    @if(Auth::user()->can('initial-product-update'))
        <div class="modal fade" id="edit-quantity" tabindex="-1" role="dialog" aria-labelledby="editQuantityLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="#" id="update-quantity" method="post">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Update Initial Quantity</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Quantity</label>
                            <input type="text" name="quantity" class="form-control" id="quantity">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endif

@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#product-table").dataTable();
            $(document).on('click','.btn-edit',function(){
                var url = $(this).data('url');
                var v = $(this).data('value');
                $('#update-quantity #quantity').val(v);
                $('#update-quantity').attr('action',url);
            });
        });

    </script>
@endsection
