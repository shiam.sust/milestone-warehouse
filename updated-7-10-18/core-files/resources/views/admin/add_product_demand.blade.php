@extends('layouts.admin')

@section('content')
    <div class="content-wraper">
        <div class="white-box">
            <h3 class="box-title m-b-0">
                Product Requisition
                <a href="{{ route('product_demand') }}" class="waves-effect pull-right btn btn-sm flat"><i class="fa fa-list"></i> All Requisitions</a>
            </h3>
            <p class="text-muted m-b-10">Add new product requisition</p>
        </div>
    </div>
    <div class="white-box">
        <div class="panel-body">
            <form action="{{ route('product_demand') }}" method="post">
                {{ csrf_field() }}

                <div class="form-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" style="margin-top: 25px;">
                                <label class="control-label">Choose Requisition Type</label>
                                <div class="radio-list">
                                    <label class="radio-inline p-0">
                                        <div class="radio radio-info">
                                            <input type="radio" name="demand_type" id="radio1" value="1" id="radio" >
                                            <label for="radio1">Product</label>
                                        </div>
                                    </label>
                                    <label class="radio-inline">
                                        <div class="radio radio-info">
                                            <input type="radio" name="demand_type" id="radio2" value="2" id="radio" >
                                            <label for="radio2">Product Set</label>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Requisition To</label>
                                <select class="form-control" data-placeholder="Choose a Category" tabindex="1" name="towirehouse">
                                    <option value="">--Select Warehouse--</option>
                                    @foreach($wh as $data)
                                        <option value="{{ $data->id }}">{{ $data->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>



                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Products</label>
                                <select class="form-control" data-placeholder="Choose a Category" tabindex="1" name="product" id="proSet">
                                </select>
                            </div>
                        </div>

                    </div>
                    <!--/row


                    -->

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Quantity</label>
                                <input type="text" id="firstName" class="form-control" name="quantity"></div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Unit</label>
                                <select class="form-control" data-placeholder="Choose a Category" tabindex="1" name="unit">
                                    <option value="">--Select Product Unit--</option>
                                    @foreach($unit as $data)
                                        <option value="{{ $data->id }}">{{ $data->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <button type="submit" class="btn btn-success pull-right"> <i class="fa fa-check"></i> Save</button>
                        <button type="button" class="btn btn-default pull-right">Cancel</button>
                    </div>
            </form>
        </div>
    </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">

    </script>


@endsection