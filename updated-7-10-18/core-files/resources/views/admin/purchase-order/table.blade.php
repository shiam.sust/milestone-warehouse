<table class="table table-bordered" id="purchaseOrderList">
    <thead>
    <tr>
        <th>SL</th>
        <th>PO No Auto/Manual</th>
        <th>Wirehouse</th>
        <th>Vendor</th>
        <th>ITEMS</th>
        <th>Date</th>
        <th>Status</th>
        <th class="text-right">Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach($purchase_orders as $k => $purchase_order)
        <tr>
            <td>{{ $k+1 }}</td>
            <td>{{ $purchase_order->po_no_auto }} / {{ $purchase_order->po_no_manual }}</td>
            <td>{{ $purchase_order->wireHouse->name }}</td>
            <td>{{ $purchase_order->vendor->name }}</td>
            <td>
                @foreach($purchase_order->purchaseOrderDetails as $k => $od)
                    @if($k==0)
                        <ul>
                            @endif
                            <li>{{ $od->item->name  }}</li>
                            @if($k+1 == count($purchase_order->purchaseOrderDetails))
                        </ul>
                    @endif
                @endforeach
            </td>
            <td>{{ $purchase_order->po_date }}</td>
            <td>
                @if($purchase_order->status == 0)
                    <span class="badge badge-info">Created</span>
                @elseif($purchase_order->status == 1)
                    <span class="badge badge-warning">Partially Received</span>
                @elseif($purchase_order->status == 2)
                    <span class="badge badge-success">Complete</span>
                @endif
            </td>
            <td class="text-right">
                <a href="{{ route('get-purchase-order',['id' => $purchase_order->id]) }}" class="btn btn-primary btn-xs text-center" data-toggle="tooltip" data-original-title="view"><i class="ti-eye"></i> DETAILS</a>
                @if(!$purchase_order->status)
                    <a href="{{ route('edit-purchase-order',['id' => $purchase_order->id]) }}" class="btn btn-warning btn-xs text-center" data-toggle="tooltip" data-original-title="edit"><i class="fa fa-pencil-square-o"></i> UPDATE</a>
                @endif

                @if(!$purchase_order->status == 2)
                    <a href="{{ route('return-purchase-order',['id' => $purchase_order->id]) }}" class="btn btn-danger btn-xs text-center" data-toggle="tooltip" data-original-title="return"><i class="ti-reload"></i> RETURN</a>
                @endif

            </td>
        </tr>
    @endforeach
    </tbody>
</table>