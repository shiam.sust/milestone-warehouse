@extends('layouts.admin')

@section('content')

    <div class="content-wraper">
        <div class="white-box mb-20">
            <h3 class="box-title text-success">Purchase Order Details <a href="{{ route('purchase-orders') }}" class="btn btn-xs btn-info pull-right"><i class="fa-fw fa fa-arrow-circle-left"></i>PURCHASE ORDER LIST</a></h3>
            <hr>
            @if($purchase_order)
                <p class="m-b-20">
                    <span class="text-success"><strong>PO No:</strong> {{ $purchase_order->po_no_auto }}</span>
                    <span class="text-info">| <strong>Wirehouse:</strong> {{ $purchase_order->wireHouse->name }}</span>
                    <span class="text-warning">| <strong>Vendor name</strong>: {{ $purchase_order->vendor->name }}</span>
                    <span class="text-primary">| <strong>Date:</strong> {{ $purchase_order->po_date }}</span>
                </p>
            @endif

            @if(!empty($purchase_order))
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Product</th>
                        <th>Ordered Quantity</th>
                        <th>Received Qty</th>
                        <th>Returned Qty</th>
                        <th>Rem Qty</th>
                        <th>Unit Price</th>
                        <th>Sub Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($purchase_order->purchaseOrderDetails as $od)
                        <tr>
                            <td>{{ $od->item->name  }}</td>
                            <td>{{ $od->quantity }}</td>
                            <td>{{ $od->chalanDetails ? $od->chalanDetails->sum('received_qty') : 0 }}</td>
                            <td>{{ $od->returned_quantity }}</td>
                            <td>{{ $od->chalanDetails ? $od->quantity - ($od->chalanDetails->sum('received_qty') + $od->returned_quantity) : ($od->quantity -  $od->returned_quantity) }}</td>
                            <td>{{ $od->unit_price }}</td>
                            <td>{{ $od->sub_total }}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="6">Grand Total</td>
                        <td>{{ $purchase_order->total_amount }}</td>
                    </tr>

                    </tbody>
                </table>
            @else
                <p class="text-danger"><strong>Sorry! No purchase order found.</strong></p>
            @endif


        </div>

        <div class="white-box mb-20">
            <h3 class="box-title text-success">LIST OF CHALAN OF THIS PURCHASE ORDER</h3>
            <hr>
            <table class="table table-bordered table-striped" id="chalanListTable">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>CH No Auto</th>
                    <th>CH No Manual</th>
                    <th>Wirehouse</th>
                    <th>PO No Auto</th>
                    <th>PO No Manual</th>
                    <th>Chalan Date</th>
                    <th>Status</th>
                    <th><i class="fa-fw ti-eye"></i></th>
                </tr>
                </thead>

                <tbody>
                @foreach($purchase_order->chalans as $k => $chalan)
                    <tr>
                        <td>{{ $k+1 }}</td>
                        <td><a href="{{ route('get-chalan',['id' => $chalan->id]) }}">{{ $chalan->chalan_no }}</a></td>
                        <td>{{ $chalan->chalan_no_manual }}</td>
                        <td>{{ $chalan->wireHouse->name }}</td>
                        <td>{{ $chalan->purchaseOrder->po_no_auto }}</td>
                        <td>{{ $chalan->purchaseOrder->po_no_namual }}</td>
                        <td>{{ $chalan->chalan_date }}</td>
                        <td>
                            @if(!$chalan->status)
                                <a href="{{ route('approve-chalan',['id' => $chalan->id]) }}" class="btn btn-warning btn-sm flat">Approve</a>
                            @else
                                <span class="badge badge-success">Approved</span>
                            @endif
                        </td>
                        <td>
                            <a href="{{ route('get-chalan',['id' => $chalan->id]) }}" class="btn btn-primary btn-xs" data-toggle="tooltip" data-original-title="view"><i class="ti-eye fa-fw"></i>DETAILS</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>

            </table>
        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#purchaseOrderList").dataTable();
            $("#chalanListTable").dataTable();
        });

    </script>
@endsection
