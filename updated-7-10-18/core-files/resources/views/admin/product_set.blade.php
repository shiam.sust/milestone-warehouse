@extends('layouts.admin')

@section('content')
    <div class="white-box">
        <div class="col-mod-12">
            <div class="col-mod-6 col-lg-6">
                <h3 class="box-title text-success m-b-0">Product Set</h3>
                <p class="text-muted m-b-30">List of all Product Set</p>
            </div>
            <div class="col-mod-6 col-lg-6 ">
                <a href="{{ route('add_product_set') }}" class="waves-effect pull-right"><button class="btn btn-xs btn-info pull-right"><i class="fa fa-plus-circle"></i> ADD NEW PRODUCT SET</button></a>
            </div>
        </div>
        <div class="clear"></div><hr/>
        <div class="table-responsive col-mod-12">

            <table id="myTable" class="table table-bordered dataTable no-footer">
                <thead>
                <tr>
                    <th >SL</th>
                    <th >Product Set Name </th>
                    <th >Class </th>
                    <th >Product Details</th>

                    <th >Status</th>

                    <th>Action</th>

                </tr>
                </thead>
                <tbody>
                @foreach($details as $k => $detail)
                
                @if($detail->status == 0)
                    @continue
                @endif
                    <tr>
                        <td >{{ $k+1 }}</td>
                        <td >{{ $detail->name }}</td>
                        <td >{{ $detail->academicClass->name }} [{{ $detail->academicClass->classVersion() }}]</td>
                        <td>

                            @foreach($detail->productSetDetails as $k => $product_set_detail)
                                @if($k==0)
                                    <ul>
                                @endif
                                    <li>{{ $product_set_detail->product->name  }} [ {{ $product_set_detail->quantity }} {{ $product_set_detail->unit ? $product_set_detail->unit->name : '' }} ]</li>
                                @if($k+1 == count($detail->ProductSetDetails))
                                    </ul>
                                @endif
                            @endforeach
                        </td>

                        <td >{{ $detail->status ? 'Active' : 'Inactive' }}</td>

                        <td >
                            
                            <a href="{{ route('edit_product_set',['id' => $detail->id]) }}" class="btn btn-warning btn-xs" data-toggle="tooltip" title="Edit" ><i class="fa fa-edit"></i> UPDATE
                            </a>

                            <a href="delete_product_set/{{ $detail->id }}" type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Delete"><i class="fa fa-times-circle"></i> DELETE
                            </a>

                        </td>
                    </tr>


                @endforeach

                </tbody>
            </table>


        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('#myTable').DataTable();
            $(document).ready(function() {
                var table = $('#example').DataTable({
                    "columnDefs": [{
                        "visible": false,
                        "targets": 2
                    }],
                    "order": [
                        [2, 'asc']
                    ],
                    "displayLength": 25,
                    "drawCallback": function(settings) {
                        var api = this.api();
                        var rows = api.rows({
                            page: 'current'
                        }).nodes();
                        var last = null;
                        api.column(2, {
                            page: 'current'
                        }).data().each(function(group, i) {
                            if (last !== group) {
                                $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                                last = group;
                            }
                        });
                    }
                });
                // Order by the grouping
                $('#example tbody').on('click', 'tr.group', function() {
                    var currentOrder = table.order()[0];
                    if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                        table.order([2, 'desc']).draw();
                    } else {
                        table.order([2, 'asc']).draw();
                    }
                });
            });
        });
        $('#example23').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });

        $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip(); 
    });

    </script>
@endsection