@extends('layouts.admin')

@section('content')
<div class="white-box">
    <div class="col-mod-12">
        <div class="col-mod-6 col-lg-6">
                <h3 class="box-title text-success m-b-0">Product </h3>
                <p class="text-muted m-b-30">Create New Product</p>
        </div>        
        <div class="col-mod-6 col-lg-6 ">
            <a href="{{ route('product') }}" class="waves-effect pull-right"><button class="btn btn-xs btn-info "><i class="fa fa-arrow-circle-left"></i> ALL PRODUCT LIST</button></a>
        </div>    
    </div>  
    <div class="clear"></div><hr/>
    <div class="panel-body">

        <form action="{{ route('post.product') }}" method="post">
            {{ csrf_field() }}
            <div class="form-body">
                <h3 class="box-title">Product information</h3>
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Product Name<span class="text-danger m-1-5">*</span></label>
                            <input type="text" id="firstName" class="form-control" placeholder="John doe" name="name" required=""> 
                        </div>
                    </div> 
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Author/Puclication</label>
                            <input type="text" id="firstName" class="form-control" placeholder="Enter author or publication name" name="author">
                        </div>
                    </div>
                   
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Unit<span class="text-danger m-1-5">*</span></label>
                            <select class="form-control" data-placeholder="Choose a Category" tabindex="1" name="unit" required="">
                                <option value="">Select Product Unit</option>
                                @foreach($unit as $data)
                                <option value="{{ $data->id }}">{{ $data->name }}</option>
                                @endforeach
                              
                            </select>
                        </div>
                    </div> 
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Product type</label>
                            <select class="form-control" data-placeholder="Choose a Category" tabindex="1" name="type">
                                <option value="">Select Product Type</option>

                                @foreach($ptype as $data)
                                <option value="{{ $data->id }}">{{ $data->name }}</option>
                                @endforeach
                                
                              
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Selling Price</label>
                            <input type="text" id="firstName" class="form-control" placeholder="Enter selling price" name="selling_price">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Cost Price</label>
                            <input type="text" id="firstName" class="form-control" placeholder="Enter cost price" name="cost_price">
                        </div>
                    </div>
                                   
                </div>       
        
                <div class="row">
                   <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Status<span class="text-danger m-1-5">*</span></label>
                            <div class="radio-list">
                                <label class="radio-inline p-0">
                                    <div class="radio radio-info">
                                        <input type="radio" name="status" id="radio1" value=1 checked>
                                        <label for="radio1">active</label>
                                    </div>
                                </label>
                                <label class="radio-inline">
                                    <div class="radio radio-info">
                                        <input type="radio" name="status" id="radio2" value=0>
                                        <label for="radio2">inactive </label>
                                    </div>
                                </label>
                            </div>
                        </div>
                    </div>
                     
                </div>

            <div class="form-actions">
                <button type="submit" class="btn btn-success pull-right"> <i class="fa fa-check"></i> SAVE PRODUCT INFORMATION</button>
            </div>
        </div>
        </div>
    </form>
    </div>
</div>    
</div>
@endsection