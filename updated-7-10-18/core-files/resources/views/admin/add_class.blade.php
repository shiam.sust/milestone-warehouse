@extends('layouts.admin')

@section('content')
<div class="white-box">
    <div class="col-mod-12">
        <div class="col-mod-6 col-lg-6">
                <h3 class="box-title text-success m-b-0">Class</h3>
                <p class="text-muted m-b-30">Create New Class</p>
        </div>        
        <div class="col-mod-6 col-lg-6 ">
            <a href="{{ route('class') }}" class="waves-effect pull-right"><button class="btn btn-xs btn-info "><i class="fa fa-arrow-circle-left"></i> ALL CLASS LIST</button></a>
        </div>    
    </div>  
    <div class="clear"></div><hr/>
    <div class="panel-body">
                    <form action="{{ route('post.class') }}" method="post">
                        {{ csrf_field() }}

                        <div class="form-body">
                             
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Class Name<span class="text-danger m-1-5">*</span></label>
                                        <input type="text" id="firstName" class="form-control" placeholder="Class one" name="name" required="">
                                    </div>
                                </div> 

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Version<span class="text-danger m-1-5">*</span></label>
                                        <select class="form-control" data-placeholder="Choose a Category" tabindex="1" name="version" required="">
                                            <option >Select Version</option>                                          
                                            <option value="1">Bangla Medium Junior Section (BMJS)</option>  
                                            <option value="2">Bangla Medium Senior Section (BMSS)</option>  
                                            <option value="3">English Medium Junior Section (EMJS)</option>  
                                            <option value="4">English Medium Senior Section (EMSS)</option>  
                                        </select>
                                    </div>
                                </div> 
                           
                                    <div class="form-group">
                                        <label class="control-label">Status</label>
                                        <div class="radio-list">
                                            <label class="radio-inline p-0">
                                                <div class="radio radio-info">
                                                    <input type="radio" id="radio1" value=1 name="status" required="" checked="">
                                                    <label for="radio1">active</label>
                                                </div>
                                            </label>
                                            <label class="radio-inline">
                                                <div class="radio radio-info">
                                                    <input type="radio" id="radio2" value=0 name="status">
                                                    <label for="radio2">inactive </label>
                                                </div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                 
                            </div>
                                             
                            <div class="form-actions">
                                <button type="submit" class="btn btn-success pull-right"> <i class="fa fa-check"></i> SAVE CLASS INFO</button>
                                 
                            </div>
                        </form>
    </div>
</div>    
</div>
@endsection