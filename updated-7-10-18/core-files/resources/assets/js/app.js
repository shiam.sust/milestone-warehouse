
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

window.Vue = require('vue');
window.axios = require('axios');
window.axios.defaults.baseURL = document.head.querySelector('meta[name="base-url"]').content;
var Vue = require('vue');
var VueScrollTo = require('vue-scrollto');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('create-purchase-order', require('./components/CreatePurchaseOrder.vue'));
Vue.component('create-chalan', require('./components/CreateChalan.vue'));
Vue.component('create-pr-chalan', require('./components/CreatePrChalan.vue'));
Vue.component('create-return-chalan', require('./components/CreateReturnChalan.vue'));
Vue.component('chalan-items', require('./components/ChalanItems.vue'));
Vue.component('edit-purchase', require('./components/EditPurchase.vue'));
Vue.component('create-central-chalan', require('./components/CreateCentralChalan.vue'));
Vue.component('return-purchase-order-item', require('./components/ReturnPurchaseOrderItem.vue'));
Vue.component('create-product-set', require('./components/CreateProductSet.vue'));
Vue.component('create-initial-item', require('./components/CreateInitialItem.vue'));
Vue.component('edit-product-set', require('./components/EditProductSet.vue'));
Vue.component('student-list', require('./components/ShowClasses.vue'));
Vue.component('my-component', require('./components/MyComponent.vue'));
Vue.component('create-product-requisition', require('./components/CreateProductRequisition.vue'));
Vue.component('edit-product-requisition', require('./components/EditProductRequisition.vue'));

const app = new Vue({
    el: '#page-wrapper'
});

Vue.use(VueScrollTo, {
     container: "body",
     duration: 500,
     easing: "ease",
     offset: 0,
     cancelable: true,
     onStart: false,
     onDone: false,
     onCancel: false,
     x: false,
     y: true
 });

var purchaseOrder = document.getElementById("select-purchase-order");
var chalan = document.getElementById("chalan-id");
var productRequisition = document.getElementById("select-product-requisition");
var po = document.getElementById("select-purchase-order");

if(purchaseOrder){
    var po_id = purchaseOrder.value;
    if(app.$refs.getPurchaseOrderItem && po_id){
        app.$refs.getPurchaseOrderItem.loadItems(po_id);
    }
    purchaseOrder.onchange = function () {
        var po_id = this.value;
        if(app.$refs.getPurchaseOrderItem){
            app.$refs.getPurchaseOrderItem.loadItems(po_id);
        }
    }
}

if(chalan){

    var ch_id = chalan.value;
    if(app.$refs.getchalanItems && ch_id){
        app.$refs.getchalanItems.loadItems(ch_id);
    }

    chalan.onchange = function () {
        var ch_id = this.value;
        if(app.$refs.getchalanItems){
            app.$refs.getchalanItems.loadItems(ch_id);
        }
    }
}
if(productRequisition){

    var p_req_id = productRequisition.value;

    if(app.$refs.getProductRequisitionItem){
        app.$refs.getProductRequisitionItem.loadItems(p_req_id);
    }

    productRequisition.onchange = function () {
        var p_req_id = this.value;
        if(app.$refs.getProductRequisitionItem){
            app.$refs.getProductRequisitionItem.loadItems(p_req_id);
        }
    }
}

