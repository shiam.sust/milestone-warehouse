<?php


Route::group(['prefix' => 'wirehouse','middleware' => ['auth','manager.admin']],function(){
   Route::name('wirehouse.')->group(function (){
       /**
        * Wirehouse
        */

       Route::get('/',[
           'uses'  => 'WirehouseController@index',
           'as'     => 'wirehouse'
       ]);
       /**

       /**
        * Purchase Orders
        */
       Route::get('purchase-orders',[
           'uses'  => 'PurchaseOrderController@index',
           'as'     => 'purchase-orders'
       ]);

       Route::get('purchase-order/{id}',[
           'uses'  => 'PurchaseOrderController@getPurchaseOrder',
           'as'     => 'purchase-order'
       ]);

       Route::get('purchase-order/items/{id}',[
           'uses'  => 'PurchaseOrderController@getPurchaseOrderItems',
           'as'     => 'purchase-order-items'
       ]);


       /**
        * Products
        */
       Route::get('products',[
           'uses'  => 'ProductController@index',
           'as'     => 'products'
       ]);

       /**
        * Product Sets
        */
       Route::get('product-sets',[
           'uses'  => 'ProductSetController@index',
           'as'     => 'product-sets'
       ]);

       /**
        * Product Demands
        */
       Route::get('product-demands',[
           'uses'  => 'ProductDemandController@index',
           'as'     => 'product-demands'
       ]);

       Route:: get('add-demand', [
          'uses' => 'ProductDemandController@showCreate',
          'as' => 'add-demand'
       ]);

       Route:: get('edit-demand/{id}', [
           'uses' => 'ProductDemandController@edit',
           'as' => 'edit-demand'
       ]);


       Route:: post('product-demand', [
          'uses' => 'ProductDemandController@store',
          'as' => 'product-demand'
       ]);

       Route:: get('show/product-demand/{id}', [
           'uses' => 'ProductDemandController@show',
           'as' => 'show-product-demand'
       ]);

       Route:: post('update-product-demand/{id}', [
           'uses' => 'ProductDemandController@update',
           'as' => 'update-product-demand'
       ]);

        Route:: get('item/{id?}', [
            'uses'  => 'ProductDemandController@getItems',

        ])->name('item');

        Route::get('receive-product-from-requisition/{id}',[
            'uses'  => 'ProductDemandController@receive',
            'as'    => 'receive-from-requisition'
        ]);

       Route::get('return-requisition/{id}',[
           'uses' => 'ProductRequisitionController@getReturn',
           'as'    => 'return-requisition'

       ]);

       Route::post('return-requisition/{id}',[
           'uses' => 'ProductRequisitionController@giveReturn',
           'as'    => 'update-return-requisition'

       ]);

       /**
        * Product Return
        */

       Route::get('product-returns',[
           'uses'   => 'ProductReturnController@index',
           'as'     => 'product-returns'
       ]);

       Route::get('create-product-return',[
           'uses'   => 'ProductReturnController@create',
           'as'     => 'create-product-return'
       ]);

       Route::post('store-product-return',[
           'uses'   => 'ProductReturnController@store',
           'as'     => 'store-product-return'
       ]);

       Route:: get('show/product-return/{id}', [
           'uses' => 'ProductReturnController@show',
           'as' => 'show-product-return'
       ]);
       Route:: get('edit/product-return/{id}', [
           'uses' => 'ProductReturnController@edit',
           'as' => 'edit-product-return'
       ]);

       Route:: post('edit/product-return/{id}', [
           'uses' => 'ProductReturnController@update',
           'as' => 'update-product-return'
       ]);

      /**
       *Student_Strength
       */

       Route:: get('student_strengths', [
          'uses' => 'StudentController@index' 
        ])->name('student_strengths');

       Route:: get('add_students', [
          'uses' => 'StudentController@create' 
        ])->name('add_students');

        Route::get('get-student-strength/{year?}',[
            'uses'  => 'StudentController@getStrength',
            'as'     => 'get-student-strength'
        ]);

        Route:: post('save-students',[
            'uses' => 'StudentController@store'
        ])->name('save-students');

        Route:: post('update-students/{id?}',[
            'uses' => 'StudentController@update'
        ])->name('update-students');

        Route::get('get_all_student', [
            'uses' => 'StudentController@index'
        ])->name('get_all_student');

        Route::get('show_new_strength', [
            'uses' => 'StudentController@getNewStrength'
        ])->name('show_new_strength');
        Route:: get('delete_item/{id?}',[
          'uses' => 'StudentController@delete'
        ])->name('delete_item');



       /**
        * Chalans
        */
       Route::get('chalans',[
           'uses'  => 'ChalanController@index',
           'as'     => 'chalans'
       ]);

       Route::get('create-chalan',[
           'uses'  => 'ChalanController@create',
           'as'     => 'create-chalan'
       ]);

       Route::post('create-chalan',[
           'uses'  => 'ChalanController@store',
           'as'     => 'store-chalan'
       ]);

       Route::get('get-chalan/{id}',[
           'uses'  => 'ChalanController@getChalan',
           'as'     => 'get-chalan'
       ]);

       /**
        * Chalans
        */
       Route::get('requisition-chalans',[
           'uses'  => 'RequisitionChalanController@index',
           'as'     => 'requisition-chalans'
       ]);

       Route::get('requisition-create-chalan',[
           'uses'  => 'RequisitionChalanController@create',
           'as'     => 'requisition-create-chalan'
       ]);

       Route::post('requisition-create-chalan',[
           'uses'  => 'RequisitionChalanController@store',
           'as'     => 'requisition-store-chalan'
       ]);

       Route::get('requisition-get-chalan/{id}',[
           'uses'  => 'RequisitionChalanController@getChalan',
           'as'     => 'requisition-get-chalan'
       ]);

       Route::get('product-order/items/{id}',[
           'uses'  => 'RequisitionChalanController@getProductRequisitionItems',
           'as'     => 'product-order-items'
       ]);

       /**
        * Return Chalans
        */
       Route::get('return-chalans',[
           'uses'  => 'ReturnChalanController@index',
           'as'     => 'return-chalans'
       ]);

       Route::get('return-create-chalan',[
           'uses'  => 'ReturnChalanController@create',
           'as'     => 'return-create-chalan'
       ]);

       Route::post('return-create-chalan',[
           'uses'  => 'ReturnChalanController@store',
           'as'     => 'return-store-chalan'
       ]);

       Route::get('return-get-chalan/{id}',[
           'uses'  => 'ReturnChalanController@getChalan',
           'as'     => 'return-get-chalan'
       ]);

       Route::get('product-return/items/{id}',[
           'uses'  => 'ReturnChalanController@getProductReturnItems',
           'as'     => 'product-return-items'
       ]);

       /**
        * Profile
        */

       Route::get('profile',[
           'uses'   => 'UserController@managerProfile',
           'as'     => 'my-profile'
       ]);

       Route::post('change-password',[
           'uses'   => 'UserController@changePassword',
           'as'     => 'change-password'
       ]);


   });
});
