<?php

namespace App\Http\Controllers\Product;

use App\Repository\ManufacturerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManufacturerController extends Controller
{
    private $manufacturer;

    public function __construct(ManufacturerRepository $manufacturerRepository)
    {
        $this->manufacturer = $manufacturerRepository;
    }

    public function index(){
        $manufacturers = $this->manufacturer->all();
        return view('admin.manufacturers')->with([
            'manufacturers'  => $manufacturers
        ]);
    }

    public function manufacturer($id){
        $manufacturer = $this->manufacturer->getById($id);
        return response()->json($manufacturer,200);
    }

    public function create(Request $request){
        $this->manufacturer->create($request->only(['name']));
        $message = [
            'status' => true,
            'text' => 'Successfully created this information'
        ];

        return redirect()->back()->with('message', $message);
    }

    public function update(Request $request,$id){
        $this->manufacturer->getById($id)->update($request->only(['name']));
        $message = [
            'status' => true,
            'text' => 'Successfully updated this information'
        ];

        return redirect()->back()->with('message', $message);

    }

    public function delete($id){
        $this->manufacturer->deleteById($id);
        $message = [
            'status' => true,
            'text' => 'Successfully deleted this information'
        ];

        return redirect()->back()->with('message', $message);
    }
}
