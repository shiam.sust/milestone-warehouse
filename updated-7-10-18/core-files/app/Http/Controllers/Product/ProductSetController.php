<?php

namespace App\Http\Controllers\Product;
use App\Model\Product\Product;
use App\Model\Product\ProductSet;
use App\Model\Product\ProductSetDetails;
use App\Model\Product\ProductType;
use App\Model\Product\Unit;
use App\Model\Classes\AcademicClass;

use App\Model\WireHouse\WireHouse;
use App\Model\WireHouse\WireHouseProductSet;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;

class ProductSetController extends Controller
{
    public function index(){
        $details = ProductSet::with(['productSetDetails.product'])->orderBy('id','desc')->get();
        
        return view('admin.product_set')->with([
            'details' => $details
        ]);
    }

    public function stock(){
        $product_sets = WireHouseProductSet::orderBy('id','desc')->get();
        return view('admin.stock.product-sets')->with([
            'product_sets'  => $product_sets
        ]);
    }

    public function createInitialQuantity(){
        $product_sets = ProductSet::all();
        $units = Unit::all();
        $warehouses = WireHouse::all();
        return view('admin.stock.create-product-sets')->with([
            'product_sets'  => $product_sets,
            'units'     => $units,
            'warehouses' => $warehouses
        ]);
    }

    public function storeInitialQuantity(Request $request){
        $wh_id = $request->input('wh_id');

        foreach ($request->input('product') as $product){
            $product_set = WireHouseProductSet::firstOrCreate(
                ['wh_id' => $wh_id,'product_set_id' => $product['id']]
            );

            $product_set->update([
                'stock_balance' => $product_set->stock_balance - $product_set->initial_quantity + $product['quantity'],
                'initial_quantity' => $product['quantity']
            ]);
        }

        return redirect()->route('balance-product-set')->withMessage([
            'status'    => 'alert-success',
            'text'      => 'Successfully created initial quantity'
        ]);
    }

    public function updateInitialQuantity(Request $request,$id){
        $product_set = WireHouseProductSet::findOrFail($id);
        $product_set->update([
            'stock_balance' => $product_set->stock_balance - $product_set->initial_quantity + $request->input('quantity'),
            'initial_quantity' => $request->input('quantity')
        ]);
        return redirect()->back()->withMessage([
            'status'    => 'alert-success',
            'text'      => 'Successfully updated initial quantity'
        ]);
    }

    public function showForm(){
        $class = AcademicClass::where('status','>',0)->get();
        $products = Product::all();
        $units = Unit::all();

        //return $ptypes;
                
        return view('admin.add_product_set')->with([
            'class' => $class,
            'products' => $products,
            'units' => $units
        ]);
    }

    public function store(Request $request){

        $product_set = ProductSet::create([
            'name'        => $request->input('name'),
            'status'      => $request->input('status'),
            'class_id'    => $request->input('academic_class')
        ]);

        $set_data = [];
        foreach ($request->input('product') as $product){
            $set_data[$product['id']] = [
                'unit_id'   => $product['unit'],
                'quantity'  => $product['quantity']
            ];
        }


        $product_set->setProducts()->sync($set_data);

        return redirect()->route('product_set')->withMessage([
            'status'    => 'alert-success',
            'text'  => 'Product set created successfully'
        ]);
    }

    public function edit($id){
        $product_set = ProductSet::findOrFail($id);
        $units = Unit::all();
        $class = AcademicClass::all();
        $book_ids = count($product_set->productSetDetails) ? $product_set->productSetDetails->pluck('product_id') : [];
        $rem_products = Product::whereNotIn('id',$book_ids)->get();
        $cur_products = Product::whereIn('id',$book_ids)->get();
        $set_items = [];

        foreach ($cur_products as $p){
            $set_item = $p;
            $set_p = ProductSetDetails::where('product_set_id',$product_set->id)->where('product_id',$p->id)->first();
            $set_item['unitId'] = $set_p->unit_id;
            $set_item['quantity'] = $set_p->quantity;
            $set_items[] = $set_item;
        }

        return view('admin.product-set-edit')->with([
            'product_set'       => $product_set,
            'class'             => $class,
            'products'          => json_encode([
                'cur_products'  => $set_items,
                'rem_products'  => $rem_products,
                'units'         => $units
            ],200)
        ]);
    }

    public function update(Request $request,$id){
        $product_set = ProductSet::findOrFail($id);
        $product_set->update([
            'name'  => $request->input('name'),
            'status'    => $request->input('status'),
            'class_id' => $request->input('academic_class')
        ]);

        $set_data = [];
        foreach ($request->input('product') as $product){
            $set_data[$product['id']] = [
                'unit_id'   => $product['unit'],
                'quantity'  => $product['quantity']
            ];
        }


        $product_set->setProducts()->sync($set_data);

        return redirect()->route('product_set')->withMessage([
            'status'    => 'alert-success',
            'text'  => 'Product set updated successfully'
        ]);
    }

    public function delete($id){
        
        DB::table('product_sets')
                    ->where('id',$id)
                    ->update(['status' => 0]);

        return redirect()->route('product_set');
        
    }
}
