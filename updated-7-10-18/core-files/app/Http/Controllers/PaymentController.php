<?php

namespace App\Http\Controllers;

use App\Model\Chalan;
use App\Model\Payment;
use App\PaymentMethod;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function index(){
        $payments = Payment::orderBy('id','desc')->get();
        return view('admin.payment.index')->with([
            'payments'  => $payments
        ]);
    }
    public function create(Request $request){
        $chalans = Chalan::where('status','<=',2)->orderBy('id','desc')->get();
        $payment_methods = PaymentMethod::orderBy('id','desc')->get();
        return view('admin.payment.create')->with([
            'chalans'   => $chalans,
            'payment_methods'   => $payment_methods,
            'ch_id'     => $request->id
        ]);
    }
    public function store(Request $request){
        $payment = Payment::create($request->only(['chalan_id','payment_method_id','amount','discount','payment_date']));
        $chalan = Chalan::findOrFail($request->input('chalan_id'));
        if($request->file('attachment')){
            $name = time().'.'.$request->file('attachment')->getClientOriginalExtension();
            $request->file('attachment')->move(public_path('attachments'),$name);
            $name = 'attachments/'.$name;
            $payment->fill(['attachment' => $name])->save();
        }

        $this->checkPaymentStatus($chalan);
        return redirect()->route('payments')->withMessage([
            'status'    => 'alert-success',
            'text'      => 'Payment created.'
        ]);
    }

    public function getChalanItem($id){
        $chalan = Chalan::findOrFail($id);
        $items = [];
        foreach ($chalan->chalanDetails as $chalanDetail){
            $items[] = [
                'title' => $chalanDetail->item->name,
                'unit'  => $chalanDetail->purchaseOrderDetail->item_type == 1 ? $chalanDetail->purchaseOrderDetail->unit->name : 'SET',
                'price'  => $chalanDetail->purchaseOrderDetail->unit_price,
                'quantity'  => $chalanDetail->received_qty,
                'sub_total' => $chalanDetail->received_qty * $chalanDetail->purchaseOrderDetail->unit_price
            ];
        }

        $total_payment = $chalan->payments ? $chalan->payments->sum('amount') : 0;
        $payments = $chalan->payments;

        return response()->json(['items' => $items,'paid' => $total_payment,'payments' => $payments],200);

    }

    private function checkPaymentStatus(Chalan $chalan){
            $product_amount = $chalan->chalanDetails ? $chalan->chalanDetails->sum('product_amount') : 0;
            $total_payment = $chalan->payments ? $chalan->payments->sum('amount') : 0;
            $total_discount = $chalan->payments ? $chalan->payments->sum('discount') : 0;

            $due = $product_amount - ($total_payment + $total_discount);

            if($due == 0){
                $chalan->update([
                    'status'    => '3'
                ]);
            }elseif (count($chalan->payments)){
                $chalan->update([
                    'status'    => '2'
                ]);
            }
    }
}
