<?php

namespace App\Http\Controllers\ManagerAdmin;

use App\Model\Product\Product;
use App\Model\Product\ProductSet;
use App\Model\Product\Unit;
use App\Model\ProductReturn\ProductReturn;
use App\Model\ProductReturn\ProductReturnDetail;
use App\Model\WireHouse\WireHouse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class ProductReturnController extends Controller
{
    public function index(){
        $wh_id = Auth::user()->wh_id;
        $return_products = ProductReturn::where('from_warehouse',$wh_id)->orderBy('id','desc')->get();
        return view('wirehouse-manager.product-return.index')->with([
            'return_products'  => $return_products
        ]);
    }

    public function show ($id){
        $product_return = ProductReturn::findOrFail($id);
        return view('wirehouse-manager.product-return.show')->with([
            'product_return'    => $product_return
        ]);
    }

    public function create(){
        $wh = WireHouse::all();
        $unit = Unit::all();
        $product = Product::all();
        $product_sets = ProductSet::all();
        $date = Carbon::today();
        $pr = count(ProductReturn::all()) + 1;
        $ret_no = 'Ret-'.$date->year.'-' . $date->month.'-'. $date->day. '-' .str_pad($pr,4,'0',STR_PAD_LEFT);

        return view('wirehouse-manager.product-return.create')->with([
            'wh' => $wh,
            'unit' => $unit,
            'product' => $product,
            'ret_no'    => $ret_no,
            'product_sets'  => $product_sets
        ]);
    }

    public function store(Request $request){
        $product_return  = ProductReturn::create([
            'return_date'  => $request->input('return_date'),
            'ret_no_auto'       => $request->input('ret_no_auto'),
            'ret_no_manual'     => $request->input('ret_no_manual'),
            'from_warehouse'    => Auth::user()->wh_id,
            'to_warehouse'      => $request->input('towirehouse'),
            'created_by'        => Auth::user()->id
        ]);

        foreach ($request->input('product') as $product){
            ProductReturnDetail::create([
                'product_ret_id'    => $product_return->id,
                'item_id'           => $product['id'],
                'item_type'         => $product['type'],
                'unit_id'           => $product['unit'],
                'quantity'          => $product['quantity']
            ]);
        }

        return redirect()->route('wirehouse.product-returns')->with([
            'message' => [
                'status'    => 'alert-success',
                'text'      => 'Successfully created this return.'
            ]
        ]);
    }

    public function edit($id){
        $wh_id = Auth::user()->wh_id;
        $product_return = ProductReturn::where('id',$id)->where('from_warehouse',$wh_id)->first();
        if(!$product_return){
            return redirect()->back();
        }


        $product_ids = ProductReturnDetail::where('product_ret_id',$product_return->id)->where('item_type',1)->get()->pluck('item_id');

        $product_set_ids = ProductReturnDetail::where('product_ret_id',$product_return->id)->where('item_type',2)->get()->pluck('item_id');

        $warehouses = WireHouse::orderBy('name','asc')->get();
        $products = Product::whereNotIn('id',$product_ids)->get();
        $product_sets = ProductSet::whereNotIn('id',$product_set_ids)->get();
        $units = Unit::orderBy('name','asc')->get();

        $data = [];
        foreach ($product_return->productReturnDetails as $rd){
            $item = $rd->item;
            $item->unitId = $rd->unit_id;
            $item->quantity = $rd->quantity;
            $data[] = $item;
        }




        return view('wirehouse-manager.product-return.edit-return')->with([
            'product_return'    => $product_return,
            'wh'        => $warehouses,
            'warehouses'        => $warehouses,
            'units'              => $units,
            'products'          => $products,
            'product_sets'       => $product_sets,
            'demand_products'  => json_encode($data)
        ]);
    }

    public function update(Request $request,$id){
        $demand = ProductReturn::findOrFail($id);
        $demand->update([
            'return_date'  => $request->input('return_date'),
            'ret_no_auto'       => $request->input('ret_no_auto'),
            'ret_no_manual'     => $request->input('ret_no_manual'),
            'from_warehouse'    => Auth::user()->wh_id,
            'to_warehouse'      => $request->input('towirehouse'),
            'created_by'        => Auth::user()->id
        ]);

        ProductReturnDetail::where('product_ret_id',$demand->id)->delete();
        foreach ($request->input('product') as $product){
            ProductReturnDetail::create([
                'product_ret_id'    => $demand->id,
                'item_id'           => $product['id'],
                'item_type'         => $product['type'],
                'unit_id'           => $product['unit'],
                'quantity'          => $product['quantity']
            ]);
        }

        return redirect()->route('wirehouse.product-returns')->with([
            'message' => [
                'status'    => 'alert-success',
                'text'      => 'Successfully created this return.'
            ]
        ]);
    }

}
