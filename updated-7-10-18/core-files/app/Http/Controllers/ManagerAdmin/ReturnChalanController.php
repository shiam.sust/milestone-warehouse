<?php

namespace App\Http\Controllers\ManagerAdmin;

use App\Model\ProductReturn\ProductReturn;
use App\Model\ProductReturn\ProductReturnChalan;
use App\Model\ProductReturn\ProductReturnChalanDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class ReturnChalanController extends Controller
{
    public function index(){
        $wh_id = Auth::user()->wh_id;
        $chalans = ProductReturnChalan::where('wh_id',$wh_id)->orderBy('id','desc')->get();
        return view('wirehouse-manager.product-return-chalan.index')->with([
            'chalans'  => $chalans
        ]);
    }

    public function getChalan($id){
        $chalan = ProductReturnChalan::findOrFail($id);
        return view('wirehouse-manager.product-return-chalan.details')->with([
            'chalan'  => $chalan
        ]);
    }
    public function create(){
        $wh_id = Auth::user()->wh_id;
        $date = Carbon::today();
        $pd = count(ProductReturnChalan::all()) + 1;
        $ch_no = 'RetCH-'.$date->year.'-' . $date->month.'-'. $date->day. '-' .str_pad($pd,4,'0',STR_PAD_LEFT);
        $product_returns = ProductReturn::where('from_warehouse',$wh_id)->where('status',1)->orderBy('id','desc')->get();
        return view('wirehouse-manager.product-return-chalan.create')->with([
            'product_returns'   => $product_returns,
            'ch_auto' => $ch_no
        ]);
    }

    public function store(Request $request){
        $wh_id = Auth::user()->wh_id;
        $data = $request->only(['product_return_id','ch_no_auto','ch_no_manual','ch_date']);
        $data['wh_id'] = $wh_id;
        $req_chalan = ProductReturnChalan::create($data);
        foreach ($request->input('chalan_details') as $chalan_detail){
            if(!isset($chalan_detail['quantity']) || $chalan_detail['quantity'] <= 0){
                continue;
            }
            ProductReturnChalanDetail::create([
                'ret_chalan_id' => $req_chalan->id,
                'item_id'   => $chalan_detail['item_id'],
                'item_type' => $chalan_detail['item_type'],
                'unit_id'   => $chalan_detail['unit_id'],
                'quantity'  => $chalan_detail['quantity'],
                'p_ret_d_id'=> $chalan_detail['p_ret_d_id'],
            ]);
        }

        return redirect()->route('wirehouse.return-chalans')->withMessage([
            'status'    => 'alert alert-success',
            'text'      => 'Successfully created returned product chalan'
        ]);
    }

    public function getProductReturnItems($id){
        $product_return  = ProductReturn::findOrFail($id);
        $returnable_data = [];

        foreach ($product_return->productReturnDetails as $returnDetail){
            $total_received = $returnDetail->chalanDetails ? $returnDetail->chalanDetails->sum('quantity') : 0;
            $rem_qty = $returnDetail->quantity - $total_received;

            $requisition_data[] = [
                'title' => $returnDetail->item->name,
                'item_id'   => $returnDetail->item->id,
                'total_qty' => $returnDetail->quantity,
                'rec_qty' => $total_received,
                'rem_qty' => $rem_qty,
                'quantity' => "",
                'item_type'  => $returnDetail->item_type,
                'p_ret_d_id'   => $returnDetail->id,
                'unit'  => $returnDetail->item->base_unit ? $returnDetail->item->unit->name : 'SET',
                'unit_id'   => $returnDetail->item->base_unit ? $returnDetail->item->unit->id : null
            ];

        }

        return [
            'items' => $requisition_data,
        ];
    }
}
