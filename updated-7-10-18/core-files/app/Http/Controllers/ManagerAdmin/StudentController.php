<?php

namespace App\Http\Controllers\ManagerAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Student;
use App\Model\Classes\AcademicClass;
use Auth;
use DB;

class StudentController extends Controller
{
    public function index(){
        $students = Student::all();
    	return response()->json(['students' => $students,200]);
    }

    public function create(){
        $students = Student::with('studentClass')->where('status','!=',-1)->get();
        $academicClasses = AcademicClass::all();
        $stu = [
            'stu' => $students,
            'our_class' => $academicClasses
        ];

        //return $stu;
    	return view('wirehouse-manager.student_strength.show')->with([
            'mystudents' => json_encode($stu)
        ]);
    }

    public function store(Request $request){

        $this->validate($request, array(
            'year' => 'required',
            'classId' => 'required',
            'numberOfStudent' => 'required'          
        ));

        //creating model object and setting our data to database column

        $data = [
            'wh_id' =>  Auth::user()->wh_id,
            'class_id'  => $request->classId,
            'year'  => $request->year
        ];

        $found = Student::where($data)->count();

        if(!$found){
            $data['quantity'] = $request->numberOfStudent;
            $stu = Student::create($data);
            $new_student = Student::with(['studentClass','wirehouse'])->where('id',$stu->id)->first();
            return response()->json([ 
                'student' => $new_student,
                'status'    => 'created',
                'message'   => 'Successfully created this information'
            ]);
        }

        return response()->json([
            'status'    => 'exist',
            'message'   => 'This information already exist.'
        ]);
    }

    public function getStrength($year){
        $wh_id = Auth::user()->wh_id;
        $student_strengths = Student::with(['studentClass','wirehouse'])->where('year',$year)->where('wh_id',$wh_id)->orderBy('id','desc')->get();
        return response()->json(['students' => $student_strengths,200]);
    }

    public function getNewStrength(){
        
        $student_strengths = Student::with(['studentClass'])->orderBy('id','desc')->get();
        return response()->json(['students' => $student_strengths,200]);
    }

    public function update(Request $request, $id){
        //return $request->all();
        $this->validate($request, array(
            'year' => 'required',
            'classId' => 'required',
            'numberOfStudent' => 'required'          
        ));

        $wh_id = Auth::user()->wh_id;

        $student = Student::find($id);
        $data = [
            'wh_id' =>  Auth::user()->wh_id,
            'class_id'  => $request->classId,
            'year'  => $request->year
        ];
        $duplicate = Student::where('id','!=',$id)->where($data)->get();
        //return $duplicate;
        if(!count($duplicate)){
            $mydata = [
                'class_id' => $request->classId,
                'year' => $request->year,
                'quantity' => $request->numberOfStudent
            ];
            $student->update($mydata);

            $new_student = Student::with(['studentClass','wirehouse'])->where('id',$student->id)->first();

            return response()->json([
                'new_student' => $new_student, 
                'status'    => 'updated',
                'message'   => 'Successfully updated this information'
            ]);
        }else{
            return response()->json([
                 
                'status'    => 'failed',
                'message'   => 'Record not  found'
            ]);
        }
    }
    public function delete($id){
        
        DB::table('students')
                    ->where('id',$id)
                    ->update(['status' => -1]);
        return response()->json([
            'status'    => 'alert-success',
            'message'   => 'Deleted Succesfully!'
        ]);
        
    }
}
