<?php

namespace App\Http\Controllers\ManagerAdmin;

use App\ProductDemand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class ProductRequisitionController extends Controller
{
    public function getReturn($id){
        $wh_id = Auth::user()->wh_id;
        $product_demand = ProductDemand::where('from_warehouse',$wh_id)->where('id',$id)->first();
        return view('wirehouse-manager.product-demand.return')->with([
            'product_demand'    => $product_demand
        ]);
    }

    public function giveReturn(Request $request,$id){
        $wh_id = Auth::user()->wh_id;
        $product_demand = ProductDemand::where('from_warehouse',$wh_id)->where('id',$id)->first();

        $product_demand->update([
            'return_quantity'   => $request->quantity,
            'status'            => 4
        ]);

        return redirect()->route('wirehouse.product-demands')->withMessage([
            'status'    => 'alert-success',
            'text'      => 'Successfully created return.'
        ]);
    }
}
