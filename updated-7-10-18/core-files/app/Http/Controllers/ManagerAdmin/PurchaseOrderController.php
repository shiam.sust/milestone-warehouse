<?php

namespace App\Http\Controllers\ManagerAdmin;

use App\Model\PurchaseOrder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class PurchaseOrderController extends Controller
{
    public function index(){
        $purchase_orders = PurchaseOrder::where('wh_id',Auth::user()->wh_id)->orderBy('id','desc')->get();
        return view('wirehouse-manager.purchase-order.index')->with([
            'purchase_orders'   => $purchase_orders
        ]);
    }

    public function getPurchaseOrder($id){
        $purchase_orders = PurchaseOrder::where('wh_id',Auth::user()->wh_id)
            ->where('id',$id)->orderBy('id','desc')->get();


        return view('wirehouse-manager.purchase-order.details')->with([
            'purchase_order'   => count($purchase_orders) ? $purchase_orders->first() : null
        ]);

    }

    public function getPurchaseOrderItems($id){
        $purchase_order = PurchaseOrder::findOrFail($id);
        $purchase_order_data = [];

        foreach ($purchase_order->purchaseOrderDetails as $purchase_order_detail){
            $total_received = $purchase_order_detail->chalanDetails ? $purchase_order_detail->chalanDetails->sum('received_qty') : 0;
            $rem_qty = $purchase_order_detail->quantity - $total_received ;
            $rem_qty = $rem_qty - $purchase_order_detail->returned_quantity;

            $purchase_order_data[] = [
                'title' => $purchase_order_detail->item->name,
                'total_qty' => $purchase_order_detail->quantity,
                'unit_price' => $purchase_order_detail->unit_price,
                'rec_qty' => $total_received,
                'rem_qty' => $rem_qty,
                'quantity' => 0,
                'delivered_amount' => 0,
                'returned_qty'  =>  $purchase_order_detail->returned_quantity,
                'type'  => $purchase_order_detail->item_type,
                'sale_id'   => $purchase_order_detail->id,
                'unit'  => $purchase_order_detail->item->base_unit ? $purchase_order_detail->item->unit->name : 'SET',
            ];

        }

        return [
            'items' => $purchase_order_data,
        ];
    }
}
