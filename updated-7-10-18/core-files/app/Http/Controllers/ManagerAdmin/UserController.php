<?php

namespace App\Http\Controllers\ManagerAdmin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class UserController extends Controller
{
    public function managerProfile(){
        return view('wirehouse-manager.profile');
    }

    public function changePassword(Request $request){
        $this->validate($request,[
            'password' => 'required|string|min:6|confirmed'
        ]);

        $user = User::findOrFail(Auth::user()->id);

        $user->update([
            'password'  => bcrypt($request->input('password'))
        ]);

        return redirect()->back()->withMessage([
            'status'    => 'alert-success',
            'text'      => 'Password has been updated'
        ]);
    }
}
