<?php

namespace App\Http\Controllers\ManagerAdmin;

use App\Model\WireHouse\WireHouseProductSet;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class ProductSetController extends Controller
{
    public function index(){
        $wh_id = Auth::user()->wh_id;
        $product_sets = WireHouseProductSet::where('wh_id',$wh_id)->get();

        return view('wirehouse-manager.product-set.index')->with([
            'product_sets'  => $product_sets
        ]);
    }
}
