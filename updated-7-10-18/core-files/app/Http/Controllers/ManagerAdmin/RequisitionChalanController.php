<?php

namespace App\Http\Controllers\ManagerAdmin;

use App\ProductDemand;
use App\RequisitionChalan;
use App\RequisitionChalanDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class RequisitionChalanController extends Controller
{
    public function index(){
        $wh_id = Auth::user()->wh_id;
        $chalans = RequisitionChalan::where('wh_id',$wh_id)->orderBy('id','desc')->get();
        return view('wirehouse-manager.requisition-chalan.index')->with([
            'chalans'  => $chalans
        ]);
    }

    public function getChalan($id){
        $chalan = RequisitionChalan::findOrFail($id);
        return view('wirehouse-manager.requisition-chalan.details')->with([
            'chalan'  => $chalan
        ]);
    }

    public function create(){
        $wh_id = Auth::user()->wh_id;
        $date = Carbon::today();
        $pd = count(RequisitionChalan::all()) + 1;
        $ch_no = 'RCH-'.$date->year.'-' . $date->month.'-'. $date->day. '-' .str_pad($pd,4,'0',STR_PAD_LEFT);
        $demands = ProductDemand::where('from_warehouse',$wh_id)->where('status',1)->orderBy('id','desc')->get();
        return view('wirehouse-manager.requisition-chalan.create')->with([
            'demands'   => $demands,
            'ch_auto' => $ch_no
        ]);
    }



    public function store(Request $request){
        //return $request->all();
        $wh_id = Auth::user()->wh_id;
        $data = $request->only(['demand_id','ch_no_auto','ch_no_manual','ch_date']);
        $data['wh_id'] = $wh_id;
        $req_chalan = RequisitionChalan::create($data);
        foreach ($request->input('chalan_details') as $chalan_detail){
            if(!isset($chalan_detail['quantity']) || $chalan_detail['quantity'] <= 0){
                continue;
            }
            RequisitionChalanDetail::create([
                'req_chalan_id' => $req_chalan->id,
                'item_id'   => $chalan_detail['item_id'],
                'item_type' => $chalan_detail['item_type'],
                'unit_id'   => $chalan_detail['unit_id'],
                'quantity'  => $chalan_detail['quantity'],
                'p_req_d_id'=> $chalan_detail['p_req_d_id'],
            ]);
        }

        return redirect()->route('wirehouse.requisition-chalans')->withMessage([
            'status'    => 'alert alert-success',
            'text'      => 'Successfully created requisition chalan'
        ]);
    }

    public function getProductRequisitionItems($id){
        $demand = ProductDemand::findOrFail($id);
        $requisition_data = [];

        foreach ($demand->productDemandDetails as $demandDetail){
            $total_received = $demandDetail->chalanDetails ? $demandDetail->chalanDetails->sum('quantity') : 0;
            $rem_qty = $demandDetail->quantity - $total_received;
            $rem_qty = $rem_qty - $demandDetail->returned_qty;

            $requisition_data[] = [
                'title' => $demandDetail->item->name,
                'item_id'   => $demandDetail->item->id,
                'total_qty' => $demandDetail->quantity,
                'rec_qty' => $total_received,
                'rem_qty' => $rem_qty,
                'quantity' => "",
                'returned_qty'  =>  $demandDetail->returned_quantity,
                'item_type'  => $demandDetail->item_type,
                'p_req_d_id'   => $demandDetail->id,
                'unit'  => $demandDetail->item->base_unit ? $demandDetail->item->unit->name : 'SET',
                'unit_id'   => $demandDetail->item->base_unit ? $demandDetail->item->unit->id : null
            ];

        }

        return [
            'items' => $requisition_data,
        ];
    }
}
