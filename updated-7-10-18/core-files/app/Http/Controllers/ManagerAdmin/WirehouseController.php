<?php

namespace App\Http\Controllers\ManagerAdmin;

use App\Model\WireHouse\WireHouse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class WirehouseController extends Controller
{
    public function index(){
        $wh_id = Auth::user()->wh_id;
        $wirehouse = WireHouse::findOrFail($wh_id);
        return view('wirehouse-manager.wirehouse.index')->with([
            'wirehouse' => $wirehouse
        ]);
    }
}
