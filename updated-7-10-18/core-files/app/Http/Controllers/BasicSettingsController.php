<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\BasicSettings;
use DB;

class BasicSettingsController extends Controller
{
    public function index(){
    	$data = BasicSettings::first();
    	//return $data;
    	return view('admin.add_basicsettings')->with([
    		'data' => $data
    	]);
    }

    public function update(Request $request, $id){
        //echo $id;
        //return $request->all();

        $this->validate($request, array(
            'name'          => 'required|max:255',
            'phone'         => 'required|max:255',
            'email'         => 'required|max:255',
            'mobile'        => 'required|max:255',
            'address'       => 'required|max:255'
        ));


        $updateDetails=array(
            'name'   => $request->get('name'),
            'phone'   => $request->get('phone'),
            'email'   => $request->get('email'),
            'mobile'   => $request->get('mobile'),
            'address'   => $request->get('address')            
        );

        //return $request->file('attach')->getClientOriginalExtension();



        if($request->file('attach')){
        	$name = time().".".$request->file('attach')->getClientOriginalExtension();
      		$request->file('attach')->move(public_path('attachments'),$name);
      		$name = 'attachments/'.$name;
      		$updateDetails['image'] = $name; 	
        }

        //return $updateDetails;

        DB::table('basic_settings')
                ->where('id', $id)
                ->update($updateDetails);

        return redirect()->route('basicsettings')->withMessage([
            'status'    => 'alert-success',
            'text'      => 'Successfully updated basic settings'
        ]);
}

}
