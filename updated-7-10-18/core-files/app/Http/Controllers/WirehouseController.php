<?php

namespace App\Http\Controllers;

use App\Model\WireHouse\WireHouse;
use Illuminate\Http\Request;

use DB;

class WirehouseController extends Controller
{
    public function index(){
    	$wirehouses = WireHouse::all();

    	return view('admin.wire-house.index')->with([
    		'wirehouses' => $wirehouses
    	]);
    }

    public function store(Request $request){

    	//return $request->all();

    	$this->validate($request, array(
    		'name' => 'required|max:255',
    		'phone' => 'required|max:255',
    		'email' => 'required|max:255',
    		'mobile' => 'required|max:255',
    		'address' => 'required',
    		'status' => 'required'
    	));

    	$wirehouse = new WireHouse;

    	$wirehouse->name = $request->name;
    	$wirehouse->phone = $request->phone;
    	$wirehouse->email = $request->email;
    	$wirehouse->mobile = $request->mobile;
    	$wirehouse->address = $request->address;
    	$wirehouse->status = $request->status;
    	$wirehouse->version = $request->version;

    	$wirehouse->save();


    	return redirect()->route('wirehouses')->withMessage([
            'status'    => 'alert-success',
            'text'  => 'Warehouse created successfully'
        ]);
    }

    public function delete($id){
        
        DB::table('wire_houses')
                    ->where('id',$id)
                    ->update(['status' => 0]);

        return redirect()->route('wirehouses')->withMessage([
            'status'    => 'alert-success',
            'text'  => 'Warehouse deleted successfully'
        ]);
        
    }

    public function showUpdate($id){
        //$product_types = ProductType::all();
        /*  $types = DB::table('product_types')
                        ->where('id', $id)
                        ->get();*/  
        
        $wirehouse = WireHouse::find($id);   

            /*return $types;*/


        //return $product_types;
        return view('editpages.edit_wirehouse')->with([
            'wirehouse'   => $wirehouse
      ]);
    }

    public function update(Request $request, $id){
        //echo $id;
        //return $request->all();

        $this->validate($request, array(
            'name'    => 'required|max:255',
            'email'   => 'required',
            'phone'   => 'required|max:255',
            'mobile'  => 'required',
            'address' => 'required|max:255',
            'status'  => 'required'
        ));


        $updateDetails=array(
            'name'      => $request->input('name'),
            'email'     => $request->input('email'),
            'phone'     => $request->input('phone'),
            'mobile'    => $request->input('mobile'),
            'address'   => $request->input('address'),
            'status'    => $request->input('status'),
            'version'   => $request->input('version')
        );

        DB::table('wire_houses')
                ->where('id', $id)
                ->update($updateDetails);

        return redirect()->route('wirehouses')->withMessage([
            'status'    => 'alert-success',
            'text'  => 'Warehouse updated successfully'
        ]);
    }

    
}
