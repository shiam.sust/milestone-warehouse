<?php

namespace App\Http\Controllers;

use App\Model\WireHouse\Vendor;
use App\Model\WireHouse\VendorContact;
use Illuminate\Http\Request;
use DB;

class VendorController extends Controller
{
    public function index(){
    	$vendors = Vendor::all();
        //echo $vendors[0]->name;
    	return view('admin.table_vendor')->with([
    		'vendors' => $vendors
    	]);
    }

    public function store(Request $request){

    	//return $request->all();


    	//server validation of our form data
    	$this->validate($request, array(
    		'name' => 'required|max:255',
    		'status' => 'required',
            'pname'    => 'required|max:255',
    	));

    	//creating model object and setting our data to database column
    	$vendor = new Vendor;

    	$vendor->name = $request->name;
    	$vendor->mobile = $request->mobile;
    	$vendor->email = $request->email;
    	$vendor->address = $request->address;
    	$vendor->invoiceaddress = $request->invoiceaddress;
    	$vendor->status = $request->status;

    	//push data to database
    	$vendor->save();

    	//creting model object and setting our data to database column
    	$vendorcontact = new VendorContact;

    	$vendorcontact->vd_id = $vendor->id;
    	$vendorcontact->name = $request->pname;
    	$vendorcontact->mobile = $request->pmobile;
    	$vendorcontact->designation = $request->pdesignation;

    	$vendorcontact->save();

    	//redirecting to vendorlist 
    	return redirect()->route('vendors')->withMessage([
    	    'status'    => 'alert-success',
            'text'      => 'Successfully created vendor.'
        ]);
    }

    public function delete($id){
        
        DB::table('vendors')
                    ->where('id',$id)
                    ->update(['status' => 0]);

        return redirect()->route('vendors')->withMessage([
            'status'    => 'alert-success',
            'text'      => 'Successfully deleted vendor.'
        ]);
        
    }

    public function showUpdate($id){
        
        $vendor = Vendor::findOrfail($id);
        return view('editpages.edit_vendor')->with([
            'vendor'   => $vendor,
            
        ]);

    }

    public function update(Request $request, $id){

        $this->validate($request, array(
            'name'          => 'required|max:255',
            'status'        => 'required',
            'pname'    => 'required|max:255',
        ));

        $updateVendor=array(
            'name'   => $request->get('name'),
            'mobile'   => $request->get('mobile'),
            'email'   => $request->get('email'),
            'address'   => $request->get('address'),
            'invoiceaddress'   => $request->get('invoiceaddress'),
            'status' => $request->get('status')
        );

        DB::table('vendors')
                ->where('id', $id)
                ->update($updateVendor);

        $updateContact=array(
            'name'   => $request->get('pname'),
            'mobile'   => $request->get('pmobile'),
            'designation'   => $request->get('pdesignation'),
            'vd_id' => $id
            
        );

        $first = VendorContact::where('vd_id', '=', $id)->first();

        if(!$first){
            VendorContact::create($updateContact);
        }else{
            $first->update($updateContact);
        }
        
        return redirect()->route('vendors')->withMessage([
            'status'    => 'alert-success',
            'text'      => 'Successfully updated vendor.'
        ]);
    }
}
