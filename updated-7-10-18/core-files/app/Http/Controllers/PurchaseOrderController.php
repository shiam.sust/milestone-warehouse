<?php

namespace App\Http\Controllers;

use App\Model\Product\Product;
use App\Model\Product\ProductSet;
use App\Model\PurchaseOrder;
use App\Model\PurchaseOrderDetail;
use App\Model\WireHouse\Vendor;
use App\Model\WireHouse\WireHouse;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PurchaseOrderController extends Controller
{
    public function index(){

        $purchase_orders = PurchaseOrder::orderBy('id','desc')->paginate(10);
        return view('admin.purchase-order.index')->with([
            'purchase_orders'   => $purchase_orders
        ]);
    }


    public function create(){
        $wirehouses = WireHouse::all();
        $vendors =Vendor::all();

        $date = Carbon::today();
        $pd = count(PurchaseOrder::all()) + 1;
        $po_no_auto = 'PO-'.$date->year.'-' . $date->month.'-'. $date->day. '-' .str_pad($pd,4,'0',STR_PAD_LEFT);

        return view('purchase.create-purchase')->with([
            'wirehouses'    => $wirehouses,
            'vendors'       => $vendors,
            'po_no_auto'    => $po_no_auto
        ]);
    }

    public function store(Request $request){
        $purchase_order = PurchaseOrder::create($request->only(['wh_id','vendor_id','po_no_auto','po_no_manual','po_date','delivery_date']));

        foreach ($request->input('product') as $product){

            $item = null;

            if($product['type'] == 1){
                $item = Product::findOrFail($product['id']);
            }else{
                $item = ProductSet::findOrFail($product['id']);
            }

            PurchaseOrderDetail::create([
                'po_id'         => $purchase_order->id,
                'quantity'  => $product['quantity'],
                'unit_price'    => $product['price'],
                'base_unit'     => $product['type'] == 1 ? $item->base_unit : null,
                'item_type'     => $product['type'],
                'product_id'    => $product['type'] == 1 ? $item->id : null,
                'product_set_id' => $product['type'] == 2 ? $item->id : null,
            ]);
        }

        return redirect()->route('purchase-orders')->withMessage([
            'status'    => 'alert-success',
            'text'      => 'Purchase order created successfully.'
        ]);
    }

    public function getPurchaseOrder($id){
        $purchase_order = PurchaseOrder::findOrFail($id);
        return view('admin.purchase-order.details')->with([
            'purchase_order'   => $purchase_order
        ]);
    }

    public function editPurchaseOrder($id){

        $purchase_order = PurchaseOrder::findOrFail($id);

        $products = Product::whereDoesntHave('purchaseDetails',function ($query) use($id){
            $query->where('po_id',$id);
        })->get();

        $product_sets = ProductSet::whereDoesntHave('purchaseDetails',function ($query) use($id){
            $query->where('po_id',$id);
        })->get();


        $product_data = [];

        foreach ($products as $product){
            $product_data[] = [
                'id'    => $product->id,
                'name'  => $product->name,
                'unit'  => $product->unit->name,
                'type'  => 1,
                'price'  => $product->cost_price,
                'quantity'  => 1,
                'subTotal'  => $product->cost_price
            ];
        }

        $product_set_data = [];

        foreach ($product_sets as $product_set){
            $product_set_data[] = [
                'id'    => $product_set->id,
                'name'  => $product_set->name,
                'unit'  => 'SET',
                'type'  => 2,
                'price'  => $product_set->cost_price,
                'quantity'  => 1,
                'subTotal'  => $product_set->cost_price
            ];
        }

        $items = [];

        foreach ($purchase_order->purchaseOrderDetails as $detail){
            $items[] =[
                'id'    => $detail->item->id,
                'name'  => $detail->item->name,
                'unit'  => $detail->item_type == 1 ? $detail->unit->name: 'SET',
                'type'  => $detail->item_type,
                'price'  => $detail->unit_price,
                'quantity'  => $detail->quantity,
                'subTotal'  => $detail->unit_price * $detail->quantity
            ];
        }

        $wirehouses = WireHouse::all();
        $vendors =Vendor::all();

        return view('admin.purchase-order.edit')->with([
            'data'  => json_encode([
                'product'   => $product_data,
                'sets'      => $product_set_data,
                'items'     => $items
            ]),
            'wirehouses'    => $wirehouses,
            'vendors'       => $vendors,
            'purchase_order'    => $purchase_order
        ]);




    }

    public function updatePurchaseOrder(Request $request,$id){
        $purchase_order = PurchaseOrder::findOrFail($id);

        $purchase_order->update($request->only(['wh_id','vendor_id','po_no_auto','po_no_manual','po_date','delivery_date']));

        PurchaseOrderDetail::where('po_id',$purchase_order->id)->delete();


        foreach ($request->input('product') as $product){

            $item = null;

            if($product['type'] == 1){
                $item = Product::findOrFail($product['id']);
            }else{
                $item = ProductSet::findOrFail($product['id']);
            }

            PurchaseOrderDetail::create([
                'po_id'         => $purchase_order->id,
                'quantity'  => $product['quantity'],
                'unit_price'    => $product['price'],
                'base_unit'     => $product['type'] == 1 ? $item->base_unit : null,
                'item_type'     => $product['type'],
                'product_id'    => $product['type'] == 1 ? $item->id : null,
                'product_set_id' => $product['type'] == 2 ? $item->id : null,
            ]);
        }

        return redirect()->route('purchase-orders')->withMessage([
            'status'    => 'alert-success',
            'text'      => 'Purchase order updated successfully.'
        ]);
    }

    public function getPurchaseOrderItems($id){
        $purchase_order = PurchaseOrder::findOrFail($id);
        $purchase_order_data = [];

        foreach ($purchase_order->purchaseOrderDetails as $purchase_order_detail){
            $total_received = $purchase_order_detail->chalanDetails ? $purchase_order_detail->chalanDetails->sum('received_qty') : 0;
            $rem_qty = $purchase_order_detail->quantity - ($total_received + $purchase_order_detail->returned_quantity);

            $purchase_order_data[] = [
                'title' => $purchase_order_detail->item->name,
                'total_qty' => $purchase_order_detail->quantity,
                'unit_price' => $purchase_order_detail->unit_price,
                'rec_qty' => $total_received,
                'rem_qty' => $rem_qty,
                'quantity' => 0,
                'delivered_amount' => 0,
                'type'  => $purchase_order_detail->item_type,
                'sale_id'   => $purchase_order_detail->id,
                'unit'  => $purchase_order_detail->item->base_unit ? $purchase_order_detail->item->unit->name : 'SET',
                'returned_qty'  =>  $purchase_order_detail->returned_quantity,
            ];

        }

        return [
            'items' => $purchase_order_data,
        ];
    }
    public function getReurnPurchaseOrder($id){
        $purchase_order = PurchaseOrder::findOrFail($id);
        $purchase_order_data = [];

        foreach ($purchase_order->purchaseOrderDetails as $purchase_order_detail){
            $total_received = $purchase_order_detail->chalanDetails ? $purchase_order_detail->chalanDetails->sum('received_qty') : 0;
            $rem_qty = $purchase_order_detail->quantity - ($total_received + $purchase_order_detail->returned_quantity);

            $purchase_order_data[] = [
                'title' => $purchase_order_detail->item->name,
                'total_qty' => $purchase_order_detail->quantity,
                'unit_price' => $purchase_order_detail->unit_price,
                'rec_qty' => $total_received,
                'rem_qty' => $rem_qty,
                'quantity' => 0,
                'delivered_amount' => 0,
                'returned_qty'  =>  $purchase_order_detail->returned_quantity,
                'type'  => $purchase_order_detail->item_type,
                'id'   => $purchase_order_detail->id,
                'unit'  => $purchase_order_detail->item->base_unit ? $purchase_order_detail->item->unit->name : 'SET',
            ];

        }

        return view('admin.purchase-order.return')->with([
            'items' => json_encode($purchase_order_data),
            'purchase_order'    => $purchase_order
        ]);
    }

    public function storeReurnPurchaseOrder(Request $request,$id){

        $purchase_order = PurchaseOrder::findOrFail($id);

        foreach ($request->input('purchase_od') as $k => $po_detail){
            $pod = PurchaseOrderDetail::findOrFail($k);
            $prev_ret = $pod->returned_quantity;

            $pod->update([
                'returned_quantity' => $po_detail['quantity'] + $prev_ret
            ]);


        }

        $this->checkPurchaseOrderStatus($purchase_order->id);

        return redirect()->route('get-purchase-order',['id' => $id])->withMessage([
            'status'    => 'alert-success',
            'text'      => 'Successfully returned items.'
        ]);
    }

    private function checkPurchaseOrderStatus($id){
        $purchaseOrder = PurchaseOrder::findOrFail($id);
        foreach ($purchaseOrder->purchaseOrderDetails as $purchase_order_detail){
            $total_received = $purchase_order_detail->chalanDetails ? $purchase_order_detail->chalanDetails->sum('received_qty') : 0;
            $rem_qty = $purchase_order_detail->quantity - $total_received;
            $rem_qty = $rem_qty - $purchase_order_detail->returned_quantity;
            if($rem_qty > 0){
                dd($purchase_order_detail);
                $purchaseOrder->update([
                    'status'    => 1
                ]);
            }
        }

        $purchaseOrder->update([
            'status'    => 2
        ]);

    }


}
