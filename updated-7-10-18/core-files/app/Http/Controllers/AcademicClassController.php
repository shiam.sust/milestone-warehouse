<?php

namespace App\Http\Controllers;

use App\Model\Classes\AcademicClass;
use Illuminate\Http\Request;
use DB;



class AcademicClassController extends Controller
{
    public function index(){
        $academic_classes = AcademicClass::all();
        
        return view('admin.class')->with([
            'academic_classes' => $academic_classes
        ]);
    }

    public function store(Request $request){

    	//return $request->all();

    	$this->validate($request, array(
    		'name' => 'required|max:255',
            'version' => 'required',
    		'status' => 'required'
    	));

    	$class = new AcademicClass;

    	$class->name = $request->name;
        $class->version = $request->version;
    	$class->status = $request->status;

    	$class->save();
    	

    	return redirect()->route('class');
    }

    public function delete($id){
        //echo $id;
        //AcademicClass::where('id', '=', '$id')->update(['status' => 0 ]);
        DB::table('academic_classes')
                    ->where('id',$id)
                    ->update(['status' => 0]);

        /*$academic_classes = AcademicClass::all();

        
        return view('admin.class')->with([
            'academic_classes' => $academic_classes
        ]);*/

        return redirect()->route('class')->with([
            'message' => [
                'status'    => 'alert-success',
                'text'      => 'Successfully deleted class.'
            ]
        ]);
    }

    public function showUpdate($id){
        //$product_types = ProductType::all();
        /*  $types = DB::table('product_types')
                        ->where('id', $id)
                        ->get();*/  
        $class = AcademicClass::find($id);   

            /*return $types;*/

        //return $class;
        //return $product_types;
        return view('editpages.edit_class')->with([
            'class'   => $class
        ]);
    }

    public function update(Request $request, $id){
        //echo $id;
        //return $request->all();

        $this->validate($request, array(
            'name' => 'required|max:255',
            'version' => 'required',
            'status' => 'required'
        ));

        $updateDetails=array(
            'name'   => $request->get('name'),
            'version' => $request->get('version'),
            'status' => $request->get('status')
        );

        DB::table('academic_classes')
                ->where('id', $id)
                ->update($updateDetails);

        return redirect()->route('class')->with([
            'message' => [
                'status'    => 'alert-success',
                'text'      => 'Successfully updated class'
            ]
        ]);
    }
}
