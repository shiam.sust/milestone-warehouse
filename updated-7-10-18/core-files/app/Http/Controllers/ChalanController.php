<?php

namespace App\Http\Controllers;

use App\Model\Chalan;
use App\Model\ChalanDetail;
use App\Model\PurchaseOrderDetail;
use App\Model\WireHouse\WireHouseProduct;
use App\Model\WireHouse\WireHouseProductSet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;
use App\Model\PurchaseOrder;

class ChalanController extends Controller
{
    public function index(){
        $chalans = Chalan::orderBy('id','desc')->get();
        return view('admin.chalan.index')->with([
            'chalans'   => $chalans
        ]);
    }

    public function getChalan($id){
        $chalan = Chalan::findOrFail($id);
        return view('admin.chalan.details')->with([
            'chalan'    => $chalan
        ]);
    }

    public function approveChalan($id){
        $chalan = Chalan::findOrFail($id);

        if($chalan->status){
            return redirect()->back()->withMessage([
                'status'    => 'alert alert-danger',
                'text'      => 'This chan is already approved'
            ]);
        }

        foreach ($chalan->chalanDetails as $chalanDetail){
            if($chalanDetail->item_type == 1){
                $wh_product = WireHouseProduct::where('product_id',$chalanDetail->product_id)->where('wh_id',$chalan->wh_id)->first();

                if($wh_product){
                    $q = $chalanDetail->received_qty + $wh_product->quantity;
                    $sb = $wh_product->stock_balance + $chalanDetail->received_qty;
                    $wh_product->update([
                        'quantity'      => $q,
                        'stock_balance' => $sb
                    ]);
                }else{
                    WireHouseProduct::create([
                        'wh_id' => $chalan->wh_id,
                        'product_id'    => $chalanDetail->product_id,
                        'quantity'      => $chalanDetail->received_qty,
                        'stock_balance' => $chalanDetail->received_qty,
                        'status'        => 1
                    ]);
                }

            }
            else{
                $wh_product_set = WireHouseProductSet::where('product_set_id',$chalanDetail->product_set_id)->where('wh_id',$chalan->wh_id)->first();
                if($wh_product_set){
                    $wh_product_set->update([
                        'quantity'      => $chalanDetail->received_qty + $wh_product_set->quantity,
                        'stock_balance' => $wh_product_set->stock_balance + $chalanDetail->received_qty
                    ]);
                }else{
                    WireHouseProductSet::create([
                        'wh_id'             => $chalan->wh_id,
                        'product_set_id'    => $chalanDetail->product_set_id,
                        'quantity'          => $chalanDetail->quantity,
                        'stock_balance'     => $chalanDetail->received_qty,
                        'status'            => 1
                    ]);
                }

            }
        }
        $chalan->update([
            'status'    => true
        ]);
        return redirect()->route('chalans')->withMessage([
            'status'    => 'alert-success',
            'text'      => 'Successfully approved chalan.'
        ]);

    }

    public function create($id = null){
        $wh_id = Auth::user()->wh_id;
        $date = Carbon::today();
        $pd = count(Chalan::all()) + 1;
        $ch_no = 'CH-'.$date->year.'-' . $date->month.'-'. $date->day. '-' .str_pad($pd,4,'0',STR_PAD_LEFT);
        $purchase_orders = PurchaseOrder::where('wh_id',$wh_id)->where('status','<',2)->orderBy('id','desc')->get();
        return view('admin.chalan.create')->with([
            'purchase_orders'   => $purchase_orders,
            'ch_auto' => $ch_no,
            'po_id' => $id
        ]);
    }

    public function store(Request $request){

        //return $request->all();

        $purchase_order = PurchaseOrder::findOrFail($request->input('purchase_order'));
        $wh_id = Auth::user()->wh_id;

        $chalan = Chalan::create([
            'purchase_order_id' => $purchase_order->id,
            'status'    => false,
            'chalan_date'   => $request->input('chalan_date'),
            'chalan_no'     => $request->input('chalan_no'),
            'chalan_no_manual' => $request->input('chalan_no_manual'),
            'wh_id' => $wh_id
        ]);

        $created = false;

        foreach ($request->input('purchase_od') as $k => $v){
            $od = PurchaseOrderDetail::findOrFail($k);

            if(isset($v['quantity']) && $v['quantity'] > 0){
                ChalanDetail::create([
                    'chalan_id' => $chalan->id,
                    'purchase_order_details_id' => $od->id,
                    'product_id' => $od->item_type == 1 ? $od->item->id : null,
                    'product_set_id' => $od->item_type == 2 ? $od->item->id : null,
                    'received_qty'  => $v['quantity'],
                    'product_amount' => $od->unit_price * $v['quantity'],
                    'item_type' => $od->item_type,
                ]);

                $created = true;
            }

        }
        if(!$created)
        {
            $chalan->delete();
            return redirect()->back()->with([
                'message' => [
                    'status'    => 'alert-danger',
                    'text'      => 'Please enter the valid quantity.'
                ]
            ]);
        }

        $this->checkPurchaseOrderStatus($purchase_order);

        return redirect()->route('chalans')->with([
            'message' => [
                'status'    => 'alert-success',
                'text'      => 'Successfully created chalan.'
            ]
        ]);

    }

    private function checkPurchaseOrderStatus(PurchaseOrder $purchaseOrder){
        foreach ($purchaseOrder->purchaseOrderDetails as $purchase_order_detail){
            $total_received = $purchase_order_detail->chalanDetails ? $purchase_order_detail->chalanDetails->sum('received_qty') : 0;
            $rem_qty = $purchase_order_detail->quantity - $total_received;
            $rem_qty = $rem_qty - $purchase_order_detail->returned_quantity;
            if($rem_qty > 0){
                $purchaseOrder->update([
                    'status'    => 1
                ]);
                return;
            }
        }

        $purchaseOrder->update([
            'status'    => 2
        ]);
        return;
    }

}
