<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'status', 'wh_id','username','manager_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function warehouse(){
        return $this->belongsTo('App\Model\WireHouse\WireHouse','wh_id','id');
    }

    public function warehouseContact(){
        return $this->belongsTo('App\Model\WireHouse\WireHouseContact','manager_id','id');
    }
    public function roles(){
        return $this->belongsToMany('App\Role','user_roles','user_id','role_id')->withTimestamps();
    }

    public function hasRole($id){
        return $this->roles->contains('id',$id);
    }

    public function mainBranch(){
        if($this->warehouse && $this->warehouse->is_main){
            return true;
        }
        return false;
    }
}
