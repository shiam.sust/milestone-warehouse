<?php
/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 24-Apr-18
 * Time: 7:18 AM
 */

namespace App\Repository\Eloquent;


use App\Model\Product\Unit;
use App\Repository\UnitRepository;

class EloquentUnit extends EloquentBase implements UnitRepository
{
    public function __construct(Unit $unit)
    {
        $this->model = $unit;
    }

}