<?php

namespace App\Model\ProductReturn;

use Illuminate\Database\Eloquent\Model;

class ProductReturnChalan extends Model
{
    protected $fillable = [
        'product_return_id',
        'ch_no_auto',
        'ch_no_manual',
        'ch_date',
        'status',
        'wh_id'
    ];

    public function wireHouse(){
        return $this->belongsTo('App\Model\WireHouse\WireHouse','wh_id','id');
    }

    public function productReturn(){
        return $this->belongsTo('App\Model\ProductReturn\ProductReturn','product_return_id','id');
    }

    public function chalanDetails(){
        return $this->hasMany('App\Model\ProductReturn\ProductReturnChalanDetail','ret_chalan_id','id');
    }
}
