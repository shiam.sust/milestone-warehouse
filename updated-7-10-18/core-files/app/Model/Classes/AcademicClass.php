<?php

namespace App\Model\Classes;

use Illuminate\Database\Eloquent\Model;

class AcademicClass extends Model
{
    protected $fillable = ['name', 'status','version'];
    protected $appends = ['academic_version'];

    public function classVersion(){
        $version = [
            '1' => 'Bangla Medium Junior Section (BMJS)',
            '2' => 'Bangla Medium Senior Section (BMSS)',
            '3' => 'English Medium Junior Section (EMJS)',
            '4' => 'English Medium Senior Section (EMSS)'
        ];

        if($this->version >=1 && $this->version <=4){
            return $version[$this->version];
        }
    }

    public function getAcademicVersionAttribute(){
        return $this->classVersion();
    }
}
