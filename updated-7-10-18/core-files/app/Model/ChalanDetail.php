<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ChalanDetail extends Model
{
    protected $fillable = [
        'chalan_id',
        'purchase_order_details_id',
        'product_id',
        'product_set_id',
        'received_qty',
        'product_amount',
        'item_type'
    ];

    protected $appends = ['item'];

    public function purchaseOrder(){
        return $this->belongsTo('App\Model\Chalan','chalan_id','id');
    }

    public function purchaseOrderDetail(){
        return $this->belongsTo('App\Model\PurchaseOrderDetail','purchase_order_details_id','id');
    }


    public function product(){
        return $this->belongsTo('App\Model\Product\Product','product_id','id');
    }

    public function productSet(){
        return $this->belongsTo('App\Model\Product\ProductSet','product_set_id','id');
    }

    public function getItemAttribute(){
        return $this->item_type == 1 ? $this->product : $this->productSet;
    }
}
