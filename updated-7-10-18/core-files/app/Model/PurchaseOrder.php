<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PurchaseOrder extends Model
{
    protected $fillable = ['wh_id','vendor_id','po_no_auto','po_no_manual','po_date','status','delivery_date'];

    protected $appends = ['total_amount'];

    public function wireHouse(){
        return $this->belongsTo('App\Model\WireHouse\WireHouse','wh_id','id');
    }

    public function vendor(){
        return $this->belongsTo('App\Model\WireHouse\WireHouse','vendor_id','id');
    }

    public function purchaseOrderDetails(){
        return $this->hasMany('App\Model\PurchaseOrderDetail','po_id','id');
    }
    public function chalans(){
        return $this->hasMany('App\Model\Chalan','purchase_order_id','id');
    }



    public function getTotalAmountAttribute(){
        return $this->purchaseOrderDetails->sum('sub_total');
    }

    public function setDeliveryDateAttribute($value)
    {
        $this->attributes['delivery_date'] = Carbon::parse($value)->format('Y-m-d');
    }

    public function setPoDateAttribute($value)
    {
        $this->attributes['po_date'] = Carbon::parse($value)->format('Y-m-d');
    }


}
