<?php

namespace App\Model\WireHouse;

use Illuminate\Database\Eloquent\Model;

class WireHouse extends Model
{
    protected $fillable = ['name', 'wh_id', 'phone', 'email', 'mobile', 'address','version'];

    public function WireHouseContacts(){
    	return $this->hasMany('App\Model\WireHouse\WireHouseContact', 'wh_id', 'id');
    } 
}
