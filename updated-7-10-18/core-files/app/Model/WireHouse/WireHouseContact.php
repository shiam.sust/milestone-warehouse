<?php

namespace App\Model\WireHouse;

use Illuminate\Database\Eloquent\Model;

class WireHouseContact extends Model
{
    protected $fillable = [
        'name',
        'wh_id',
        'designation',
        'address',
        'email',
        'mobile',
        'profile_pic'
    ];

    public function wireHouse(){
    	return $this->belongsTo('App\Model\WireHouse\WireHouse','wh_id','id');
    }
}
