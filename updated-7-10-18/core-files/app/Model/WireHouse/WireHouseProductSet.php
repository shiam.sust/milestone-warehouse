<?php

namespace App\Model\WireHouse;

use Illuminate\Database\Eloquent\Model;

class WireHouseProductSet extends Model
{
    protected $fillable = [
        'wh_id',
        'product_set_id',
        'initial_quantity',
        'quantity',
        'stock_balance',
        'status'
    ];

    public function productSetDetails(){
        return $this->hasMany('App\Model\Product\ProductSetDetails','product_set_id','id');
    }

    public function warehouse(){
        return $this->belongsTo('App\Model\WireHouse\WireHouse','wh_id','id');
    }

    public function productSet(){
        return $this->belongsTo('App\Model\Product\ProductSet','product_set_id','id');
    }
}
