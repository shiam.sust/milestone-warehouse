<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Chalan extends Model
{
    protected $fillable = [
        'purchase_order_id',
        'chalan_date',
        'chalan_no',
        'chalan_no_manual',
        'status',
        'wh_id'
    ];

    public function purchaseOrder(){
        return $this->belongsTo('App\Model\PurchaseOrder','purchase_order_id','id');
    }

    public function chalanDetails(){
        return $this->hasMany('App\Model\ChalanDetail','chalan_id','id');
    }

    public function payments(){
        return $this->hasMany('App\Model\Payment','chalan_id','id');
    }

    public function wireHouse(){
        return $this->belongsTo('App\Model\WireHouse\WireHouse','wh_id','id');
    }
}
