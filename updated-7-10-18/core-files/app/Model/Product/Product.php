<?php

namespace App\Model\Product;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name',
        'type_id',
        'manufacturer_id',
        'base_unit',
        'author',
        'selling_price',
        'cost_price',
        'status',
    ];

    protected $appends = ['item_type'];

    public function productType(){
        return $this->belongsTo('App\Model\Product\ProductType','type_id','id');
    }

    public function manufacturer(){
        return $this->belongsTo('App\Model\Product\Manufacturer','manufacturer_id','id');
    }
    public function unit(){
        return $this->belongsTo('App\Model\Product\Unit','base_unit','id');
    }

    public function productSetDetails(){
        return $this->hasMany('App\Model\Product\ProductSetDetails','product_id','id');
    }

    public function purchaseDetails(){
        return $this->hasMany('App\Model\PurchaseOrderDetail','product_id','id');
    }

    public function warehouses(){
        return $this->belongsToMany('App\Model\Wirehouse\Wirehouse','wire_house_products','product_id','wh_id');
    }

    public function getItemTypeAttribute(){
        return 1;
    }
}
