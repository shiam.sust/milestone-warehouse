<?php

namespace App\Model\Product;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $fillable = ['name', 'status'];

    public function products(){
    	return $this->hasMany('App\Model\Product\Product', 'base_unit', 'id');
    } 
}
